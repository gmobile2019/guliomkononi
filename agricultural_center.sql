-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 28, 2018 at 04:40 PM
-- Server version: 5.7.22-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agricultural_center`
--
CREATE DATABASE IF NOT EXISTS `agricultural_center` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `agricultural_center`;

-- --------------------------------------------------------

--
-- Table structure for table `agro_cart`
--

DROP TABLE IF EXISTS `agro_cart`;
CREATE TABLE `agro_cart` (
  `id` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `productprice` float NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `refOrderID` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `agro_cart`
--

INSERT INTO `agro_cart` (`id`, `productid`, `quantity`, `productprice`, `status`, `refOrderID`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(2, 4, 1500, 750000, 'processed', 'ABC251', 8, '2018-05-24 12:37:00', 8, '2018-05-28 16:34:39'),
(3, 5, 320, 320000, 'processed', 'ABC251', 8, '2018-05-24 12:37:12', 8, '2018-05-28 16:34:39'),
(4, 1, 1000, 2000000, 'processed', 'ABC251', 8, '2018-05-28 16:25:10', 8, '2018-05-28 16:34:39'),
(5, 2, 1000, 500000, 'processed', 'ABC251', 8, '2018-05-28 16:26:38', 8, '2018-05-28 16:34:39');

-- --------------------------------------------------------

--
-- Table structure for table `agro_delivery_cost`
--

DROP TABLE IF EXISTS `agro_delivery_cost`;
CREATE TABLE `agro_delivery_cost` (
  `id` int(11) NOT NULL,
  `destination` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `cost` float NOT NULL,
  `status` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `agro_delivery_cost`
--

INSERT INTO `agro_delivery_cost` (`id`, `destination`, `cost`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Buguruni', 1000, 'Active', 1, '2018-05-24 15:09:52', NULL, NULL),
(2, 'Tabata', 2000, 'Active', 1, '2018-05-24 15:21:36', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `agro_inventory`
--

DROP TABLE IF EXISTS `agro_inventory`;
CREATE TABLE `agro_inventory` (
  `id` int(11) NOT NULL,
  `productbatch` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supplier` int(11) NOT NULL,
  `productid` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `availabilityPeriod` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `availableDate` date DEFAULT NULL,
  `status` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `reference_txn` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reference_orderID` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comments` text COLLATE utf8_unicode_ci,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `agro_inventory`
--

INSERT INTO `agro_inventory` (`id`, `productbatch`, `supplier`, `productid`, `quantity`, `availabilityPeriod`, `availableDate`, `status`, `reference_txn`, `reference_orderID`, `comments`, `createdon`, `createdby`) VALUES
(1, 'OFHQLGCBMADPJN0451276', 6, 3, 10000, 'Now', '2018-05-16', 'Stocking', NULL, NULL, NULL, '2018-05-16 18:05:45', 6),
(2, 'LCG340', 6, 4, 50000, '20', '2018-06-05', 'Stocking', NULL, NULL, NULL, '2018-05-16 18:51:11', 6),
(3, 'ONP621', 6, 5, 200, '10', '2018-05-31', 'Stocking', NULL, NULL, NULL, '2018-05-21 14:00:56', 6),
(4, 'DGJ103', 6, 4, 30000, '40', '2018-07-04', 'Stocking', NULL, NULL, NULL, '2018-05-25 19:01:14', 6),
(12, 'DGJ103', 6, 4, -1000, '40', '2018-07-04', 'Reserved', NULL, '6', NULL, '2018-05-28 14:59:49', 1),
(15, 'LCG340', 6, 4, -500, '20', '2018-06-05', 'Reserved', NULL, '6', NULL, '2018-05-28 15:17:13', 1),
(16, 'ONP621', 6, 5, -200, '10', '2018-05-31', 'Reserved', NULL, '5', NULL, '2018-05-28 15:18:19', 1);

-- --------------------------------------------------------

--
-- Table structure for table `agro_orders`
--

DROP TABLE IF EXISTS `agro_orders`;
CREATE TABLE `agro_orders` (
  `id` int(11) NOT NULL,
  `orderid` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `deliverydestination` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deliverycost` float NOT NULL,
  `productcost` float NOT NULL,
  `orderedby` int(11) NOT NULL,
  `orderdate` datetime NOT NULL,
  `orderstatus` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `deliverydriver` int(11) DEFAULT NULL,
  `deliverydate` date DEFAULT NULL,
  `reason` text COLLATE utf8_unicode_ci,
  `refTransaction` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `agro_orders`
--

INSERT INTO `agro_orders` (`id`, `orderid`, `deliverydestination`, `deliverycost`, `productcost`, `orderedby`, `orderdate`, `orderstatus`, `deliverydriver`, `deliverydate`, `reason`, `refTransaction`, `modifiedby`, `modifiedon`) VALUES
(1, 'JKL403', 'Tabata', 2000, 1070000, 8, '2018-05-24 17:24:44', 'confirmed', 9, '2018-05-28', NULL, NULL, 1, '2018-05-28 15:40:58'),
(2, 'ABC251', 'Buguruni', 1000, 2500000, 8, '2018-05-28 16:34:39', 'pending', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `agro_order_items`
--

DROP TABLE IF EXISTS `agro_order_items`;
CREATE TABLE `agro_order_items` (
  `id` int(11) NOT NULL,
  `orderid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `productid` int(11) NOT NULL,
  `productprice` float NOT NULL,
  `productqty` int(11) NOT NULL,
  `status` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'pending',
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `agro_order_items`
--

INSERT INTO `agro_order_items` (`id`, `orderid`, `productid`, `productprice`, `productqty`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(5, 'JKL403', 5, 320000, 320, 'pending', 8, '2018-05-24 17:24:44', NULL, NULL),
(6, 'JKL403', 4, 750000, 1500, 'pending', 8, '2018-05-24 17:24:44', NULL, NULL),
(7, 'ABC251', 2, 500000, 1000, 'pending', 8, '2018-05-28 16:34:39', NULL, NULL),
(8, 'ABC251', 1, 2000000, 1000, 'pending', 8, '2018-05-28 16:34:39', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `captcha`
--

DROP TABLE IF EXISTS `captcha`;
CREATE TABLE `captcha` (
  `captcha_id` bigint(13) UNSIGNED NOT NULL,
  `captcha_time` int(10) UNSIGNED NOT NULL,
  `ip_address` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `word` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `captcha`
--

INSERT INTO `captcha` (`captcha_id`, `captcha_time`, `ip_address`, `word`) VALUES
(474, 1527517665, '127.0.0.1', 'YSqZ18'),
(475, 1527517665, '127.0.0.1', '31iBt2'),
(476, 1527517665, '127.0.0.1', 'fcBoay'),
(477, 1527517729, '127.0.0.1', 'ofJyvu'),
(478, 1527517729, '127.0.0.1', '0CP3B8'),
(479, 1527517729, '127.0.0.1', 'bjgt7t');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(8) UNSIGNED NOT NULL,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'Administrator', 'system administrator'),
(2, 'Buyer', 'agro buyer'),
(3, 'Supplier', 'agro supplier'),
(4, 'Manager', 'Operations manager'),
(5, 'Farmer', 'Agricultural farmer'),
(6, 'Driver', 'drivers that are concerned with deliveries');

-- --------------------------------------------------------

--
-- Table structure for table `harvest_agreements`
--

DROP TABLE IF EXISTS `harvest_agreements`;
CREATE TABLE `harvest_agreements` (
  `id` int(11) NOT NULL,
  `projectID` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `cropID` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `harvestQuantity` float NOT NULL,
  `suitableQuantity` float NOT NULL,
  `unsuitableQuantity` float NOT NULL,
  `market_price` float NOT NULL,
  `expected_return` float NOT NULL,
  `ha_comments` text COLLATE utf8_unicode_ci,
  `status` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending',
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `agreedon` datetime DEFAULT NULL,
  `agreedby` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `harvest_agreements`
--

INSERT INTO `harvest_agreements` (`id`, `projectID`, `cropID`, `harvestQuantity`, `suitableQuantity`, `unsuitableQuantity`, `market_price`, `expected_return`, `ha_comments`, `status`, `createdon`, `createdby`, `modifiedon`, `modifiedby`, `agreedon`, `agreedby`) VALUES
(1, 'C192-2018-0321', 'C-623', 1990, 1900, 90, 100, 190000, 'hhhgjhgjhg', 'Closed', '2018-03-27 15:43:45', 1, NULL, NULL, '2018-03-29 12:41:30', '213'),
(2, 'A513-2018-0423', 'C-013', 300000, 290000, 10000, 9000, 2610000000, '', 'Cancelled', '2018-04-23 12:23:40', 1, '2018-04-23 13:10:02', 1, NULL, NULL),
(3, 'A513-2018-0423', 'C-013', 30000, 30000, 0, 600, 18000000, '', 'Closed', '2018-04-23 13:42:19', 1, '2018-04-23 13:45:13', 218, '2018-04-23 13:45:13', '218'),
(4, 'B275-2018-0508', 'C-013', 10000, 9990, 10, 1800, 17982000, '', 'Closed', '2018-05-08 17:24:17', 1, '2018-05-08 17:24:40', 218, '2018-05-08 17:24:40', '218');

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `category` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `parent_region` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `locations`
--

INSERT INTO `locations` (`id`, `name`, `category`, `parent_region`) VALUES
(1, 'Arusha Region', 'region', NULL),
(2, 'Dar es Salaam Region', 'region', NULL),
(3, 'Dodoma Region', 'region', NULL),
(4, 'Geita Region', 'region', NULL),
(5, 'Iringa Region', 'region', NULL),
(6, 'Kagera Region', 'region', NULL),
(7, 'Kaskazini Pemba Region of Zanzibar', 'region', NULL),
(8, 'Kaskazini Unguja Region of Zanzibar', 'region', NULL),
(9, 'Katavi Region', 'region', NULL),
(10, 'Kigoma Region', 'region', NULL),
(11, 'Kilimanjaro Region', 'region', NULL),
(12, 'Kusini Pemba Region of Zanzibar', 'region', NULL),
(13, 'Kusini Unguja Region of Zanzibar', 'region', NULL),
(14, 'Lindi Region', 'region', NULL),
(15, 'Manyara Region', 'region', NULL),
(16, 'Mara Region', 'region', NULL),
(17, 'Mbeya Region', 'region', NULL),
(18, 'Mjini Magharibi Region of Zanzibar', 'region', NULL),
(19, 'Morogoro Region', 'region', NULL),
(20, 'Mtwara Region', 'region', NULL),
(21, 'Mwanza Region', 'region', NULL),
(22, 'Njombe Region', 'region', NULL),
(23, 'Pwani Region', 'region', NULL),
(24, 'Rukwa Region', 'region', NULL),
(25, 'Ruvuma Region', 'region', NULL),
(26, 'Shinyanga Region', 'region', NULL),
(27, 'Simiyu Region', 'region', NULL),
(28, 'Singida Region', 'region', NULL),
(29, 'Tabora Region', 'region', NULL),
(30, 'Tanga Region', 'region', NULL),
(31, 'Meru District Council ', 'district', 1),
(32, 'Arusha City Council ', 'district', 1),
(33, 'Arusha District Council ', 'district', 1),
(34, 'Karatu District Council ', 'district', 1),
(35, 'Longido District Council ', 'district', 1),
(36, 'Monduli District Council ', 'district', 1),
(37, 'Ngorongoro District Council ', 'district', 1),
(38, 'Ilala Municipal Council ', 'district', 2),
(39, 'Kinondoni Municipal Council ', 'district', 2),
(40, 'Temeke Municipal Council ', 'district', 2),
(41, 'Bahi District Council ', 'district', 3),
(42, 'Chamwino District Council ', 'district', 3),
(43, 'Chemba District Council ', 'district', 3),
(44, 'Dodoma Municipal Council ', 'district', 3),
(45, 'Kondoa District Council ', 'district', 3),
(46, 'Kongwa District Council ', 'district', 3),
(47, 'Mpwapwa District Council ', 'district', 3),
(48, 'Bukombe District Council ', 'district', 4),
(49, 'Chato District Council ', 'district', 4),
(50, 'Geita Town Council & Geita District Council ', 'district', 4),
(51, 'Mbongwe District Council ', 'district', 4),
(52, 'Nyang\'hwale District Council ', 'district', 4),
(53, 'Iringa District Council ', 'district', 5),
(54, 'Iringa Municipal Council ', 'district', 5),
(55, 'Kilolo District Council ', 'district', 5),
(56, 'Mafinga Town Council ', 'district', 5),
(57, 'Mufindi District Council ', 'district', 5),
(58, 'Biharamulo District Council ', 'district', 6),
(59, 'Bukoba District Council ', 'district', 6),
(60, 'Bukoba Municipal Council ', 'district', 6),
(61, 'Karagwe District Council ', 'district', 6),
(62, 'Kyerwa District Council ', 'district', 6),
(63, 'Missenyi District Council ', 'district', 6),
(64, 'Muleba District Council ', 'district', 6),
(65, 'Ngara District Council ', 'district', 6),
(66, 'Micheweni District ', 'district', 7),
(67, 'Wete District ', 'district', 7),
(68, 'Kaskazini A District ', 'district', 8),
(69, 'Kaskazini B District ', 'district', 8),
(70, 'Mlele District Council ', 'district', 9),
(71, 'Mpanda District Council ', 'district', 9),
(72, 'Mpanda Town Council ', 'district', 9),
(73, 'Buhigwe District Council ', 'district', 10),
(74, 'Kakonko District Council ', 'district', 10),
(75, 'Kasulu District Council ', 'district', 10),
(76, 'Kasulu Town Council ', 'district', 10),
(77, 'Kibondo District Council ', 'district', 10),
(78, 'Kigoma District Council ', 'district', 10),
(79, 'Kigoma-Ujiji Municipal Council ', 'district', 10),
(80, 'Uvinza District Council ', 'district', 10),
(81, 'Hai District Council ', 'district', 11),
(82, 'Moshi District Council ', 'district', 11),
(83, 'Moshi Municipal Council ', 'district', 11),
(84, 'Mwanga District Council ', 'district', 11),
(85, 'Rombo District Council ', 'district', 11),
(86, 'Same District Council ', 'district', 11),
(87, 'Siha District Council ', 'district', 11),
(88, 'Chake Chake District ', 'district', 12),
(89, 'Mkoani District ', 'district', 12),
(90, 'Kati District ', 'district', 13),
(91, 'Kusini District ', 'district', 13),
(92, 'Kilwa District Council ', 'district', 14),
(93, 'Lindi District Council ', 'district', 14),
(94, 'Lindi Municipal Council ', 'district', 14),
(95, 'Liwale District Council ', 'district', 14),
(96, 'Nachingwea District Council ', 'district', 14),
(97, 'Ruangwa District Council ', 'district', 14),
(98, 'Babati Town Council ', 'district', 15),
(99, 'Babati District Council ', 'district', 15),
(100, 'Hanang District Council ', 'district', 15),
(101, 'Kiteto District Council ', 'district', 15),
(102, 'Mbulu District Council ', 'district', 15),
(103, 'Simanjiro District Council ', 'district', 15),
(104, 'Bunda District Council ', 'district', 16),
(105, 'Butiama District Council ', 'district', 16),
(106, 'Musoma District Council ', 'district', 16),
(107, 'Musoma Municipal Council ', 'district', 16),
(108, 'Rorya District Council ', 'district', 16),
(109, 'Serengeti District Council ', 'district', 16),
(110, 'Tarime District Council ', 'district', 16),
(111, 'Chunya District Council ', 'district', 17),
(112, 'Ileje District Council ', 'district', 17),
(113, 'Kyela District Council ', 'district', 17),
(114, 'Mbarali District Council ', 'district', 17),
(115, 'Mbeya City Council ', 'district', 17),
(116, 'Mbeya District Council ', 'district', 17),
(117, 'Mbozi District Council ', 'district', 17),
(118, 'Momba District Council ', 'district', 17),
(119, 'Rungwe District Council ', 'district', 17),
(120, 'Tunduma Town Council ', 'district', 17),
(121, 'Magharibi District ', 'district', 18),
(122, 'Mjini District ', 'district', 18),
(123, 'Gairo District Council ', 'district', 19),
(124, 'Kilombero District Council ', 'district', 19),
(125, 'Kilosa District Council ', 'district', 19),
(126, 'Morogoro District Council ', 'district', 19),
(127, 'Morogoro Municipal Council ', 'district', 19),
(128, 'Mvomero District Council ', 'district', 19),
(129, 'Ulanga District Council ', 'district', 19),
(130, 'Masasi District Council ', 'district', 20),
(131, 'Masasi Town Council ', 'district', 20),
(132, 'Mtwara District Council ', 'district', 20),
(133, 'Mtwara Municipal Council ', 'district', 20),
(134, 'Nanyumbu District Council ', 'district', 20),
(135, 'Newala District Council ', 'district', 20),
(136, 'Tandahimba District Council ', 'district', 20),
(137, 'Ilemela Municipal Council ', 'district', 21),
(138, 'Kwimba District Council ', 'district', 21),
(139, 'Magu District Council ', 'district', 21),
(140, 'Misungwi District Council ', 'district', 21),
(141, 'Nyamagana Municipal Council ', 'district', 21),
(142, 'Sengerema District Council ', 'district', 21),
(143, 'Ukerewe District Council ', 'district', 21),
(144, 'Ludewa District Council ', 'district', 22),
(145, 'Makambako Town Council ', 'district', 22),
(146, 'Makete District Council ', 'district', 22),
(147, 'Njombe District Council ', 'district', 22),
(148, 'Njombe Town Council ', 'district', 22),
(149, 'Wanging\'ombe District Council ', 'district', 22),
(150, 'Bagamoyo District Council ', 'district', 23),
(151, 'Kibaha District Council ', 'district', 23),
(152, 'Kibaha Town Council ', 'district', 23),
(153, 'Kisarawe District Council ', 'district', 23),
(154, 'Mafia District Council ', 'district', 23),
(155, 'Mkuranga District Council ', 'district', 23),
(156, 'Rufiji District Council ', 'district', 23),
(157, 'Kalambo District Council ', 'district', 24),
(158, 'Nkasi District Council ', 'district', 24),
(159, 'Sumbawanga District Council ', 'district', 24),
(160, 'Sumbawanga Municipal Council ', 'district', 24),
(161, 'Mbinga District Council ', 'district', 25),
(162, 'Songea District Council ', 'district', 25),
(163, 'Songea Municipal Council ', 'district', 25),
(164, 'Tunduru District Council ', 'district', 25),
(165, 'Namtumbo District Council ', 'district', 25),
(166, 'Nyasa District Council ', 'district', 25),
(167, 'Kahama Town Council ', 'district', 26),
(168, 'Kahama District Council ', 'district', 26),
(169, 'Kishapu District Council ', 'district', 26),
(170, 'Shinyanga District Council ', 'district', 26),
(171, 'Shinyanga Municipal Council ', 'district', 26),
(172, 'Bariadi District Council ', 'district', 27),
(173, 'Busega District Council ', 'district', 27),
(174, 'Itilima District Council ', 'district', 27),
(175, 'Maswa District Council ', 'district', 27),
(176, 'Meatu District Council ', 'district', 27),
(177, 'Ikungi District Council ', 'district', 28),
(178, 'Iramba District Council ', 'district', 28),
(179, 'Manyoni District Council ', 'district', 28),
(180, 'Mkalama District Council ', 'district', 28),
(181, 'Singida District Council ', 'district', 28),
(182, 'Singida Municipal Council ', 'district', 28),
(183, 'Igunga District Council ', 'district', 29),
(184, 'Kaliua District Council ', 'district', 29),
(185, 'Nzega District Council ', 'district', 29),
(186, 'Sikonge District Council ', 'district', 29),
(187, 'Tabora Municipal Council ', 'district', 29),
(188, 'Urambo District Council ', 'district', 29),
(189, 'Uyui District Council ', 'district', 29),
(190, 'Handeni District Council ', 'district', 30),
(191, 'Handeni Town Council ', 'district', 30),
(192, 'Kilindi District Council ', 'district', 30),
(193, 'Korogwe Town Council ', 'district', 30),
(194, 'Korogwe District Council ', 'district', 30),
(195, 'Lushoto District Council ', 'district', 30),
(196, 'Muheza District Council ', 'district', 30),
(197, 'Mkinga District Council ', 'district', 30),
(198, 'Pangani District Council ', 'district', 30),
(199, 'Tanga City Council ', 'district', 30);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `producttype` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Crop',
  `productname` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `productunit` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Kg',
  `productcategory` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `productimage` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `productprice` float DEFAULT NULL,
  `productdiscount` float DEFAULT NULL,
  `createdon` datetime NOT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `producttype`, `productname`, `productunit`, `productcategory`, `description`, `productimage`, `productprice`, `productdiscount`, `createdon`, `createdby`, `modifiedon`, `modifiedby`, `status`) VALUES
(1, 'Crop', 'Soybean', 'Kilogram', 'Cereal', 'The soybean, or soya bean, is a species of legume native to East Asia, widely grown for its edible bean which has numerous uses. Fat-free soybean meal is a significant and cheap source of protein for animal feeds and many packaged meals', '1526401560.jpeg', 2000, 0, '2018-05-15 19:26:00', 1, '2018-05-24 12:33:02', 1, 'Active'),
(2, 'Crop', 'Wheat', 'Kilogram', 'Cereal', 'Wheat is a grass widely cultivated for its seed, a cereal grain which is a worldwide staple food. The many species of wheat together make up the genus Triticum; the most widely grown is common wheat', '1526402088.jpg', 500, 0, '2018-05-15 19:34:48', 1, '2018-05-24 12:32:29', 1, 'Active'),
(3, 'Crop', 'Onion', 'Grams', 'Vegetable', 'The onion, also known as the bulb onion or common onion, is a vegetable that is the most widely cultivated species of the genus Allium. Its close relatives include the garlic, shallot, leek, chive, and Chinese onion.', '1526404173.jpeg', 1000, 0, '2018-05-15 20:09:33', 1, '2018-05-23 14:03:44', 1, 'Active'),
(4, 'Crop', 'Pea', 'Kilogram', 'Vegetable', 'The pea is most commonly the small spherical seed or the seed-pod of the pod fruit Pisum sativum. Each pod contains several peas, which can be green or yellow.', '1526404236.jpeg', 500, 0, '2018-05-15 20:10:36', 1, '2018-05-24 12:32:08', 1, 'Active'),
(5, 'Crop', 'Pineapple', 'Kilogram', 'Fruit', 'The pineapple is a tropical plant with an edible multiple fruit consisting of coalesced berries, also called pineapples, and the most economically significant plant in the family Bromeliaceae', '1526469571.jpg', 1000, 0, '2018-05-16 14:19:31', 6, '2018-05-24 12:32:47', 1, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `project_activities`
--

DROP TABLE IF EXISTS `project_activities`;
CREATE TABLE `project_activities` (
  `id` int(11) NOT NULL,
  `projectID` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `activity_subject` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `activity_summary` text COLLATE utf8_unicode_ci NOT NULL,
  `project_stage` int(11) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Ongoing',
  `lastupdater` int(11) NOT NULL,
  `lastupdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `project_agreements`
--

DROP TABLE IF EXISTS `project_agreements`;
CREATE TABLE `project_agreements` (
  `id` int(11) NOT NULL,
  `agreementID` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `farmer` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `productid` int(11) NOT NULL,
  `market_price` float NOT NULL,
  `farming_scheme` int(11) NOT NULL,
  `sponsorship_scheme` int(11) NOT NULL,
  `org_investment_amount` float DEFAULT NULL,
  `pvt_investment_amount` float DEFAULT NULL,
  `farmer_profit_share` float NOT NULL,
  `org_profit_share` float DEFAULT NULL,
  `farm_size` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `farm_region` int(11) NOT NULL,
  `farm_district` int(11) NOT NULL,
  `farm_ward` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `farm_street` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `farm_latitude_gps` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `farm_longitude_gps` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `pa_comments` text COLLATE utf8_unicode_ci,
  `status` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending',
  `createdby` int(11) NOT NULL,
  `createdon` int(11) NOT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `agreedby` int(11) DEFAULT NULL,
  `agreedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_agreements`
--

INSERT INTO `project_agreements` (`id`, `agreementID`, `farmer`, `productid`, `market_price`, `farming_scheme`, `sponsorship_scheme`, `org_investment_amount`, `pvt_investment_amount`, `farmer_profit_share`, `org_profit_share`, `farm_size`, `farm_region`, `farm_district`, `farm_ward`, `farm_street`, `farm_latitude_gps`, `farm_longitude_gps`, `pa_comments`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`, `agreedby`, `agreedon`) VALUES
(1, 'B796-2018-0522', '7', 3, 200, 1, 1, 0, 2000000, 80, 20, '3', 3, 41, 'Bahi', 'Bahi', '-5.9735', '35.3232', '', 'Pending', 1, 2018, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_schemes`
--

DROP TABLE IF EXISTS `project_schemes`;
CREATE TABLE `project_schemes` (
  `id` int(11) NOT NULL,
  `scheme_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_schemes`
--

INSERT INTO `project_schemes` (`id`, `scheme_name`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Block Farming', 'In this scheme farmers rent farming plots owned by kijani kibichi so as to conduct their farming practices', 'Active', 1, '2018-03-20 11:45:50', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_stages`
--

DROP TABLE IF EXISTS `project_stages`;
CREATE TABLE `project_stages` (
  `id` int(11) NOT NULL,
  `stage` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `percentage_completion` float NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `project_stages`
--

INSERT INTO `project_stages` (`id`, `stage`, `percentage_completion`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Land Preparation', 15.5, 'Effects of any disease from the previous cultivation and steps needed to minimize this impact.\r\n Fertilizers needed to bring land to its normal fertility depending upon the previous crops and fertilizer used.\r\nLayout and design of the field with respect to crop for efficient irrigation.\r\nLatest techniques for leveling the fields and their cost.', 'Active', 1, '2018-03-21 13:27:30', 1, '2018-05-21 16:43:11'),
(2, 'Harvesting', 80, 'Proper time and method for harvesting.\r\nComparative market rates.\r\nProper crop storage.\r\nCost of transportation.', 'Active', 1, '2018-03-21 13:32:42', NULL, NULL),
(3, 'Seed Sowing', 28.9, 'Appropriate time to sow the seed.\r\nOptimal weather conditions at sowing time.\r\nBest method for the sowing of seeds.\r\nSeed sowing depth.', 'Active', 1, '2018-03-21 13:34:02', 1, '2018-03-21 14:01:16'),
(4, 'Agreement Finalization', 5, 'A farmer has entered into an agreement for the particular project', 'Active', 1, '2018-03-21 16:11:01', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sponsorship_schemes`
--

DROP TABLE IF EXISTS `sponsorship_schemes`;
CREATE TABLE `sponsorship_schemes` (
  `id` int(11) NOT NULL,
  `scheme_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `createdby` int(11) NOT NULL,
  `createdon` datetime NOT NULL,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sponsorship_schemes`
--

INSERT INTO `sponsorship_schemes` (`id`, `scheme_name`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Private Sponsorship', 'All farming activities including consultations are funded by the farmer except product marketing', 'Active', 1, '2018-05-22 16:23:54', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tribes`
--

DROP TABLE IF EXISTS `tribes`;
CREATE TABLE `tribes` (
  `id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `tribes`
--

INSERT INTO `tribes` (`id`, `name`, `status`) VALUES
(1, 'Alagwa', 'Active'),
(2, 'Akiek', 'Active'),
(3, 'Akie', 'Active'),
(4, 'Arusha', 'Active'),
(5, 'Assa', 'Active'),
(6, 'Barabaig', 'Active'),
(7, 'Balouch', 'Active'),
(8, 'Bembe', 'Active'),
(9, 'Bena', 'Active'),
(10, 'Bende', 'Active'),
(11, 'Bondei', 'Active'),
(12, 'Bungu', 'Active'),
(13, 'Burunge', 'Active'),
(14, 'Chaga', 'Active'),
(15, 'Datooga', 'Active'),
(16, 'Dhaiso', 'Active'),
(17, 'Digo', 'Active'),
(18, 'Doe', 'Active'),
(19, 'Fipa', 'Active'),
(20, 'Gogo', 'Active'),
(21, 'Goa', 'Active'),
(22, 'Gorowa', 'Active'),
(23, 'Gujirati', 'Active'),
(24, 'Gweno', 'Active'),
(25, 'Ha', 'Active'),
(26, 'Hutu', 'Active'),
(27, 'Hadza', 'Active'),
(28, 'Hangaza', 'Active'),
(29, 'Haya', 'Active'),
(30, 'Hehe', 'Active'),
(31, 'Holoholo', 'Active'),
(32, 'Ikizu', 'Active'),
(33, 'Ikoma', 'Active'),
(34, 'Iraqw', 'Active'),
(35, 'Isanzu', 'Active'),
(36, 'Jiji', 'Active'),
(37, 'Jita', 'Active'),
(38, 'Kabwa', 'Active'),
(39, 'Kagura', 'Active'),
(40, 'Kaguru', 'Active'),
(41, 'Kahe', 'Active'),
(42, 'Kami', 'Active'),
(43, 'Kamba', 'Active'),
(44, 'Kara (also called Regi)', 'Active'),
(45, 'Kerewe', 'Active'),
(46, 'Kikuyu', 'Active'),
(47, 'Kimbu', 'Active'),
(48, 'Kinga', 'Active'),
(49, 'Kisankasa', 'Active'),
(50, 'Kisi', 'Active'),
(51, 'Konongo', 'Active'),
(52, 'Kuria', 'Active'),
(53, 'Kutu', 'Active'),
(54, 'Kw’adza', 'Active'),
(55, 'Kwavi', 'Active'),
(56, 'Kwaya', 'Active'),
(57, 'Kwere', 'Active'),
(58, 'Kwifa', 'Active'),
(59, 'Lambya', 'Active'),
(60, 'Luguru', 'Active'),
(61, 'Luo', 'Active'),
(62, 'Maasai', 'Active'),
(63, 'Machinga', 'Active'),
(64, 'Magoma', 'Active'),
(65, 'Mbulu', 'Active'),
(66, 'Makonde', 'Active'),
(67, 'Makua', 'Active'),
(68, 'Makwe', 'Active'),
(69, 'Malila', 'Active'),
(70, 'Mambwe', 'Active'),
(71, 'Manyema', 'Active'),
(72, 'Manda', 'Active'),
(73, 'Mahara', 'Active'),
(74, 'Mediak', 'Active'),
(75, 'Matengo', 'Active'),
(76, 'Matumbi', 'Active'),
(77, 'Maviha', 'Active'),
(78, 'Mbugwe', 'Active'),
(79, 'Mbunga', 'Active'),
(80, 'Mbugu', 'Active'),
(81, 'Meru', 'Active'),
(82, 'Mosiro', 'Active'),
(83, 'Mpoto', 'Active'),
(84, 'Msur Zanzibar', 'Active'),
(85, 'Mwanga', 'Active'),
(86, 'Mwera', 'Active'),
(87, 'Ndali', 'Active'),
(88, 'Ndamba', 'Active'),
(89, 'Ndendeule', 'Active'),
(90, 'Ndengereko', 'Active'),
(91, 'Ndonde', 'Active'),
(92, 'Nyanja', 'Active'),
(93, 'Ngas', 'Active'),
(94, 'Ngasa', 'Active'),
(95, 'Ngindo', 'Active'),
(96, 'Ngoni', 'Active'),
(97, 'Ngulu', 'Active'),
(98, 'Ngazija', 'Active'),
(99, 'Ngurimi', 'Active'),
(100, 'Ngwele', 'Active'),
(101, 'Nilamba', 'Active'),
(102, 'Nindi', 'Active'),
(103, 'Nyakyusa', 'Active'),
(104, 'Nyasa', 'Active'),
(105, 'Nyambo', 'Active'),
(106, 'Nyamwanga', 'Active'),
(107, 'Nyamwezi', 'Active'),
(108, 'Nyanyembe', 'Active'),
(109, 'Nyaturu', 'Active'),
(110, 'Nyiha', 'Active'),
(111, 'Nyiramba', 'Active'),
(112, 'Omotik', 'Active'),
(113, 'Okiek people', 'Active'),
(114, 'Pangwa', 'Active'),
(115, 'Pare', 'Active'),
(116, 'Pimbwe', 'Active'),
(117, 'Pogolo', 'Active'),
(118, 'Rangi', 'Active'),
(119, 'Rufiji', 'Active'),
(120, 'Rungi', 'Active'),
(121, 'Rungu', 'Active'),
(122, 'Rungwa', 'Active'),
(123, 'Rwa', 'Active'),
(124, 'Safwa', 'Active'),
(125, 'Sagara', 'Active'),
(126, 'Sandawe', 'Active'),
(127, 'Sangu', 'Active'),
(128, 'Segeju', 'Active'),
(129, 'Swengwear', 'Active'),
(130, 'Shambaa', 'Active'),
(131, 'Shirazi', 'Active'),
(132, 'Shubi', 'Active'),
(133, 'Sizaki', 'Active'),
(134, 'Suba', 'Active'),
(135, 'Sukuma', 'Active'),
(136, 'Sumbwa', 'Active'),
(137, 'Sungu Tanga', 'Active'),
(138, 'Swahili', 'Active'),
(139, 'Temi', 'Active'),
(140, 'Tongwe', 'Active'),
(141, 'Twa', 'Active'),
(142, 'Tutsi', 'Active'),
(143, 'Tumbuka', 'Active'),
(144, 'Vidunda', 'Active'),
(145, 'Vinza', 'Active'),
(146, 'Wanda', 'Active'),
(147, 'Washihiri', 'Active'),
(148, 'Wamanga', 'Active'),
(149, 'Wanji', 'Active'),
(150, 'Wangarenaro', 'Active'),
(151, 'Ware', 'Active'),
(152, 'Yaaku', 'Active'),
(153, 'Yao', 'Active'),
(154, 'Zanaki', 'Active'),
(155, 'Zaramo', 'Active'),
(156, 'Zigula', 'Active'),
(157, 'Zinza', 'Active'),
(158, 'Zyoba', 'Active'),
(159, 'Zulu', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `tribe` varchar(50) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `msisdn` varchar(20) NOT NULL,
  `pst_address` varchar(50) DEFAULT NULL,
  `userphoto` varchar(100) DEFAULT NULL,
  `center_residence_region` varchar(50) DEFAULT NULL,
  `center_residence_district` varchar(50) DEFAULT NULL,
  `center_residence_ward` varchar(50) DEFAULT NULL,
  `center_residence_street` varchar(50) DEFAULT NULL,
  `vehicleno` varchar(30) DEFAULT NULL,
  `driverlicense` varchar(100) DEFAULT NULL,
  `memberid` varchar(20) DEFAULT NULL,
  `password` varchar(40) NOT NULL,
  `ip_address` int(10) UNSIGNED NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `status` varchar(20) NOT NULL DEFAULT 'Active',
  `emailNotificationStatus` varchar(50) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `gender`, `tribe`, `username`, `email`, `msisdn`, `pst_address`, `userphoto`, `center_residence_region`, `center_residence_district`, `center_residence_ward`, `center_residence_street`, `vehicleno`, `driverlicense`, `memberid`, `password`, `ip_address`, `salt`, `status`, `emailNotificationStatus`, `createdon`, `createdby`, `modifiedon`, `modifiedby`) VALUES
(1, 'Emmanuel', NULL, 'Mfikwa', NULL, NULL, 'administrator', 'kemixenock@gmail.co', '0719186759', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'd8e423a9d5eb97da9e2d58cd57b92808', 0, '0', 'Active', NULL, NULL, NULL, NULL, NULL),
(2, 'Emmanuel', 'Enock', 'Mfikwa', 'Male', NULL, 'kemixenock@gmail.com', 'kemixenock@gmail.com', '255719186759', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '551162756XMQ2', '39d1f2402540ed4e8cd00a61cada8fb7', 0, '629a1c0480', 'Active', 'sent', '2018-05-12 14:49:16', NULL, NULL, NULL),
(3, 'Rose', NULL, 'Gulla', 'Female', NULL, 'rosedorothea@gmail.co', 'rosedorothea@gmail.co', '255625502273', NULL, '02Y946421C5F6.JPG', '2', '39', 'Kawe', 'Wazo', NULL, NULL, '02Y946421C5F6', 'fca12812017395ad563351e921355d94', 0, 'b2c9674ba2', 'Active', 'sent', '2018-05-12 23:51:22', 1, '2018-05-16 12:16:12', 1),
(5, 'Manuel', '', 'Enock', 'Male', NULL, 'kemixenock@yaho.com', 'kemixenock@yaho.com', '255719186759', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '01G1X50E21836', 'c5db388d16a2723060f62fce38f8ba92', 0, '32681f19aa', 'Active', 'pending', '2018-05-15 13:28:21', NULL, NULL, NULL),
(4, 'Manuel', NULL, 'Lanzini', 'Male', NULL, 'kemixenock@yahoo.co', 'kemixenock@yahoo.co', '255719186759', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1AM57660I2134', '37c5ac18312e1273a573357a09f94c47', 0, 'f607a57f56', 'Active', 'sent', '2018-05-13 00:32:14', 1, NULL, NULL),
(6, 'Afiswa', NULL, 'Mnyungu', 'Female', NULL, 'afiswa.hamidu@gmobile.co.tz', 'afiswa.hamidu@gmobile.co.tz', '255717091814', NULL, 'L1645P842165W.jpeg', '2', '40', 'Mbagala', 'Mbagala', NULL, NULL, 'L1645P842165W', 'a7bcbde14026c4ad2f705f27d750a22b', 0, 'ce89bfef8e', 'Active', 'sent', '2018-05-16 14:01:54', 1, NULL, NULL),
(7, 'Agro', NULL, 'Farmer', 'Male', NULL, 'kemixenock@yao.co', 'kemixenock@yao.co', '255719186759', NULL, '9217069645ROT.jpg', '2', '39', 'Mbezi Beach', 'Mbezi Beach', NULL, NULL, '9217069645ROT', '90c10957e30fafefef225ed0f8b2a7fc', 0, 'abe89ad9fa', 'Active', 'sent', '2018-05-22 16:11:46', 1, '2018-05-25 18:24:57', 1),
(8, 'Hery', '', 'Enock', 'Male', NULL, 'kemixenock@yahoo.com', 'kemixenock@yahoo.com', '255719186759', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '270XK1026P158', '872f36f2ead34390ef95e25b3a506961', 0, '442ab5975b', 'Active', 'sent', '2018-05-23 12:33:41', NULL, NULL, NULL),
(9, 'Agro', NULL, 'Driver', 'Male', NULL, 'emmmanuel.mfikwa@gmobile.co.tz', 'emmmanuel.mfikwa@gmobile.co.tz', '255777777', NULL, '5B2A51R127352.jpg', '2', '39', '', '', 'T123CCC', NULL, '5B2A51R127352', '051f4ecee485dfea689a12e46b4a6e68', 0, 'ea5c25e985', 'Active', 'sent', '2018-05-25 16:35:21', 1, '2018-05-25 17:09:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 4),
(5, 5, 2),
(6, 6, 3),
(7, 7, 5),
(8, 8, 2),
(9, 9, 6);

-- --------------------------------------------------------

--
-- Table structure for table `user_feedback`
--

DROP TABLE IF EXISTS `user_feedback`;
CREATE TABLE `user_feedback` (
  `id` int(11) NOT NULL,
  `emailAddress` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `subject` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `createdon` datetime NOT NULL,
  `readStatus` varchar(50) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'unread'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_feedback`
--

INSERT INTO `user_feedback` (`id`, `emailAddress`, `subject`, `message`, `createdon`, `readStatus`) VALUES
(1, 'kemixenock@gmail.com', 'FURNITURE', 'Please try adding more funritures', '2018-05-12 15:22:11', 'unread');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `agro_cart`
--
ALTER TABLE `agro_cart`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agro_delivery_cost`
--
ALTER TABLE `agro_delivery_cost`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `agro_inventory`
--
ALTER TABLE `agro_inventory`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `productbatch` (`productbatch`,`supplier`,`productid`,`reference_orderID`);

--
-- Indexes for table `agro_orders`
--
ALTER TABLE `agro_orders`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `orderid` (`orderid`);

--
-- Indexes for table `agro_order_items`
--
ALTER TABLE `agro_order_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `captcha`
--
ALTER TABLE `captcha`
  ADD PRIMARY KEY (`captcha_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `harvest_agreements`
--
ALTER TABLE `harvest_agreements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `cropname` (`productname`);

--
-- Indexes for table `project_activities`
--
ALTER TABLE `project_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_agreements`
--
ALTER TABLE `project_agreements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `agreementID` (`agreementID`);

--
-- Indexes for table `project_schemes`
--
ALTER TABLE `project_schemes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_stages`
--
ALTER TABLE `project_stages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sponsorship_schemes`
--
ALTER TABLE `sponsorship_schemes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tribes`
--
ALTER TABLE `tribes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `memberid` (`memberid`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_feedback`
--
ALTER TABLE `user_feedback`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `agro_cart`
--
ALTER TABLE `agro_cart`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `agro_delivery_cost`
--
ALTER TABLE `agro_delivery_cost`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `agro_inventory`
--
ALTER TABLE `agro_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `agro_orders`
--
ALTER TABLE `agro_orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `agro_order_items`
--
ALTER TABLE `agro_order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `captcha`
--
ALTER TABLE `captcha`
  MODIFY `captcha_id` bigint(13) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=480;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `harvest_agreements`
--
ALTER TABLE `harvest_agreements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `locations`
--
ALTER TABLE `locations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=200;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `project_activities`
--
ALTER TABLE `project_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `project_agreements`
--
ALTER TABLE `project_agreements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `project_schemes`
--
ALTER TABLE `project_schemes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `project_stages`
--
ALTER TABLE `project_stages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sponsorship_schemes`
--
ALTER TABLE `sponsorship_schemes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tribes`
--
ALTER TABLE `tribes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user_feedback`
--
ALTER TABLE `user_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
