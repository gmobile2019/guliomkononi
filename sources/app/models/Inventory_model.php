<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Inventory_model
 *
 * @author gmobile
 */
class Inventory_model extends CI_Model{
    
    private $shopping;
    
    public function __construct()
    {
            parent::__construct();
            $this->shopping=$this->load->database('shopping',TRUE);//load shopping database configuration
    }
    
    function inventory_stock_info_count($product,$supplier,$batch,$availability){
        
        
        if($product <> null){
            
           $where .=" AND p.productname LIKE '%$product%'"; 
        }
        
        if($supplier <> null){
            
           $where .=" AND ai.supplier='$supplier'"; 
        }
        
        if($batch <> null){
            
           $where .=" AND ai.productbatch LIKE '%$batch%'"; 
        }
        
        if($availability <> null){
            
           $where .=" AND ai.availableDate >='$availability'"; 
        }
        
        return count($this->shopping->query("SELECT SUM(ai.quantity) AS quantity,p.productname,ai.productbatch,ai.supplier "
                                    . "FROM agro_inventory AS ai "
                                    . "INNER JOIN products AS p ON p.id=ai.productid "
                                    . "WHERE ai.id is not null $where "
                                    . "GROUP BY p.productname,ai.productbatch,ai.supplier "
                                    . "ORDER BY p.productname,ai.productbatch ASC")->result());
    }
    
    function inventory_stock_info($product,$supplier,$batch,$availability,$page,$limit){
        
        if($product <> null){
            
           $where .=" AND (p.productname LIKE '%$product%')"; 
        }
        
        if($supplier <> null){
            
           $where .=" AND ai.supplier='$supplier'"; 
        }
        
        if($batch <> null){
            
           $where .=" AND ai.productbatch LIKE '%$batch%'"; 
        }
        
        if($availability <> null){
            
           $where .=" AND ai.availableDate >='$availability'"; 
        }
        
        return $this->shopping->query("SELECT SUM(ai.quantity) AS quantity,ai.productid,p.productname,ai.productbatch,ai.supplier "
                                    . "FROM agro_inventory AS ai "
                                    . "INNER JOIN products AS p ON p.id=ai.productid "
                                    . "WHERE ai.id is not null $where "
                                    . "GROUP BY ai.productid,p.productname,ai.productbatch,ai.supplier "
                                    . "ORDER BY p.productname,ai.productbatch ASC "
                                    . "LIMIT $page,$limit")->result();
    }
    
    function inventory_stock($product,$supplier,$batch,$availability){
        
        if($product <> null){
            
           $where .=" AND (p.productname LIKE '%$product%')"; 
        }
        
        if($supplier <> null){
            
           $where .=" AND ai.supplier='$supplier'"; 
        }
        
        if($batch <> null){
            
           $where .=" AND ai.productbatch LIKE '%$batch%'"; 
        }
        
        if($availability <> null){
            
           $where .=" AND ai.availableDate >='$availability'"; 
        }
        
        return $this->shopping->query("SELECT SUM(ai.quantity) AS quantity,ai.productid,p.productname,ai.productbatch,ai.supplier "
                                    . "FROM agro_inventory AS ai "
                                    . "INNER JOIN products AS p ON p.id=ai.productid "
                                    . "WHERE ai.id is not null $where "
                                    . "GROUP BY ai.productid,p.productname,ai.productbatch,ai.supplier "
                                    . "ORDER BY p.productname,ai.productbatch ASC")->result();
    }
    
    
    function inventory_details_info_count($product,$supplier,$batch,$status,$start,$end){
        
        if($product <> null){
            
           $where .=" AND p.productname LIKE '%$product%'"; 
        }
        
        if($supplier <> null){
            
           $where .=" AND ai.supplier='$supplier'"; 
        }
        
        if($batch <> null){
            
           $where .=" AND ai.productbatch LIKE '%$batch%'"; 
        }
        
        if($status <> NULL){
            
            $where .=" AND ai.status LIKE '$status'"; 
        }
        
        if($start <> NULL){
            
            $where .=" AND ai.createdon >='$start 00:00:00'"; 
        }
        
        if($end <> NULL){
            
            $where .=" AND ai.createdon <='$end 23:59:59'"; 
        }
       
        return count($this->shopping->query("SELECT ai.id,ai.quantity,ai.status,ai.reference_txn,"
                                    . "ai.reference_orderID,ai.productbatch,ai.comments,"
                                    . "DATE_FORMAT(ai.createdon,'%d/%m/%Y %h:%i %p') AS createdon,DATE_FORMAT(ai.availableDate,'%d %M %Y') AS availableDate,"
                                    . "DATE_FORMAT(ai.availableDate,'%Y-%m-%d') AS availableDate2,ai.availabilityPeriod,"
                                    . "p.productname,CONCAT(u.first_name,' ',u.last_name) AS supplier "
                                    . "FROM agro_inventory AS ai "
                                    . "INNER JOIN products AS p ON p.id=ai.productid "
                                    . "INNER JOIN users AS u ON u.id=ai.supplier "
                                    . "WHERE ai.id is not null AND ai.status IN ('Sold','Reserved') $where "
                                    . "ORDER BY ai.createdon DESC")->result());
    }
    
    function inventory_details_info($product,$supplier,$batch,$status,$start,$end,$page,$limit){
        
        if($product <> null){
            
           $where .=" AND p.productname LIKE '%$product%'"; 
        }
        
        if($supplier <> null){
            
           $where .=" AND ai.supplier='$supplier'"; 
        }
        
        if($batch <> null){
            
           $where .=" AND ai.productbatch LIKE '%$batch%'"; 
        }
        
        if($status <> NULL){
            
            $where .=" AND ai.status LIKE '$status'"; 
        }
        
        if($start <> NULL){
            
            $where .=" AND ai.createdon >='$start 00:00:00'"; 
        }
        
        if($end <> NULL){
            
            $where .=" AND ai.createdon <='$end 23:59:59'"; 
        }
       
        return $this->shopping->query("SELECT ai.id,ai.quantity,ai.status,ai.reference_txn,"
                                    . "ai.reference_orderID,ai.productbatch,ai.comments,"
                                    . "DATE_FORMAT(ai.createdon,'%d/%m/%Y %h:%i %p') AS createdon,DATE_FORMAT(ai.availableDate,'%d %M %Y') AS availableDate,"
                                    . "DATE_FORMAT(ai.availableDate,'%Y-%m-%d') AS availableDate2,ai.availabilityPeriod,"
                                    . "p.productname,p.productunit,CONCAT(u.first_name,' ',u.last_name) AS supplier "
                                    . "FROM agro_inventory AS ai "
                                    . "INNER JOIN products AS p ON p.id=ai.productid "
                                    . "INNER JOIN users AS u ON u.id=ai.supplier "
                                    . "WHERE ai.id is not null AND ai.status IN ('Sold','Reserved') $where "
                                    . "ORDER BY ai.createdon DESC "
                                    . "LIMIT $page,$limit")->result();
    }
    
    function inventory_details($product,$supplier,$batch,$status,$referenceOrderID){
        
        if($product <> null){
            
           $where .=" AND ai.productid='$product'"; 
        }
        
        if($supplier <> null){
            
           $where .=" AND ai.supplier='$supplier'"; 
        }
        
        if($batch <> null){
            
           $where .=" AND ai.productbatch='$batch'"; 
        }
        
        if($status <> NULL){
            
            $where .=" AND ai.status='$status'"; 
        }
        
        if($referenceOrderID <> NULL){
            
            $where .=" AND ai.reference_orderID='$referenceOrderID'"; 
        }
       
        return $this->shopping->query("SELECT ai.id,ai.quantity,ai.status,ai.reference_txn,"
                                    . "ai.reference_orderID,ai.productbatch,ai.comments,"
                                    . "DATE_FORMAT(ai.createdon,'%d/%m/%Y %h:%i %p') AS createdon,DATE_FORMAT(ai.availableDate,'%d %M %Y') AS availableDate,"
                                    . "DATE_FORMAT(ai.availableDate,'%Y-%m-%d') AS availableDate2,ai.availabilityPeriod,"
                                    . "p.productname,p.productunit,CONCAT(u.first_name,' ',u.last_name) AS supplier "
                                    . "FROM agro_inventory AS ai "
                                    . "INNER JOIN products AS p ON p.id=ai.productid "
                                    . "INNER JOIN users AS u ON u.id=ai.supplier "
                                    . "WHERE ai.id is not null $where "
                                    . "ORDER BY ai.createdon DESC")->result();
    }
    
    function available_stock($product,$supplier){
        
        if($product <> null){
            
           $where .=" AND ai.productid='$product'"; 
        }
        
        if($supplier <> null){
            
           $where .=" AND ai.supplier='$supplier'"; 
        }
        
        return $this->shopping->query("SELECT SUM(ai.quantity) AS qty,ai.supplier,ai.productid,ai.productbatch,DATE_FORMAT(ai.availableDate,'%d %M %Y') AS availableDate "
                                    . "FROM agro_inventory AS ai "
                                    . "WHERE ai.id is not null $where "
                                    . "GROUP BY ai.supplier,ai.productid,ai.productbatch,ai.availableDate "
                                    . "ORDER BY ai.productid,ai.supplier,ai.productbatch,ai.availableDate ASC")->result();
    }
    
    function total_stock($productid,$supplierid,$productbatch,$referenceOrderId){
        
        if($productid <> null){
            
           $where .=" AND ai.productid='$productid'"; 
        }
        
        if($supplierid <> null){
            
           $where .=" AND ai.supplier='$supplierid'"; 
        }
        
        if($productbatch <> null){
            
           $where .=" AND ai.productbatch='$productbatch'"; 
        }
        
        if($referenceOrderId <> null){
            
           $where .=" AND ai.reference_orderID='$referenceOrderId'"; 
        }
        
        return $this->shopping->query("SELECT SUM(ai.quantity) AS ttlStk FROM agro_inventory AS ai WHERE ai.id is not null $where")->row();
    }
    
    function save_product_stock($data){
        
        return $this->shopping->insert('agro_inventory',$data);
    }
    
    function save_cart_details($data,$id){
        
        if($id <> NULL){
            
            return $this->shopping->update('agro_cart',$data,array('id'=>$id));
        }
        
        return $this->shopping->insert('agro_cart',$data);
    }
    
    function agro_cart_items($id,$productid,$status,$buyer){
        
        if($id <> NULL){
            
            $where .=" AND ac.id='$id'";
        }
        
        if($productid <> NULL){
            
            $where .=" AND ac.productid='$productid'";
        }
        
        if($status <> NULL){
            
            $where .=" AND ac.status='$status'";
        }
        
        if($buyer <> NULL){
            
            $where .=" AND ac.createdby='$buyer'";
        }
        
        return $this->shopping->query("SELECT ac.id,ac.productid,ac.quantity,ac.productprice,"
                                    . "ac.status,ac.refOrderID,DATE_FORMAT(ac.createdon,'%d/%m/%Y %h:%i %p') AS createdon,"
                                    . "p.productname,p.productimage,p.productunit "
                                    . "FROM agro_cart AS ac "
                                    . "INNER JOIN products AS p ON ac.productid=p.id "
                                    . "WHERE ac.id is not NULL $where "
                                    . "ORDER BY ac.createdon DESC")->result();
    }
    
    function remove_cart_agro_product($id){
        
        return $this->shopping->delete('agro_cart',array('id'=>$id,'Status'=>'Active'));
    }
    
    function outOfStockProducts(){
      
        return $this->shopping->query("SELECT SUM(ai.quantity) AS qty,p.productname "
                                    . "FROM agro_inventory as ai "
                                    . "INNER JOIN products as p "
                                    . "ON p.id=ai.productid "
                                    . "WHERE "
                                    . "(SELECT SUM(ai2.quantity) FROM agro_inventory ai2 WHERE ai2.productid=ai.productid) <= p.stocklimit "
                                    . "AND p.stocklimit is not null "
                                    . "GROUP BY p.productname "
                                    . "ORDER BY p.productname")->result();
    }
    
}
