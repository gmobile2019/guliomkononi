<?php defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of SystemMonitor_model
 *
 * @author gmobile
 */
class SystemMonitor_model extends CI_Model {
   
    
    private $shopping;
    
    public function __construct()
    {
            parent::__construct();
            $this->shopping=$this->load->database('shopping',TRUE);//load shopping database configuration
    }
    
    function cartItems($status,$limittimestamp){
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        if($limittimestamp <> NULL){
            
            $where .=" AND createdon <='$limittimestamp'";
        }
        
        return $this->shopping->query("SELECT id FROM agro_cart WHERE refOrderID is null $where ORDER BY createdon ASC LIMIT 0,500")->result();
    }
    
    function cancel_cartItem($ids){
        
        
        return $this->shopping->query("UPDATE agro_cart SET status='cancelled',reason='cart item has not been processed within ".$this->config->item('cart_item_expiry_hours')." hours' WHERE id IN ($ids) AND status='active'");
    }
    
    function agro_orders($status,$limittimestamp){
        
        if($status <> NULL){
            
            $where .=" AND orderstatus='$status'";
        }
        
        if($limittimestamp <> NULL){
            
            $where .=" AND orderdate <='$limittimestamp'";
        }
       
        return $this->shopping->query("SELECT orderid FROM agro_orders WHERE id is not null $where ORDER BY orderdate ASC LIMIT 0,500")->result();
    }
    
    function agro_order_items($orderid){
        
        return $this->shopping->query("SELECT id FROM agro_order_items WHERE orderid='$orderid'")->result();
    }
    
    function cancel_order($orderids,$itemids){
        
        $this->shopping->trans_start();
        $this->shopping->query("DELETE FROM agro_inventory WHERE reference_orderID IN ($itemids) AND status='Reserved'");
        $this->shopping->query("UPDATE agro_order_items SET status='cancelled' WHERE id IN ($itemids) AND status='confirmed'");
        $this->shopping->query("UPDATE agro_cart SET status='cancelled' WHERE refOrderID IN ($orderids) AND status='processed'");
        $this->shopping->query("UPDATE agro_orders SET orderstatus='cancelled',reason='order has not been paid for within ".$this->config->item('order_expiry_hours')." hours' WHERE orderid IN ($orderids) AND orderstatus='confirmed'");
        $this->shopping->trans_complete();
        
        return $this->shopping->trans_status();
    }
    
    function agro_messages($status){
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        return $this->shopping->query("SELECT id,textmessage,textrecipient FROM agro_messages WHERE id is not null $where ORDER BY createdon DESC LIMIT 0,500")->result();
    }
    
    function update_agro_message($ids){
        
        return $this->shopping->query("UPDATE agro_messages SET status='sent' WHERE id IN ($ids) AND status='pending'");
    }
}
