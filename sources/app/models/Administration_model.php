<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Administration_model
 *
 * @author gmobile
 */
class Administration_model extends CI_Model {
    
    private $shopping;
    
    public function __construct()
    {
            parent::__construct();
            $this->shopping=$this->load->database('shopping',TRUE);//load shopping database configuration
    }
    
    function current_user_info(){

           return $this->shopping->query("SELECT u.first_name,u.middle_name,u.last_name "
                   . "FROM users AS u "
                   . "WHERE u.id='".$this->session->userdata('user_id')."'")->row();
    }
    
    function profile_data(){
        
        return $this->shopping->query("SELECT users.id,users.password,users.salt,users.first_name,users.middle_name,"
                . "users.last_name,users.username,users.email,"
                . "users.msisdn,users_groups.group_id "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id "
                . "WHERE users.id='".$this->session->userdata('user_id')."'")->row();
    }
    
    function users_info_count($name,$group){
        
        if($name <> null){
            
           $where .=" AND (u.first_name LIKE '%$name%' OR u.middle_name LIKE '%$name%' OR u.last_name LIKE '%$name%')"; 
        }
        
        if($group <> null){
            
            $where .=" AND ug.group_id='$group'"; 
        }
        
        return count($this->shopping->query("SELECT u.id,u.first_name,u.userphoto,u.gender,u.memberid,"
                . "u.middle_name,u.last_name,u.email,u.center_residence_region,"
                . "u.center_residence_district,u.center_residence_ward,u.center_residence_street,"
                . "u.msisdn,u.username,u.status,ug.group_id,g.name AS grp,"
                . "lreg.name AS region,ldist.name AS district,t.name AS tribe "
                . "FROM users AS u "
                . "INNER JOIN users_groups AS ug ON u.id=ug.user_id "
                . "INNER JOIN groups AS g ON g.id=ug.group_id "
                . "LEFT OUTER JOIN locations AS lreg ON lreg.id=u.center_residence_region "
                . "LEFT OUTER JOIN locations AS ldist ON ldist.id=u.center_residence_district "
                . "LEFT OUTER JOIN tribes AS t ON t.id=u.tribe "
                . "WHERE u.id is not null $where ")->result());
    }
    
    function users_info($name,$group,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (u.first_name LIKE '%$name%' OR u.middle_name LIKE '%$name%' OR u.last_name LIKE '%$name%')"; 
        }
        
        if($group <> null){
            
            $where .=" AND ug.group_id='$group'"; 
        }
     
        return $this->shopping->query("SELECT u.id,u.first_name,u.userphoto,u.gender,u.memberid,"
                . "u.middle_name,u.last_name,u.email,u.center_residence_region,u.vehicleno,"
                . "u.center_residence_district,u.center_residence_ward,u.center_residence_street,"
                . "u.msisdn,u.username,u.status,ug.group_id,g.name AS grp,"
                . "lreg.name AS region,ldist.name AS district,t.name AS tribe "
                . "FROM users AS u "
                . "INNER JOIN users_groups AS ug ON u.id=ug.user_id "
                . "INNER JOIN groups AS g ON g.id=ug.group_id "
                . "LEFT OUTER JOIN locations AS lreg ON lreg.id=u.center_residence_region "
                . "LEFT OUTER JOIN locations AS ldist ON ldist.id=u.center_residence_district "
                . "LEFT OUTER JOIN tribes AS t ON t.id=u.tribe "
                . "WHERE u.id is not null $where "
                . "ORDER BY u.first_name,u.middle_name,u.last_name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function get_member_info($id,$username,$memberID,$group){
        
        if($id <> null){
            
            $where =" AND u.id='$id'";
            
        }
        
        if($username <> null){
            
            $where =" AND u.username='$username'";
            
        }
        
        if($memberID <> null){
            
            $where =" AND u.memberid='$memberID'";
            
        }
        
        if($group <> null){
            
            $where =" AND ug.group_id='$group'";
            
        }
       
        return $this->shopping->query("SELECT u.id,u.first_name,u.userphoto,u.gender,u.memberid,"
                . "u.middle_name,u.last_name,u.email,u.center_residence_region,u.vehicleno,"
                . "u.center_residence_district,u.center_residence_ward,u.center_residence_street,"
                . "u.msisdn,u.username,u.status,ug.group_id,g.name AS grp,"
                . "lreg.name AS region,ldist.name AS district,t.name AS tribe "
                . "FROM users AS u "
                . "INNER JOIN users_groups AS ug ON u.id=ug.user_id "
                . "INNER JOIN groups AS g ON g.id=ug.group_id "
                . "LEFT OUTER JOIN locations AS lreg ON lreg.id=u.center_residence_region "
                . "LEFT OUTER JOIN locations AS ldist ON ldist.id=u.center_residence_district "
                . "LEFT OUTER JOIN tribes AS t ON t.id=u.tribe "
                . "WHERE u.id is not null $where "
                . "ORDER BY u.first_name,u.middle_name,u.last_name ASC ")->result();
    }
    
    function tribes($id,$status){
        
        
        if($id <> NULL){
            
            $where .=" AND id='$id'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        
        
         return $this->shopping->query("SELECT id,name,status FROM tribes WHERE name is not null $where ORDER BY name ASC ")->result();
    }
    
    function groups($id,$user){
        
        if($id <> null){
            
            $where =" AND id='$id'";
            
        }
        
        if($user == 'administrator'){
            
           $where .=" AND id IN ('".$this->config->item('admin_group_id')."',"
                    . "'".$this->config->item('manager_group_id')."',"
                    . "'".$this->config->item('supplier_group_id')."',"
                    . "'".$this->config->item('buyer_group_id')."',"
                    . "'".$this->config->item('farmer_group_id')."',"
                   . "'".$this->config->item('agent_group_id')."',"
                   . "'".$this->config->item('driver_group_id')."')";
            
        }else{
            
            $where .=" AND id IN ('".$this->config->item('manager_group_id')."',"
                    . "'".$this->config->item('supplier_group_id')."',"
                    . "'".$this->config->item('farmer_group_id')."',"
                    . "'".$this->config->item('agent_group_id')."',"
                    . "'".$this->config->item('driver_group_id')."')";
            
        }
       
        return $this->shopping->query("SELECT id,name,description FROM groups WHERE name is not NULL $where ORDER BY name ASC")->result();
    }
    
    function activate_deactivate_user($id,$status){
        
            $status=$status == 'Suspended'?'Active':'Suspended';
            return  $this->shopping->update('users',array('status'=>$status),array('id'=>$id));
    }
    
    function save_user_details($data,$id,$username){
        
        
        if($id <> NULL){
            
            return $this->shopping->update('users',$data,array('id'=>$id));
        }
        
        if($username <> NULL){
            
            return $this->shopping->update('users',$data,array('username'=>$username));
        }
        
        return $this->shopping->insert('users',$data);
    }
    
    function save_user_feedback($data,$id){
        
        if($id <> NULL){
            
            return $this->shopping->update('user_feedback',$data,array('id'=>$id));
        }
       
        return $this->shopping->insert('user_feedback',$data);
    }
    
    function locations($id,$category,$parent_region){
        
        
        if($id <> NULL){
            
            $where .=" AND id='$id'";
        }
        
        if($category <> NULL){
            
            $where .=" AND category='$category'";
        }
        
        if($parent_region <> NULL){
            
            $where .=" AND parent_region='$parent_region'";
        }
        
         return $this->shopping->query("SELECT id,name,category,parent_region FROM locations WHERE name is not null $where ORDER BY name ASC ")->result();
    }
    
    function products_info_count($name,$productcategory,$status,$producttype){
        
        if($name <> null){
            
           $where .=" AND p.productname LIKE '%$name%'"; 
        }
        
        if($productcategory <> null){
            
            $where .=" AND p.productcategory='$productcategory'"; 
        }
        
        if($status <> null){
            
            $where .=" AND p.status='$status'"; 
        }
        
        if($producttype <> null){
            
            $where .=" AND p.producttype='$producttype'"; 
        }
        
        return count($this->shopping->query("SELECT p.id,p.productunit,p.producttype,p.productname,p.productimage,"
                . "p.productcategory,p.description,DATE_FORMAT(p.createdon,'%d/%m/%Y %h:%i %p') AS createdon,"
                . "DATE_FORMAT(p.modifiedon,'%d/%m/%Y %h:%i %p') AS modifiedon,p.status,p.productprice,p.productdiscount,"
                . "CONCAT(u.first_name,' ',u.last_name) AS creator,CONCAT(u2.first_name,' ',u2.last_name) AS modifier "
                . "FROM products AS p "
                . "INNER JOIN users AS u ON p.createdby=u.id "
                . "LEFT OUTER JOIN users AS u2 ON p.modifiedby=u2.id "
                . "WHERE p.id is not null $where "
                . "ORDER BY p.productname ASC ")->result());
    }
    
    function products_info($name,$productcategory,$status,$producttype,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND p.productname LIKE '%$name%'"; 
        }
        
        if($productcategory <> null){
            
            $where .=" AND p.productcategory='$productcategory'"; 
        }
        
        if($status <> null){
            
            $where .=" AND p.status='$status'"; 
        }
        
        if($producttype <> null){
            
            $where .=" AND p.producttype='$producttype'"; 
        }
     
        return $this->shopping->query("SELECT p.id,p.productunit,p.producttype,p.productname,p.productimage,"
                . "p.productcategory,p.description,DATE_FORMAT(p.createdon,'%d/%m/%Y %h:%i %p') AS createdon,"
                . "DATE_FORMAT(p.modifiedon,'%d/%m/%Y %h:%i %p') AS modifiedon,p.status,p.productprice,p.productdiscount,"
                . "CONCAT(u.first_name,' ',u.last_name) AS creator,CONCAT(u2.first_name,' ',u2.last_name) AS modifier "
                . "FROM products AS p "
                . "INNER JOIN users AS u ON p.createdby=u.id "
                . "LEFT OUTER JOIN users AS u2 ON p.modifiedby=u2.id "
                . "WHERE p.id is not null $where "
                . "ORDER BY p.productname ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function products($id,$productname,$productcategory,$status,$producttype){
        
        if($id <> null){
            
            $where =" AND p.id='$id'";
            
        }
        
        if($productname <> null){
            
           $where .=" AND p.productname LIKE '%$productname%'"; 
        }
        
        if($productcategory <> null){
            
            $where .=" AND p.productcategory='$productcategory'"; 
        }
        
        if($status <> null){
            
            $where .=" AND p.status='$status'"; 
        }
        
        if($producttype <> null){
            
            $where .=" AND p.producttype='$producttype'"; 
        }
       
        return $this->shopping->query("SELECT p.id,p.productunit,p.producttype,p.productname,p.productimage,"
                . "p.productcategory,p.description,DATE_FORMAT(p.createdon,'%d/%m/%Y %h:%i %p') AS createdon,"
                . "DATE_FORMAT(p.modifiedon,'%d/%m/%Y %h:%i %p') AS modifiedon,p.status,p.productprice,p.productdiscount,p.stocklimit,"
                . "CONCAT(u.first_name,' ',u.last_name) AS creator,CONCAT(u2.first_name,' ',u2.last_name) AS modifier "
                . "FROM products AS p "
                . "INNER JOIN users AS u ON p.createdby=u.id "
                . "LEFT OUTER JOIN users AS u2 ON p.modifiedby=u2.id "
                . "WHERE p.id is not null $where "
                . "ORDER BY p.productname ASC ")->result();
    }
    
    function save_product($data,$id){
        
        if($id <> NULL){
            
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->shopping->update('products',$data,array('id'=>$id));
        }

            $data['createdby']=$this->session->userdata('user_id');
            $data['createdon']=date('Y-m-d H:i:s');
            return $this->shopping->insert('products',$data);
    }
    
    function activate_deactivate_product($id,$status){
        
            $status=$status == 'Suspended'?'Active':'Suspended';
            return  $this->shopping->update('products',array('status'=>$status),array('id'=>$id));
    }
    
    function remove_product_request($id){
        
            return  $this->shopping->delete('products',array('id'=>$id,'Status'=>'Pending'));
    }
    
    
    function product_suppliers($id,$status){
        
        if($id <> NULL){
            
            $where .=" AND u.id='$id'";
        }
        
        if($status <> NULL){
            
            $where .=" AND u.status='$status'";
        }
        
        return $this->shopping->query("SELECT u.id,CONCAT(u.first_name,' ',u.last_name) AS supplier,u.status,ug.group_id "
                . "FROM users AS u "
                . "INNER JOIN users_groups AS ug ON u.id=ug.user_id "
                . "WHERE group_id IN ('".$this->config->item('farmer_group_id')."','".$this->config->item('supplier_group_id')."') "
                . "$where "
                . "ORDER BY u.first_name ASC ")->result();
    }
    
    function agro_farmers($id,$status){
        
        if($id <> NULL){
            
            $where .=" AND u.id='$id'";
        }
        
        if($status <> NULL){
            
            $where .=" AND u.status='$status'";
        }
        
        return $this->shopping->query("SELECT u.id,CONCAT(u.first_name,' ',u.last_name) AS farmer,u.status,ug.group_id "
                . "FROM users AS u "
                . "INNER JOIN users_groups AS ug ON u.id=ug.user_id "
                . "WHERE group_id IN ('".$this->config->item('farmer_group_id')."') "
                . "$where "
                . "ORDER BY u.first_name ASC ")->result();
    }
    
    function project_schemes_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (scheme_name LIKE '%$name%')"; 
        }
        
        return count($this->shopping->query("SELECT id,scheme_name,description,status "
                                        . "FROM project_schemes "
                                        . "WHERE id is not null $where ORDER BY scheme_name ASC"
                                        )->result());
    }
    
    function project_schemes_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (scheme_name LIKE '%$name%')"; 
        }
     
        return $this->shopping->query("SELECT id,scheme_name,description,status "
                                    . "FROM project_schemes "
                                    . "WHERE id is not null $where ORDER BY scheme_name ASC "
                                    . "LIMIT $page,$limit"
                                    )->result();
    } 
    
    function project_schemes($id,$status){
        
        if($id <> NULL){
            
            $where .=" AND id='$id'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        return $this->shopping->query("SELECT id,scheme_name,description,status "
                                    . "FROM project_schemes "
                                    . "WHERE id is not null $where ORDER BY scheme_name ASC")->result();
    }
    
    function activate_deactivate_project_scheme($id,$status){
        
            $status=$status == 'Suspended'?'Active':'Suspended';
            return  $this->shopping->update('project_schemes',array('status'=>$status),array('id'=>$id));
    }
    
    function add_project_scheme($data,$id){
        
        if($id <> NULL){
                   $data['modifiedby']=$this->session->userdata('user_id');
                   $data['modifiedon']=date('Y-m-d H:i:s');
                   
            return $this->shopping->update('project_schemes',$data,array('id'=>$id));
        }
               $data['createdby']=$this->session->userdata('user_id');
               $data['createdon']=date('Y-m-d H:i:s');
        return $this->shopping->insert('project_schemes',$data);
    }
    
    function sponsorship_schemes_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (scheme_name LIKE '%$name%')"; 
        }
        
        return count($this->shopping->query("SELECT id,scheme_name,description,status "
                                        . "FROM sponsorship_schemes "
                                        . "WHERE id is not null $where ORDER BY scheme_name ASC"
                                        )->result());
    }
    
    function sponsorship_schemes_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (scheme_name LIKE '%$name%')"; 
        }
     
        return $this->shopping->query("SELECT id,scheme_name,description,status "
                                    . "FROM sponsorship_schemes "
                                    . "WHERE id is not null $where ORDER BY scheme_name ASC "
                                    . "LIMIT $page,$limit"
                                    )->result();
    } 
    
    function sponsorship_schemes($id,$status){
        
        if($id <> NULL){
            
            $where .=" AND id='$id'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        return $this->shopping->query("SELECT id,scheme_name,description,status "
                                    . "FROM sponsorship_schemes "
                                    . "WHERE id is not null $where ORDER BY scheme_name ASC")->result();
    }
    
    function activate_deactivate_sponsorship_scheme($id,$status){
        
            $status=$status == 'Suspended'?'Active':'Suspended';
            return  $this->shopping->update('sponsorship_schemes',array('status'=>$status),array('id'=>$id));
    }
    
    function add_sponsorship_scheme($data,$id){
        
        if($id <> NULL){
                   $data['modifiedby']=$this->session->userdata('user_id');
                   $data['modifiedon']=date('Y-m-d H:i:s');
                   
            return $this->shopping->update('sponsorship_schemes',$data,array('id'=>$id));
        }
               $data['createdby']=$this->session->userdata('user_id');
               $data['createdon']=date('Y-m-d H:i:s');
        return $this->shopping->insert('sponsorship_schemes',$data);
    }
    
    function project_stages($id,$stage,$status){
        
        if($id <> NULL){
            
            $where .=" AND id='$id'";
        }
        
        if($stage <> NULL){
            
            $where .=" AND stage LIKE '%$stage%'";
        }
        
        if($status <> NULL){
            
            $where .=" AND status='$status'";
        }
        
        return $this->shopping->query("SELECT id,stage,percentage_completion,description,status "
                                    . "FROM project_stages "
                                    . "WHERE id is not null $where ORDER BY percentage_completion ASC")->result();
    }
    
    function add_project_stage($data,$id){
        
        if($id <> NULL){
                   $data['modifiedby']=$this->session->userdata('user_id');
                   $data['modifiedon']=date('Y-m-d H:i:s');
                   
            return $this->shopping->update('project_stages',$data,array('id'=>$id));
        }
               $data['createdby']=$this->session->userdata('user_id');
               $data['createdon']=date('Y-m-d H:i:s');
        return $this->shopping->insert('project_stages',$data);
    }
    
    function activate_deactivate_project_stage($id,$status){
        
            $status=$status == 'Suspended'?'Active':'Suspended';
            return  $this->shopping->update('project_stages',array('status'=>$status),array('id'=>$id));
    }
    
    function project_agreements_info_count($farmer,$product,$status,$start,$end,$frmr){
        
        if($farmer <> null){
            
           $where .=" AND (u.first_name LIKE '%$farmer%' OR u.last_name LIKE '%$farmer%')"; 
        }
        
        if($product <> null){
            
           $where .=" AND (pr.productname LIKE '%$product%')"; 
        }
        
        if($status <> null){
            
           $where .=" AND pa.status LIKE '$status'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pa.createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pa.createdon<='$end'"; 
        }
        
        if($frmr <> null){
            
           $where .=" AND pa.farmer='$frmr'"; 
        }
        
        return count($this->shopping->query("SELECT pa.id,pa.farmer,pa.agreementID,pa.market_price,pa.org_investment_amount,"
                                    . "pa.pvt_investment_amount,pa.farmer_profit_share,pa.org_profit_share,pa.farm_ward,pa.farm_street,pa.farm_latitude_gps,"
                                    . "pa.farm_longitude_gps,pa.status,pa.pa_comments,pa.farm_size,DATE_FORMAT(pa.agreedon,'%d/%m/%Y %h:%i %p') AS agreedon,"
                                    . "CONCAT(u.first_name,' ',u.last_name) AS fullname,"
                                    . "ps.scheme_name AS farming_scheme,ss.scheme_name AS sp_scheme,"
                                    . "pr.productname,pr.productunit,l.name AS region,l2.name AS district "
                                    . "FROM project_agreements AS pa "
                                    . "INNER JOIN users AS u ON pa.farmer=u.id "
                                    . "INNER JOIN products AS pr ON pa.productid=pr.id "
                                    . "INNER JOIN project_schemes AS ps ON pa.farming_scheme=ps.id "
                                    . "INNER JOIN sponsorship_schemes AS ss ON pa.sponsorship_scheme=ss.id "
                                    . "INNER JOIN locations AS l ON pa.farm_region=l.id "
                                    . "INNER JOIN locations AS l2 ON pa.farm_district=l2.id "
                                    . "WHERE pa.id is not null $where ")->result());
    }
    
    function project_agreements_info($farmer,$product,$status,$start,$end,$frmr,$page,$limit){
        
        if($farmer <> null){
            
           $where .=" AND (u.first_name LIKE '%$farmer%' OR u.last_name LIKE '%$farmer%')"; 
        }
        
        if($crop <> null){
            
           $where .=" AND (pr.productname LIKE '%$product%')"; 
        }
        
        if($status <> null){
            
           $where .=" AND pa.status LIKE '$status'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pa.createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pa.createdon<='$end'"; 
        }
        
        if($frmr <> null){
            
           $where .=" AND pa.farmer='$frmr'"; 
        }
        
        return $this->shopping->query("SELECT pa.id,pa.farmer,pa.agreementID,pa.market_price,pa.org_investment_amount,"
                                    . "pa.pvt_investment_amount,pa.farmer_profit_share,pa.org_profit_share,pa.farm_ward,pa.farm_street,pa.farm_latitude_gps,"
                                    . "pa.farm_longitude_gps,pa.status,pa.pa_comments,pa.farm_size,DATE_FORMAT(pa.agreedon,'%d/%m/%Y %h:%i %p') AS agreedon,"
                                    . "CONCAT(u.first_name,' ',u.last_name) AS fullname,"
                                    . "ps.scheme_name AS farming_scheme,ss.scheme_name AS sp_scheme,"
                                    . "pr.productname,pr.productunit,l.name AS region,l2.name AS district "
                                    . "FROM project_agreements AS pa "
                                    . "INNER JOIN users AS u ON pa.farmer=u.id "
                                    . "INNER JOIN products AS pr ON pa.productid=pr.id "
                                    . "INNER JOIN project_schemes AS ps ON pa.farming_scheme=ps.id "
                                    . "INNER JOIN sponsorship_schemes AS ss ON pa.sponsorship_scheme=ss.id "
                                    . "INNER JOIN locations AS l ON pa.farm_region=l.id "
                                    . "INNER JOIN locations AS l2 ON pa.farm_district=l2.id "
                                    . "WHERE pa.id is not null $where ORDER BY pa.createdon DESC "
                                    . "LIMIT $page,$limit"
                                    )->result();
    } 
    
    function project_agreements($id,$farmer,$product,$status,$start,$end,$frmr){
        
        if($id <> NULL){
            
            $where .=" AND pa.agreementID='$id'";
        }
        
        if($farmer <> null){
            
           $where .=" AND (u.first_name LIKE '%$farmer%' OR u.last_name LIKE '%$farmer%')"; 
        }
        
        if($product <> null){
            
           $where .=" AND (pr.productname LIKE '%$product%')"; 
        }
        
        if($status <> null){
            
           $where .=" AND pa.status LIKE '$status'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pa.createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pa.createdon<='$end'"; 
        }
        
        if($frmr <> null){
            
           $where .=" AND pa.farmer='$frmr'"; 
        }
        
        return $this->shopping->query("SELECT pa.id,pa.farmer,pa.productid,pa.agreementID,pa.market_price,pa.org_investment_amount,"
                                    . "pa.pvt_investment_amount,pa.farmer_profit_share,pa.org_profit_share,pa.farm_region,pa.farm_district,pa.farm_ward,pa.farm_street,pa.farm_latitude_gps,"
                                    . "pa.farm_longitude_gps,pa.status,pa.pa_comments,pa.farming_scheme AS f_scheme,pa.sponsorship_scheme AS s_scheme,pa.farm_size,"
                                    . "DATE_FORMAT(pa.agreedon,'%d/%m/%Y %h:%i %p') AS agreedon,DATE_FORMAT(pa.createdon,'%d/%m/%Y %h:%i %p') AS createdon,"
                                    . "CONCAT(u.first_name,' ',u.last_name) AS fullname,"
                                    . "ps.scheme_name AS farming_scheme,ss.scheme_name AS sp_scheme,"
                                    . "pr.productname,pr.productunit,l.name AS region,l2.name AS district "
                                    . "FROM project_agreements AS pa "
                                    . "INNER JOIN users AS u ON pa.farmer=u.id "
                                    . "INNER JOIN products AS pr ON pa.productid=pr.id "
                                    . "INNER JOIN project_schemes AS ps ON pa.farming_scheme=ps.id "
                                    . "INNER JOIN sponsorship_schemes AS ss ON pa.sponsorship_scheme=ss.id "
                                    . "INNER JOIN locations AS l ON pa.farm_region=l.id "
                                    . "INNER JOIN locations AS l2 ON pa.farm_district=l2.id "
                                    . "WHERE pa.id is not null $where ORDER BY pa.createdon DESC")->result();
    }
    
    function activate_deactivate_project_agreement($id,$status){
        
            $status=$status == 'Suspended'?'Active':'Suspended';
            return  $this->shopping->update('project_agreements',array('status'=>$status),array('id'=>$id));
    }
    
    function add_project_agreement($data,$id){
        
        if($id <> NULL){
                   $data['modifiedby']=$this->session->userdata('user_id');
                   $data['modifiedon']=date('Y-m-d H:i:s');
                   
            return $this->shopping->update('project_agreements',$data,array('agreementID'=>$id));
        }
               $data['createdby']=$this->session->userdata('user_id');
               $data['createdon']=date('Y-m-d H:i:s');
        return $this->shopping->insert('project_agreements',$data);
    }
    
    function confirm_project_agreement($data,$id){
        
        return $this->shopping->update('project_agreements',$data,array('agreementID'=>$id));
    }
    
    function project_activities_info_count($projectID,$stage,$status,$start,$end,$farmer){
        
        if($projectID <> null){
            
           $where .=" AND pac.projectID LIKE '%$projectID%'"; 
        }
        
        if($stage <> null){
            
           $where .=" AND pac.project_stage LIKE '$stage'"; 
        }
        
        if($status <> null){
            
           $where .=" AND pac.status LIKE '$status'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pac.lastupdate >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pac.lastupdate <='$end'"; 
        }
        
        if($farmer <> null){
            
           $where .=" AND pa.farmer='$farmer'"; 
        }
        
        return count($this->shopping->query("SELECT pac.id,pac.projectID,pac.id AS pjid,pac.activity_subject,pac.activity_summary,pac.project_stage,"
                                        . "pac.status,pac.lastupdater,DATE_FORMAT(pac.lastupdate,'%d/%m/%Y %h:%i %p') AS lastupdate,CONCAT(u.first_name,' ',u.last_name) AS fullname,ps.stage,"
                                        . "ps.percentage_completion,pa.farmer "
                                        . "FROM project_activities AS pac "
                                        . "INNER JOIN users AS u ON pac.lastupdater=u.id "
                                        . "INNER JOIN project_stages AS ps ON pac.project_stage=ps.id "
                                        . "INNER JOIN project_agreements AS pa ON pac.projectID=pa.agreementID "
                                        . "WHERE pac.id is not null $where"
                                        )->result());
    }
    
    function project_activities_info($projectID,$stage,$status,$start,$end,$farmer,$page,$limit){
        
        if($projectID <> null){
            
           $where .=" AND pac.projectID LIKE '%$projectID%'"; 
        }
        
        if($stage <> null){
            
           $where .=" AND pac.project_stage LIKE '$stage'"; 
        }
        
        if($status <> null){
            
           $where .=" AND pac.status LIKE '$status'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pa.createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pa.createdon<='$end'"; 
        }
        
        if($farmer <> null){
            
           $where .=" AND pa.farmer='$farmer'"; 
        }
     
        return $this->shopping->query("SELECT pac.id,pac.projectID,pac.id AS pjid,pac.activity_subject,pac.activity_summary,pac.project_stage,"
                                    . "pac.status,pac.lastupdater,DATE_FORMAT(pac.lastupdate,'%d/%m/%Y %h:%i %p') AS lastupdate2,pac.lastupdate,CONCAT(u.first_name,' ',u.last_name) AS fullname,ps.stage,"
                                    . "ps.percentage_completion,pa.farmer "
                                    . "FROM project_activities AS pac "
                                    . "INNER JOIN users AS u ON pac.lastupdater=u.id "
                                    . "INNER JOIN project_stages AS ps ON pac.project_stage=ps.id "
                                    . "INNER JOIN project_agreements AS pa ON pac.projectID=pa.agreementID "
                                    . "WHERE pac.id is not null $where ORDER BY pac.lastupdate DESC "
                                    . "LIMIT $page,$limit"
                                    )->result();
    } 
    
    function project_activities($id,$projectID,$stage,$status,$start,$end,$farmer){
        
        if($id <> NULL){
            
            $where .=" AND pac.id='$id'";
        }
        
        if($projectID <> null){
            
           $where .=" AND pac.projectID LIKE '%$projectID%'"; 
        }
        
        if($stage <> null){
            
           $where .=" AND pac.project_stage LIKE '$stage'"; 
        }
        
        if($status <> null){
            
           $where .=" AND pac.status LIKE '$status'"; 
        }
        
        if($start <> null){
            
           $where .=" AND pa.createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND pa.createdon<='$end'"; 
        }
        
        if($farmer <> null){
            
           $where .=" AND pa.farmer='$farmer'"; 
        }
        
        return $this->shopping->query("SELECT pac.id,pac.projectID,pac.id AS pjid,pac.activity_subject,pac.activity_summary,pac.project_stage,"
                                    . "pac.status,pac.lastupdater,DATE_FORMAT(pac.lastupdate,'%d/%m/%Y %h:%i %p') AS lastupdate,CONCAT(u.first_name,' ',u.last_name) AS fullname,ps.stage,"
                                    . "ps.percentage_completion,pa.farmer "
                                    . "FROM project_activities AS pac "
                                    . "INNER JOIN users AS u ON pac.lastupdater=u.id "
                                    . "INNER JOIN project_stages AS ps ON pac.project_stage=ps.id "
                                    . "INNER JOIN project_agreements AS pa ON pac.projectID=pa.agreementID "
                                    . "WHERE pac.id is not null $where ORDER BY pac.lastupdate DESC")->result();
    }
    
    function remove_project_activity($id){
        
            return  $this->shopping->delete('project_activities',array('id'=>$id));
    }
    
    function add_project_activity($data,$id){
        
        if($id <> NULL){
                   
            return $this->shopping->update('project_activities',$data,array('id'=>$id));
        }
        
        return $this->shopping->insert('project_activities',$data);
    }
    
    function running_projects(){
        
        return $this->shopping->query("SELECT pa.agreementID,pac.project_stage "
                                    . "FROM project_agreements AS pa "
                                    . "LEFT OUTER JOIN project_activities AS pac ON pa.agreementID=pac.projectID "
                                    . "WHERE pa.status='Closed' AND pac.status NOT IN ('Completed') "
                                    . "ORDER BY pa.createdon DESC")->result();
    }
    
    function harvest_agreements_info_count($project,$crop,$status,$start,$end,$farmer){
        
        if($project <> null){
            
           $where .=" AND ha.projectID LIKE '%$project%'"; 
        }
        
        if($crop <> null){
            
           $where .=" AND (cr.common_name LIKE '%$crop%' OR cr.scientific_name LIKE '%$crop%')"; 
        }
        
        if($status <> null){
            
           $where .=" AND ha.status LIKE '$status'"; 
        }
        
        if($start <> null){
            
           $where .=" AND ha.createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND ha.createdon<='$end'"; 
        }
        
        if($farmer <> null){
            
           $where .=" AND pa.farmer='$farmer'"; 
        }
        
        return count($this->shopping->query("SELECT ha.id,ha.projectID,ha.harvestQuantity,ha.market_price,ha.suitableQuantity,ha.expected_return,"
                                    . "ha.unsuitableQuantity,ha.status,ha.ha_comments,DATE_FORMAT(ha.agreedon,'%d/%m/%Y %h:%i %p') AS agreedon,pa.farmer,cr.common_name,cr.measurement_unit,CONCAT(u.first_name,' ',u.last_name) AS farmername "
                                    . "FROM harvest_agreements AS ha "
                                    . "INNER JOIN project_agreements AS pa ON pa.agreementID=ha.projectID "
                                    . "INNER JOIN crops AS cr ON cr.crop_id=ha.cropID "
                                    . "LEFT OUTER JOIN users AS u ON u.id=ha.agreedby "
                                    . "WHERE ha.id is not null $where"
                                        )->result());
    }
    
    function harvest_agreements_info($project,$crop,$status,$start,$end,$farmer,$page,$limit){
        
        if($project <> null){
            
           $where .=" AND ha.projectID LIKE '%$project%'"; 
        }
        
        if($crop <> null){
            
           $where .=" AND (cr.common_name LIKE '%$crop%' OR cr.scientific_name LIKE '%$crop%')"; 
        }
        
        if($status <> null){
            
           $where .=" AND ha.status LIKE '$status'"; 
        }
        
        if($start <> null){
            
           $where .=" AND ha.createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND ha.createdon<='$end'"; 
        }
        
        if($farmer <> null){
            
           $where .=" AND pa.farmer='$farmer'"; 
        }
        
        return $this->shopping->query("SELECT ha.id,ha.projectID,ha.harvestQuantity,ha.market_price,ha.suitableQuantity,ha.expected_return,"
                                    . "ha.unsuitableQuantity,ha.status,ha.ha_comments,DATE_FORMAT(ha.agreedon,'%d/%m/%Y %h:%i %p') AS agreedon,pa.farmer,pa.crop,cr.common_name,cr.measurement_unit,CONCAT(u.first_name,' ',u.last_name) AS farmername "
                                    . "FROM harvest_agreements AS ha "
                                    . "INNER JOIN project_agreements AS pa ON pa.agreementID=ha.projectID "
                                    . "INNER JOIN crops AS cr ON cr.crop_id=ha.cropID "
                                    . "LEFT OUTER JOIN users AS u ON u.id=ha.agreedby "
                                    . "WHERE ha.id is not null $where ORDER BY ha.createdon DESC "
                                    . "LIMIT $page,$limit"
                                    )->result();
    } 
    
    function harvest_agreements($id,$projectID,$crop,$status,$start,$end,$farmer){
        
        if($id <> null){
            
           $where .=" AND ha.id='$id'"; 
        }
        
        if($projectID <> null){
            
           $where .=" AND ha.projectID='$projectID'"; 
        }
        
        if($crop <> null){
            
           $where .=" AND (cr.common_name LIKE '%$crop%' OR cr.scientific_name LIKE '%$crop%')"; 
        }
        
        if($status <> null){
            
           $where .=" AND ha.status LIKE '$status'"; 
        }
        
        if($start <> null){
            
           $where .=" AND ha.createdon >='$start'"; 
        }
        
        if($end <> null){
            
           $where .=" AND ha.createdon<='$end'"; 
        }
        
        if($farmer <> null){
            
           $where .=" AND pa.farmer='$farmer'"; 
        }
        
        return $this->shopping->query("SELECT ha.id,ha.projectID,ha.harvestQuantity,ha.market_price,ha.suitableQuantity,ha.expected_return,"
                                    . "ha.unsuitableQuantity,ha.status,ha.ha_comments,DATE_FORMAT(ha.agreedon,'%d/%m/%Y %h:%i %p') AS agreedon,pa.farmer,pa.crop,cr.common_name,cr.measurement_unit,CONCAT(u.first_name,' ',u.last_name) AS farmername "
                                    . "FROM harvest_agreements AS ha "
                                    . "INNER JOIN project_agreements AS pa ON pa.agreementID=ha.projectID "
                                    . "INNER JOIN crops AS cr ON cr.crop_id=ha.cropID "
                                    . "LEFT OUTER JOIN users AS u ON u.id=ha.agreedby "
                                    . "WHERE ha.id is not null $where ORDER BY ha.createdon DESC")->result();
    }
    
    function harvest_candidate_agreements(){
        
        return $this->shopping->query("SELECT pa.agreementID,ha.id "
                                    . "FROM project_agreements AS pa "
                                    . "LEFT OUTER JOIN harvest_agreements AS ha ON pa.agreementID=ha.projectID "
                                    . "WHERE pa.status='Closed' AND (ha.status NOT LIKE 'Closed' OR ha.status is null) ORDER BY pa.createdon DESC")->result();
    }
    
    function add_harvest_agreement($data,$id){
        
        if($id <> NULL){
                   $data['modifiedby']=$this->session->userdata('user_id');
                   $data['modifiedon']=date('Y-m-d H:i:s');
                   
            return $this->shopping->update('harvest_agreements',$data,array('id'=>$id));
        }
               $data['createdby']=$this->session->userdata('user_id');
               $data['createdon']=date('Y-m-d H:i:s');
        return $this->shopping->insert('harvest_agreements',$data);
    }
    
    function delivery_destinations_info_count($destination,$status){
        
        if($destination <> NULL){
            
            $where .=" AND dst.destination LIKE '%$destination%'";
        }
        
        if($status <> NULL){
            
            $where .=" AND dst.status LIKE '$status'";
        }
        
        return count($this->shopping->query("SELECT dst.id,dst.destination,dst.cost,dst.status "
                                    . "FROM agro_delivery_cost AS dst "
                                    . "WHERE dst.id is not null $where")->result());
    }
    
    function delivery_destinations_info($destination,$status,$page,$limit){
        
        if($destination <> NULL){
            
            $where .=" AND dst.destination LIKE '%$destination%'";
        }
        
        if($status <> NULL){
            
            $where .=" AND dst.status LIKE '$status'";
        }
        
        return $this->shopping->query("SELECT dst.id,dst.destination,dst.cost,dst.status "
                                    . "FROM agro_delivery_cost AS dst "
                                    . "WHERE dst.id is not null $where "
                                    . "ORDER BY dst.destination "
                                    . "LIMIT $page,$limit")->result();
    }
    
    function delivery_destinations($id,$destination,$status){
        
        if($id <> NULL){
           
            $where .=" AND dst.id='$id'";
        }
        
        if($destination <> NULL){
            
            $where .=" AND dst.destination LIKE '%$destination%'";
        }
        
        if($status <> NULL){
            
            $where .=" AND dst.status LIKE '$status'";
        }
        
        return $this->shopping->query("SELECT dst.id,dst.destination,dst.cost,dst.status "
                                    . "FROM agro_delivery_cost AS dst "
                                    . "WHERE dst.id is not null $where "
                                    . "ORDER BY dst.destination")->result();
    }
    
    function add_delivery_destination($data,$id){
        
        if($id <> NULL){
                   $data['modifiedby']=$this->session->userdata('user_id');
                   $data['modifiedon']=date('Y-m-d H:i:s');
                   
            return $this->shopping->update('agro_delivery_cost',$data,array('id'=>$id));
        }
               $data['createdby']=$this->session->userdata('user_id');
               $data['createdon']=date('Y-m-d H:i:s');
        return $this->shopping->insert('agro_delivery_cost',$data);
    }
    
    function agro_orders_info_count($orderid,$dlrdst,$status,$buyer,$agent,$start,$end){
        
        if($orderid <> NULL){
            
            $where .=" AND ao.orderid LIKE '%$orderid%'";
        }
        if($dlrdst <> NULL){
            
            $where .=" AND ao.deliverydestination LIKE '%$dlrdst%'";
        }
        
        if($status <> NULL){
            
            $where .=" AND ao.orderstatus='$status'";
        }
        
        if($buyer <> NULL){
            
            $where .=" AND u.id='$buyer'";
        }
        
        if($agent <> NULL){
            
            $where .=" AND (u2.first_name LIKE '%$agent%' OR u2.last_name LIKE '%$agent%')";
        }
        
        if($start <> NULL){
            
            $where .=" AND ao.orderdate >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND ao.orderdate <='$end 23:59:59'";
        }
        
        
        return count($this->shopping->query("SELECT ao.orderid,ao.deliverytype,ao.deliverydestination,ao.deliverycost,"
                                    . "ao.productcost,ao.orderedby,"
                                    . "DATE_FORMAT(ao.orderdate,'%d/%m/%Y %h:%i %p') AS orderdate,ao.orderstatus,"
                                    . "CONCAT(u.first_name,' ',u.last_name) AS buyer,"
                                    . "CONCAT(u2.first_name,' ',u2.last_name) AS agent "
                                    . "FROM agro_orders AS ao "
                                    . "INNER JOIN users AS u ON u.id=ao.orderedby "
                                    . "LEFT OUTER JOIN users AS u2 ON u2.id=ao.modifiedby "
                                    . "WHERE ao.id is not null $where "
                                    . "ORDER BY orderdate DESC")->result());
    }
    
    function agro_orders_info($orderid,$dlrdst,$status,$buyer,$agent,$start,$end,$page,$limit){
      
        if($orderid <> NULL){
            
            $where .=" AND ao.orderid LIKE '%$orderid%'";
        }
        if($dlrdst <> NULL){
            
            $where .=" AND ao.deliverydestination LIKE '%$dlrdst%'";
        }
        
        if($status <> NULL){
            
            $where .=" AND ao.orderstatus='$status'";
        }
        
        if($buyer <> NULL){
            
            $where .=" AND u.id='$buyer'";
        }
        
        if($agent <> NULL){
            
            $where .=" AND (u2.first_name LIKE '%$agent%' OR u2.last_name LIKE '%$agent%')";
        }
        
        if($start <> NULL){
            
            $where .=" AND ao.orderdate >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND ao.orderdate <='$end 23:59:59'";
        }
        
        return $this->shopping->query("SELECT ao.orderid,ao.deliverytype,ao.deliverydestination,ao.deliverycost,"
                                    . "ao.productcost,ao.orderedby,"
                                    . "DATE_FORMAT(ao.orderdate,'%d/%m/%Y %h:%i %p') AS orderdate,ao.orderstatus,"
                                    . "CONCAT(u.first_name,' ',u.last_name) AS buyer,"
                                    . "CONCAT(u2.first_name,' ',u2.last_name) AS agent "
                                    . "FROM agro_orders AS ao "
                                    . "INNER JOIN users AS u ON u.id=ao.orderedby "
                                    . "LEFT OUTER JOIN users AS u2 ON u2.id=ao.modifiedby "
                                    . "WHERE ao.id is not null $where "
                                    . "ORDER BY orderdate DESC "
                                    . "LIMIT $page,$limit")->result();
    }
    
    function agro_orders($id,$orderid,$dlrdst,$status,$buyer,$agent,$start,$end){
        
        if($id <> NULL){
            
            $where .=" AND ao.id='$id'";
        }
        
        if($orderid <> NULL){
            
            $where .=" AND ao.orderid LIKE '%$orderid%'";
        }
        if($dlrdst <> NULL){
            
            $where .=" AND ao.deliverydestination LIKE '%$dlrdst%'";
        }
        
        if($status <> NULL){
            
            $where .=" AND ao.orderstatus='$status'";
        }
        
        if($buyer <> NULL){
            
            $where .=" AND (u.first_name LIKE '%$buyer%' OR u.last_name LIKE '%$buyer%')";
        }
        
        if($agent <> NULL){
            
            $where .=" AND (u2.first_name LIKE '%$agent%' OR u2.last_name LIKE '%$agent%')";
        }
        
        if($start <> NULL){
            
            $where .=" AND ao.orderdate >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND ao.orderdate <='$end 23:59:59'";
        }
        
        return $this->shopping->query("SELECT ao.orderid,ao.deliverytype,ao.deliverydestination,ao.deliverycost,"
                                    . "ao.productcost,ao.orderedby,ao.refTransaction,"
                                    . "DATE_FORMAT(ao.orderdate,'%d/%m/%Y %h:%i %p') AS orderdate,ao.orderstatus,"
                                    . "CONCAT(u.first_name,' ',u.last_name) AS buyer,"
                                    . "CONCAT(u2.first_name,' ',u2.last_name) AS agent "
                                    . "FROM agro_orders AS ao "
                                    . "INNER JOIN users AS u ON u.id=ao.orderedby "
                                    . "LEFT OUTER JOIN users AS u2 ON u2.id=ao.modifiedby "
                                    . "WHERE ao.id is not null $where "
                                    . "ORDER BY orderdate DESC")->result();
    }
    
    function agro_order_items($orderid,$productid,$id,$status){
        
        if($orderid <> NULL){
            
            $where .=" AND aoi.orderid='$orderid'";
        }
        
        if($productid <> NULL){
            
            $where .=" AND aoi.productid='$productid'";
        }
        
        if($id <> NULL){
            
            $where .=" AND aoi.id='$id'";
        }
        
        if($status <> NULL){
            
            $where .=" AND aoi.status='$status'";
        }
       
        return $this->shopping->query("SELECT aoi.productprice,aoi.productqty,aoi.id AS aoiID,aoi.productid,"
                                    . "aoi.orderid,aoi.status,ao.deliverytype,ao.deliverydestination,ao.deliverycost,"
                                    . "ao.productcost,ao.orderedby,DATE_FORMAT(ao.deliverydate,'%d %M %Y') AS deliverydate,"
                                    . "DATE_FORMAT(ao.orderdate,'%d/%m/%Y %h:%i %p') AS orderdate,ao.orderstatus,"
                                    . "CONCAT(u.first_name,' ',u.last_name) AS buyer,u.msisdn AS buyercontact,"
                                    . "CONCAT(u2.first_name,' ',u2.last_name) AS agent,"
                                    . "CONCAT(u3.first_name,' ',u3.last_name) AS drivername,u3.vehicleno,"
                                    . "p.productname,p.productunit,p.productimage "
                                    . "FROM agro_order_items AS aoi "
                                    . "INNER JOIN agro_orders AS ao ON ao.orderid=aoi.orderid "
                                    . "INNER JOIN products AS p ON p.id=aoi.productid "
                                    . "INNER JOIN users AS u ON u.id=ao.orderedby "
                                    . "LEFT OUTER JOIN users AS u2 ON u2.id=ao.modifiedby "
                                    . "LEFT OUTER JOIN users AS u3 ON u3.id=ao.deliverydriver "
                                    . "WHERE ao.id is not null $where "
                                    . "ORDER BY p.productname ASC ")->result();
    }
    
    function save_order($agro_items,$agro_order,$agro_cart,$message){
        
        $this->shopping->trans_start();
        $this->shopping->insert_batch('agro_order_items',$agro_items);
        $this->shopping->insert('agro_orders',$agro_order);
        $this->shopping->insert('agro_messages',$message);
        $this->shopping->update('agro_cart',$agro_cart,array('createdby'=>$this->session->userdata('user_id')));
        $this->shopping->trans_complete();

       return $this->shopping->trans_status();
    }
    
    function update_order($orderdata,$orderitems,$message,$orderid){
        
        $this->shopping->trans_start();
        $this->shopping->update('agro_orders',$orderdata,array('orderid'=>$orderid));
        $this->shopping->update('agro_order_items',$orderitems,array('orderid'=>$orderid,'status'=>'pending'));
        $this->shopping->insert('agro_messages',$message);
        $this->shopping->trans_complete();
        
        return $this->shopping->trans_status();
    }
    
    function cancel_order($data,$orderid){
        
        $this->shopping->trans_start();
        $this->shopping->update('agro_orders',$data,array('orderid'=>$orderid));
        $this->shopping->query("UPDATE agro_order_items SET status='confirmed',modifiedby='".$this->session->userdata('user_id')."',modifiedon='".date('Y-m-d H:i:s')."' WHERE id IN ($items) AND status='pending'");
        $this->shopping->insert('agro_messages',$message);
        $this->shopping->trans_complete();
        
        return $this->shopping->trans_status();
    }
    
    function remove_agro_item_order($id,$agro_item,$message){
        
        
        $this->shopping->trans_start();
        $this->shopping->query("DELETE FROM agro_inventory WHERE reference_orderID='$id'");
        $this->shopping->query("UPDATE agro_order_items SET status='cancelled',modifiedby='".$this->session->userdata('user_id')."',modifiedon='".date('Y-m-d H:i:s')."' WHERE id='$id' AND status='pending'");
        $this->shopping->query("UPDATE agro_cart SET status='cancelled',modifiedby='".$this->session->userdata('user_id')."',modifiedon='".date('Y-m-d H:i:s')."' WHERE refOrderID='".$agro_item[0]->orderid."' AND productid='".$agro_item[0]->productid."'");
        $this->shopping->query("UPDATE agro_orders SET productcost=productcost-".$agro_item[0]->productprice.",modifiedby='".$this->session->userdata('user_id')."',modifiedon='".date('Y-m-d H:i:s')."' WHERE orderid='".$agro_item[0]->orderid."'");
        $this->shopping->insert('agro_messages',$message);
        $this->shopping->trans_complete();
        
        return $this->shopping->trans_status();
    }
    
    function save_transaction($data,$message,$order_items_id){
        
        $this->shopping->trans_start();
        $this->shopping->insert('agro_transactions',$data);
        $this->shopping->query("UPDATE agro_inventory SET status='Sold',reference_txn='".$data['transactionid']."' WHERE reference_orderID IN ($order_items_id)");
        $this->shopping->query("UPDATE agro_order_items SET status='paid',modifiedon='".date('Y-m-d H:i:s')."' WHERE orderid='".$data['orderid']."'");
        $this->shopping->query("UPDATE agro_orders SET modifiedon='".date('Y-m-d H:i:s')."',orderstatus='paid',refTransaction='".$data['transactionid']."' WHERE orderid='".$data['orderid']."'");
        $this->shopping->insert('agro_messages',$message);
        $this->shopping->trans_complete();
        
        return $this->shopping->trans_status();
    }
    
    function agro_transactions_info_count($orderid,$txnid,$msisdn,$start,$end,$buyer){
        
        if($orderid <> NULL){
            
            $where .=" AND at.orderid LIKE '%$orderid%'";
        }
        
        if($txnid <> NULL){
            
            $where .=" AND at.transactionid LIKE '%$txnid%'";
        }
        
        if($msisdn <> NULL){
            
            $where .=" AND at.msisdn='$msisdn'";
        }
        
        if($buyer <> NULL){
            
            $where .=" AND u.id='$buyer' ";
        }
        
        
        if($start <> NULL){
            
            $where .=" AND at.transactiondate >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND at.transactiondate <='$end 23:59:59'";
        }
        
        
        return count($this->shopping->query("SELECT at.id,at.transactionid,ao.orderid,at.channel,"
                                    . "at.amount,ao.orderedby,at.msisdn,"
                                    . "DATE_FORMAT(at.transactiondate,'%d/%m/%Y %h:%i %p') AS transactiondate,"
                                    . "CONCAT(u.first_name,' ',u.last_name) AS buyer "
                                    . "FROM agro_transactions AS at "
                                    . "INNER JOIN agro_orders AS ao ON ao.orderid=at.orderid "
                                    . "INNER JOIN users AS u ON u.id=ao.orderedby "
                                    . "WHERE at.id is not null $where "
                                    . "ORDER BY at.transactiondate DESC")->result());
    }
    
    function agro_transactions_info($orderid,$txnid,$msisdn,$start,$end,$buyer,$page,$limit){
      
       if($orderid <> NULL){
            
            $where .=" AND at.orderid LIKE '%$orderid%'";
        }
        
        if($txnid <> NULL){
            
            $where .=" AND at.transactionid LIKE '%$txnid%'";
        }
        
        if($msisdn <> NULL){
            
            $where .=" AND at.msisdn='$msisdn'";
        }
        
        if($buyer <> NULL){
            
            $where .=" AND u.id='$buyer' ";
        }
        
        
        if($start <> NULL){
            
            $where .=" AND at.transactiondate >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND at.transactiondate <='$end 23:59:59'";
        }
        
        return $this->shopping->query("SELECT at.id,ao.orderid,at.transactionid,at.channel,"
                                    . "at.amount,ao.orderedby,at.msisdn,"
                                    . "DATE_FORMAT(at.transactiondate,'%d/%m/%Y %h:%i %p') AS transactiondate,"
                                    . "CONCAT(u.first_name,' ',u.last_name) AS buyer "
                                    . "FROM agro_transactions AS at "
                                    . "INNER JOIN agro_orders AS ao ON ao.orderid=at.orderid "
                                    . "INNER JOIN users AS u ON u.id=ao.orderedby "
                                    . "WHERE at.id is not null $where "
                                    . "ORDER BY at.transactiondate DESC "
                                    . "LIMIT $page,$limit")->result();
    }
    
    function agro_transactions($id,$orderid,$txnid,$msisdn,$start,$end,$buyer){
        
        if($id <> NULL){
            
            $where .=" AND at.id='$id'";
        }
        
        if($orderid <> NULL){
            
            $where .=" AND at.orderid LIKE '%$orderid%'";
        }
        
        if($txnid <> NULL){
            
            $where .=" AND at.transactionid LIKE '%$txnid%'";
        }
        
        if($msisdn <> NULL){
            
            $where .=" AND at.msisdn='$msisdn'";
        }
        
        if($buyer <> NULL){
            
            $where .=" AND u.id='$buyer' ";
        }
        
        
        if($start <> NULL){
            
            $where .=" AND at.transactiondate >='$start 00:00:00'";
        }
        
        if($end <> NULL){
            
            $where .=" AND at.transactiondate <='$end 23:59:59'";
        }
        
        return $this->shopping->query("SELECT at.id,ao.orderid,at.transactionid,at.channel,"
                                    . "at.amount,ao.orderedby,at.msisdn,"
                                    . "DATE_FORMAT(at.transactiondate,'%d/%m/%Y %h:%i %p') AS transactiondate,"
                                    . "CONCAT(u.first_name,' ',u.last_name) AS buyer "
                                    . "FROM agro_transactions AS at "
                                    . "INNER JOIN agro_orders AS ao ON ao.orderid=at.orderid "
                                    . "INNER JOIN users AS u ON u.id=ao.orderedby "
                                    . "WHERE at.id is not null $where "
                                    . "ORDER BY at.transactiondate DESC")->result();
    }
}
