<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author gmobile
 */
class Agent extends CI_Controller{
   
    function __construct(){
        
            parent::__construct();
            $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        $this->data['activeMenu']='homemenu';
        $this->data['activeLink']='';
        $this->data['title']='Home';
        $this->data['content']='agent/home';
        $this->load->view('agent/template',$this->data);
    }
    
    function user_details($id){
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
       $this->data['activeMenu']='usersmenu';
       $this->data['activeLink']='viewuserslnk';
       $this->data['title']="User Details";
       $this->data['member']=$this->Administration_model->get_member_info($id,NULL,NULL,NULL);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='agent/user_details';
       $this->load->view('agent/template',$this->data);
    }
    
    function crops(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->input->post('categ')) {
            $key['categ'] = $this->input->post('categ');
            $this->data['categ']=$this->input->post('categ');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }


       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['categ'] = $exp[3];
            $this->data['categ']=$key['categ'];
            
            $key['status'] = $exp[5];
            $this->data['status']=$key['status'];

        }

        $name =$key['name'];
        $categ =$key['categ'];
        $status =$key['status'];

        $config["base_url"] = base_url() . "Agent/crops/name_".$key['name']."_categ_".$key['categ']."_status_".$key['status']."/";
        $config["total_rows"] =$this->Administration_model->products_info_count($name,$categ,$status,$this->config->item('crop_product_identifier'));
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->products_info($name,$categ,$status,$this->config->item('crop_product_identifier'),$page,$limit);
                
        $this->data['activeMenu']='productsmenu';
        $this->data['activeLink']='sellingcropslnk';
        $this->data['title']='Crops';
        $this->data['searchform']='searchforms/agent/sellingcrops_agent_search_form';
        $this->data['content']='agent/crops';
        $this->load->view('agent/template',$this->data);
    }
    
    function delivery_destinations(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('destination')) {
            $key['destination'] = $this->input->post('destination');
            $this->data['destination']=$this->input->post('destination');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['destination'] = $exp[1];
            $this->data['destination']=$key['destination'];
            
            $key['status'] = $exp[3];
            $this->data['status']=$key['status'];
            
        }
        
        $destination =$key['destination'];
        $status =$key['status'];
        
        $config["base_url"] = base_url() . "Agent/delivery_destinations/dst_".$key['destination']."_st_".$key['status']."/";
        $config["total_rows"] =$this->Administration_model->delivery_destinations_info_count($destination,$status);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->delivery_destinations_info($destination,$status,$page,$limit);
        
        $this->session->set_userdata('prev_url_path',base_url() . "Agent/delivery_destinations/dst_".$key['destination']."_st_".$key['status']."/".$page);
        $this->data['activeMenu']='servicesmenu';
        $this->data['activeLink']='dlrdstlnk';
        $this->data['searchform']='searchforms/agent/delivery_destination_search_form';
        $this->data['title']='Delivery Destinations';
        $this->data['content']='agent/delivery_destinations';
        $this->load->view('agent/template',$this->data);
    }
    
    function add_delivery_destination($id){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
        $this->form_validation->set_rules('destination','Destination Name','trim|required|xss_clean');
        $this->form_validation->set_rules('cost','Delivery Cost','trim|required|xss_clean|numeric|greater_than[0]');
        
        if ($this->form_validation->run() == TRUE){
           
            $data=array(
                'destination'=>$this->input->post('destination'),
                'cost'=>$this->input->post('cost'),
            );

           
            $resp=$this->Administration_model->add_delivery_destination($data,$id);
            if($resp){

                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">data not saved!</div>';
            }
        }
        
       $this->data['activeMenu']='servicesmenu';
       $this->data['activeLink']='dlrdstlnk';
       $this->data['title']="Add/Edit Delivery Destination";
       $this->data['id']=$id;
       $this->data['dlr_dst']=$this->Administration_model->delivery_destinations($id,NULL,NULL);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='agent/add_delivery_destination';
       $this->load->view('agent/template',$this->data);
    }
    
    function orders(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('orderid')) {
            $key['orderid'] = $this->input->post('orderid');
            $this->data['orderid']=$this->input->post('orderid');
        }
        
        if ($this->input->post('dst')) {
            $key['dst'] = $this->input->post('dst');
            $this->data['dst']=$this->input->post('dst');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->input->post('buyer')) {
            $key['buyer'] = $this->input->post('buyer');
            $this->data['buyer']=$this->input->post('buyer');
        }
        
        if ($this->input->post('from')) {
            $key['from'] = $this->input->post('from');
            $this->data['from']=$this->input->post('from');
        }
        
        if ($this->input->post('to')) {
            $key['to'] = $this->input->post('to');
            $this->data['to']=$this->input->post('to');
        }
        

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['orderid'] = $exp[1];
            $this->data['orderid']=$key['orderid'];
            
            $key['dst'] = $exp[3];
            $this->data['dst']=$key['dst'];
            
            $key['status'] = $exp[5];
            $this->data['status']=$key['status'];
            
            $key['buyer'] = $exp[7];
            $this->data['buyer']=$key['buyer'];
            
            $key['from'] = $exp[9];
            $this->data['from']=$key['from'];
            
            $key['to'] = $exp[11];
            $this->data['to']=$key['to'];
            
            $doc= $exp[13];
        }

        $orderid =$key['orderid'];
        $dst =$key['dst'];
        $status =$key['status'];
        $buyer =$key['buyer'];
        $from =$key['from'];
        $to =$key['to'];

        if($doc == 1){
            
            $data=$this->Administration_model->agro_orders(NULL,$orderid,$dst,$status,$buyer,NULL,$from,$to);
            require_once 'reports/orders_pdf.php';
        }
        
        $config["base_url"] = base_url() . "Agent/orders/ord_".$key['orderid']."_dst_".$key['dst']."_status_".$key['status']."_by_".$key['buyer']."_st_".$key['from']."_end_".$key['to']."/";
        $config["total_rows"] =$this->Administration_model->agro_orders_info_count($orderid,$dst,$status,$buyer,NULL,$from,$to);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->agro_orders_info($orderid,$dst,$status,$buyer,NULL,$from,$to,$page,$limit);
        
        $this->session->set_userdata('prev_url_path',base_url() . "Agent/orders/ord_".$key['orderid']."_dst_".$key['dst']."_status_".$key['status']."_by_".$key['buyer']."_st_".$key['from']."_end_".$key['to']."/".$page);        
        $this->data['destinations'] = $this->Administration_model->delivery_destinations(NULL,NULL,'Active');
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['title']='Orders';
        $this->data['searchform']='searchforms/agent/orders_search_form';
        $this->data['content']='agent/orders';
        $this->load->view('agent/template',$this->data);
    }
    
    function transactions(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('transactionid')) {
            $key['txnid'] = $this->input->post('transactionid');
            $this->data['transactionid']=$this->input->post('transactionid');
        }
        
        if ($this->input->post('orderid')) {
            $key['orderid'] = $this->input->post('orderid');
            $this->data['orderid']=$this->input->post('orderid');
        }
        
        if ($this->input->post('msisdn')) {
            $key['msisdn'] = $this->input->post('msisdn');
            $this->data['msisdn']=$this->input->post('msisdn');
        }
        
        if ($this->input->post('from')) {
            $key['from'] = $this->input->post('from');
            $this->data['from']=$this->input->post('from');
        }
        
        if ($this->input->post('to')) {
            $key['to'] = $this->input->post('to');
            $this->data['to']=$this->input->post('to');
        }
        

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['orderid'] = $exp[1];
            $this->data['orderid']=$key['orderid'];
            
            $key['txnid'] = $exp[3];
            $this->data['txnid']=$key['txnid'];
            
            $key['msisdn'] = $exp[5];
            $this->data['msisdn']=$key['msisdn'];
            
            $key['from'] = $exp[7];
            $this->data['from']=$key['from'];
            
            $key['to'] = $exp[9];
            $this->data['to']=$key['to'];
            
            $doc= $exp[11];
        }

        $orderid =$key['orderid'];
        $txnid =$key['txnid'];
        $msisdn =$key['msisdn'];
        $from =$key['from'];
        $to =$key['to'];

        if($doc == 1){
            
            $data=$this->Administration_model->agro_transactions(NULL,$orderid,$txnid,$msisdn,$from,$to,NULL);
            require_once 'reports/transactions_pdf.php';
        }
        
        $config["base_url"] = base_url() . "Agent/transactions/ord_".$key['orderid']."_txnid_".$key['txnid']."_msisdn_".$key['msisdn']."_st_".$key['from']."_end_".$key['to']."/";
        $config["total_rows"] =$this->Administration_model->agro_transactions_info_count($orderid,$txnid,$msisdn,$from,$to,NULL);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->agro_transactions_info($orderid,$txnid,$msisdn,$from,$to,NULL,$page,$limit);
        
        $this->session->set_userdata('prev_url_path',base_url() . "Agent/transactions/ord_".$key['orderid']."_txnid_".$key['txnid']."_msisdn_".$key['msisdn']."_st_".$key['from']."_end_".$key['to']."/".$page);        
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='txnlnk';
        $this->data['title']='Transactions';
        $this->data['searchform']='searchforms/agent/transactions_search_form';
        $this->data['content']='agent/transactions';
        $this->load->view('agent/template',$this->data);
    }
    
    function orderdetails($orderid){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
           
        if($this->input->post('confirm_order')){
            $error=FALSE;
            $orderid=$this->input->post('orderid');
            $ord_items=$this->Administration_model->agro_order_items($orderid,NULL,NULL,'pending');
            
            if($ord_items == NULL){
                $err_msg="no active products in the order!";
                $error=TRUE;
            }
            
            foreach($ord_items AS $vl){
                
                if($vl->productqty <> (0-$this->Inventory_model->total_stock($vl->productid,NULL,NULL,$vl->aoiID)->ttlStk)){
                    $err_msg="please reserve stock for $vl->productname before confirming the order";
                    $error=TRUE;
                    break;
                }
                
            }
            
            if(!$error){
                
                $data=array(
                'deliverydriver'=>$this->input->post('driver'),
                'deliverydate'=>$this->input->post('deliverydate'),
                'orderstatus'=>'confirmed',
                'modifiedby'=>$this->session->userdata('user_id'),
                'modifiedon'=>date('Y-m-d H:i:s'),
                );
                
                $item=array(
                        'status'=>'confirmed',
                        'modifiedby'=>$this->session->userdata('user_id'),
                        'modifiedon'=>date('Y-m-d H:i:s'),
                        );
                
                $text=sprintf($this->config->item('order_confirmation_message'),$ord_items[0]->buyer,$orderid,number_format(($ord_items[0]->productcost+$ord_items[0]->deliverycost),2)." /=",$this->config->item('order_expiry_hours'));
             
                $message=array(
                    'textmessage'=>$text,
                    'textrecipient'=>$ord_items[0]->buyercontact,
                    'status'=>'pending',
                    'createdon'=>date('Y-m-d H:i:s')
                );
                
                $this->Administration_model->update_order($data,$item,$message,$orderid);
            }else{
               
                $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">'.$err_msg
                                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>';
            }
            
            
        }
        
        if($this->input->post('cancel_order')){
            
            $orderid=$this->input->post('orderid');
            $ord_items=$this->Administration_model->agro_order_items($orderid,NULL,NULL,'pending');
            
            $data=array(
                        'reason'=>$this->input->post('reason'),
                        'orderstatus'=>'cancelled',
                        'modifiedby'=>$this->session->userdata('user_id'),
                        'modifiedon'=>date('Y-m-d H:i:s'),
                    );
            
            $item=array(
                        'status'=>'cancelled',
                        'modifiedby'=>$this->session->userdata('user_id'),
                        'modifiedon'=>date('Y-m-d H:i:s'),
                        );
            $text=sprintf($this->config->item('order_cancellation_message'),$orderid);
             
            $message=array(
                'textmessage'=>$text,
                'textrecipient'=>$ord_items[0]->buyercontact,
                'status'=>'pending',
                'createdon'=>date('Y-m-d H:i:s')
            );
            $this->Administration_model->update_order($data,$item,$message,$orderid);
            
        }
        
        $ord_items=$this->Administration_model->agro_order_items($orderid,NULL,NULL,NULL);
        //$this->session->set_userdata('prev_url_path',base_url() . "Agent/orderdetails/".$orderid);
        $this->data['ord'] = $ord_items;
        $this->data['drivers'] = $this->Administration_model->get_member_info(NULL,NULL,NULL,$this->config->item('driver_group_id'));
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['title']='Order Details';
        $this->data['content']='agent/orderdetails';
        $this->load->view('agent/template',$this->data);
    }
    
    function remove_agro_order_item($id){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
           
           $this->Administration_model->remove_agro_item_order($id);
           redirect($this->session->userdata('prev_url_path'),'refresh');
    }
    
    function view_supply_details($item_order){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
        }
        
       
        $this->data['data'] = $this->Inventory_model->inventory_details(NULL,NULL,NULL,'Reserved',$item_order);
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['title']='Supply Details';
        $this->data['content']='agent/view_supply_details';
        $this->load->view('agent/template',$this->data);
    }
    
    function available_stock($product){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
        }
        
        if($this->uri->segment('3') <> NULL){
            
            $details=explode("_",$this->uri->segment('3'));
            $product=$details[0];
            
            $ord_itm_info=$this->Administration_model->agro_order_items($details[1],$details[0],$details[2]);
            
            if($ord_itm_info == NULL){
                
                redirect($this->session->userdata('prev_url_path'),'refresh');
            }
        }
       
        $this->data['data'] = $this->Inventory_model->available_stock($product,$supplier);
        $this->data['ordered_qty'] = $ord_itm_info[0]->productqty+$this->Inventory_model->total_stock($ord_itm_info[0]->productid,NULL,NULL,$ord_itm_info[0]->aoiID)->ttlStk;
        $this->data['order_item'] =$ord_itm_info;
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['title']='Available Stock';
        $this->data['content']='agent/available_stock';
        $this->load->view('agent/template',$this->data);
    }
    
    function reserve_stock(){
       $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
            $b_detls=$this->Inventory_model->inventory_details($this->input->post('productid'),$this->input->post('supplierid'),$this->input->post('productbatch'));
            $data=array(
                'productid'=>$this->input->post('productid'),
                'supplier'=>$this->input->post('supplierid'),
                'quantity'=>0-$this->input->post('quantity'),
                'availabilityPeriod'=>$b_detls[0]->availabilityPeriod,
                'availableDate'=>$b_detls[0]->availableDate2,
                'productbatch'=>$this->input->post('productbatch'),
                'reference_orderID'=>$this->input->post('item_order_id'),
                'status'=>'Reserved',
                'createdon'=>date('Y-m-d H:i:s'),
                'createdby'=>$this->session->userdata('user_id'),
            );
            
            $sve=$this->Inventory_model->save_product_stock($data);
            if($sve){

               $message='product stock reserved.';
            }else{

                $message='sorry data saving error!';
            }
            
            echo $message;
            exit;
    }
    
    function inventory_stock(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('product')) {
            $key['product'] = $this->input->post('product');
            $this->data['product']=$this->input->post('product');
        }
        
        if ($this->input->post('batch')) {
            $key['batch'] = $this->input->post('batch');
            $this->data['batch']=$this->input->post('batch');
        }
        
        if ($this->input->post('supplier')) {
            $key['supplier'] = $this->input->post('supplier');
            $this->data['supplier']=$this->input->post('supplier');
        }
        
        if ($this->input->post('availability')) {
            $key['availability'] = $this->input->post('availability');
            $this->data['availability']=$this->input->post('availability');
        }
        
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['product'] = $exp[1];
            $this->data['product']=$key['product'];

            $key['batch'] = $exp[3];
            $this->data['batch']=$key['batch'];
            
            $key['availability'] = $exp[5];
            $this->data['availability']=$key['availability'];
            
            $key['supplier'] = $exp[7];
            $this->data['supplier']=$key['supplier'];
            
            $doc = $exp[9];
        }
        
        $product =$key['product'];
        $batch =$key['batch'];
        $availability =$key['availability'];
        $supplier=$key['supplier'];
        
        if($doc == 1){
            
            $data=$this->Inventory_model->inventory_stock($product,$supplier,$batch,$availability);
            require_once 'reports/products_inventory_pdf.php';
        }
        
        $config["base_url"] = base_url() . "Agent/inventory_stock/product_".$key['product']."_batch_".$key['batch']."_av_".$key['availability']."_sp_".$key['supplier']."/";
        $config["total_rows"] =$this->Inventory_model->inventory_stock_info_count($product,$supplier,$batch,$availability);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Inventory_model->inventory_stock_info($product,$supplier,$batch,$availability,$page,$limit);
        
        $this->session->set_userdata('prev_url_path',base_url() . "Agent/inventory_stock/product_".$key['product']."_batch_".$key['batch']."_av_".$key['availability']."_sp_".$key['supplier']."/".$page);
        $this->data['suppliers']=$this->Administration_model->product_suppliers();
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='inventorystocklnk';
        $this->data['searchform']='searchforms/agent/inventory_search_form';
        $this->data['title']='Product Stock';
        $this->data['content']='agent/inventory_stock';
        $this->load->view('agent/template',$this->data);
    }
    
    function inventory_details($product,$batch,$export=FALSE){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
        }
        
        $data=$this->Inventory_model->inventory_details($product,NULL,$batch);
        
        if($export){
        
            require_once 'reports/stock_inventory_details_pdf.php';
        }
        
        $this->data['data'] = $data;
        $this->data['product']=$product;
        $this->data['batch']=$batch;
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='inventorystocklnk';
        $this->data['title']='Inventory Stock Details';
        $this->data['content']='agent/inventory_details';
        $this->load->view('agent/template',$this->data);
    }
    
    function getAgreementID(){
       
        while(true){
            $d=date('D');
            switch ($d){
                case "Mon":
                    $agreementID="A";
                    break;
                case "Tue":
                    $agreementID="B";
                    break;
                case "Wed":
                    $agreementID="C";
                    break;
                case "Thu":
                    $agreementID="D";
                    break;
                case "Fri":
                    $agreementID="E";
                    break;
                case "Fri":
                    $agreementID="F";
                    break;
                default :
                    $agreementID="G";
            }

            $agreementID .="".substr(str_shuffle("0123456789"), -3);
            $agreementID .="-".date('Y-md');
            if($this->Administration_model->project_agreements($agreementID) == NULL){
                return $agreementID;
            }
        }
    }
    
    function check_session_account_validity(){
        if (!$this->ion_auth->logged_in()){
                return FALSE;
        }

        if(!$this->ion_auth->in_group('Agent')){

            return FALSE;
        }

        return TRUE;
    }
    
}