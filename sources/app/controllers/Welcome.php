<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
            parent::__construct();
        }
        
    public function home(){
        
        if($this->input->post('user_signup')){ //signup a buyer
            
            $this->form_validation->set_rules('firstName','First Name','trim|required|xss_clean');
            $this->form_validation->set_rules('middleName','Middle Name','trim|xss_clean');
            $this->form_validation->set_rules('surname','Surname','trim|required|xss_clean');
            $this->form_validation->set_rules('gender','Gender','trim|required|xss_clean');
            $this->form_validation->set_rules('emailAddress', 'Email Address', 'trim|required|valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|required|xss_clean');
            $this->form_validation->set_rules('captcha_signup', 'Captcha', 'trim|required|xss_clean|callback_captcha_signup');

            if ($this->form_validation->run() == TRUE){
                
                $first_name=$this->input->post('firstName');
                $middle_name=$this->input->post('middleName');
                $surname=$this->input->post('surname');
                $fullname=$first_name." ".$middle_name." ".$surname;
                $gender=$this->input->post('gender');
                $email=$this->input->post('emailAddress');
                $contact=$this->input->post('mobile');
                                
                if($this->Administration_model->get_member_info(NULL,$email,NULL,NULL) == NULL){
                    
                    $buyerID=str_shuffle(substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'),-3).time());
                    $password= str_shuffle(substr(str_shuffle('abcdefghijklmnopqrstuvwxyz!_-'),-3).substr(str_shuffle('0123456789'),-4));
                
                    $user=array(
                        'first_name'=>$first_name,
                        'middle_name'=>$middle_name,
                        'last_name'=>$surname,
                        'gender'=>$gender,
                        'username'=>$email,
                        'memberid'=>$buyerID,
                        'email'=>$email,
                        'msisdn'=>$contact,
                    );

                    $user_group=array(
                        'group_id'=>$this->config->item('buyer_group_id')
                    );

                    $sve=$this->Ion_Auth_model->system_user_registration($user,$user_group,NULL,$password);
                    if($sve){

                        $mailMessage=sprintf($this->config->item('mailMessage'),$fullname,$email,$password);

                        $this->email->from($this->config->item('from_email_address'),$this->config->item('from_name'),$this->config->item('failure_reply_email_address'));
                        $this->email->reply_to($this->config->item('reply_to_email_address'),$this->config->item('reply_to_name'));
                        $this->email->to($email);
                        $this->email->subject("KARIBU GULIO MKONONI");
                        $this->email->message($mailMessage);
                        $send=$this->email->send(TRUE);

                        if($send){

                            $emailNotification='sent';
                        }else{

                            $emailNotification='pending';
                        }
                        $this->Administration_model->save_user_details(array('emailNotificationStatus'=>$emailNotification),NULL,$email);
                        $this->data['message']='<div class="alert alert-success alert-dismissible fade show" role="alert">account created!please check your email account within 24 hours for your login credentials.'
                                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>';
                    }else{

                        $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert"">sorry account not created!'
                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></div>';
                    }
                    
                }else{
                    
                    $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert"">email address exists,account not created!'
                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></div>';
                }
            }
        }

        if($this->input->post('user_signin')){ //user signin
            //validate form input
            $this->form_validation->set_rules('email', 'Email Address', 'trim|required|xss_clean');
            $this->form_validation->set_rules('userPassword', 'Password', 'trim|required|xss_clean');
            $this->form_validation->set_rules('captcha_signin', 'Captcha', 'trim|required|xss_clean|callback_captcha_signin');
            
            if ($this->form_validation->run() == true){

                if ($this->ion_auth->login($this->input->post('email'), $this->input->post('userPassword'), NULL)){ //if the login is successful

                        //redirect them to a specific  module
                        if($this->ion_auth->in_group('Administrator')){

                            redirect('Administrator/', 'refresh');
                        }

                        if($this->ion_auth->in_group('Manager')){

                            redirect('Manager/', 'refresh');
                        }
                        
                        if($this->ion_auth->in_group('Agent')){

                            redirect('Agent/', 'refresh');
                        }

                        if($this->ion_auth->in_group('Supplier')){

                            redirect('Supplier/', 'refresh');  
                        }

                        if($this->ion_auth->in_group('Buyer')){

                            redirect('Buyer/', 'refresh');  
                        }
                }else{ 

                        $this->data['message']="sorry username or password is not correct!";
                        $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert"">username or password is not correct'
                                    . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                        .'<span aria-hidden="true">&times;</span>
                                      </button>
                                      </div>';
                }
            }
    }
    
        if($this->input->post('sendEmail')){ //public feedback and comments
            //validate form input
            $this->form_validation->set_rules('emailAddress', 'Email Address', 'trim|required|xss_clean');
            $this->form_validation->set_rules('emailSubject', 'Subject', 'trim|required|xss_clean');
            $this->form_validation->set_rules('emailContent', 'Message', 'trim|required|xss_clean');
            $this->form_validation->set_rules('captcha_sendemail', 'Captcha', 'trim|required|xss_clean|callback_captcha_sendemail');
            
            if ($this->form_validation->run() == true){ //check to see if the user is logging in
                
                $feedback=array(
                        'emailAddress'=>$this->input->post('emailAddress'),
                        'subject'=>$this->input->post('emailSubject'),
                        'message'=>$this->input->post('emailContent'),
                        'createdon'=>date('Y-m-d H:i:s'),
                    );
                
                if($this->Administration_model->save_user_feedback($feedback,NULL)){
                     $this->data['feedbackResponse']='<div class="alert alert-success alert-dismissible fade show" role="alert">'
                                                . 'Your message has been received.Thank you!!'
                                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'
                                                        .'<span aria-hidden="true">&times;</span>'
                                                .'</button>'
                                            .'</div>';
                }else{
                    
                    $this->data['feedbackResponse']='<div class="alert alert-danger alert-dismissible fade show" role="alert"">sorry your message has not been received,please try sending again later.Thank you!!'
                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></div>';
                }
            }
        }
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->input->post('categ')) {
            $key['categ'] = $this->input->post('categ');
            $this->data['categ']=$this->input->post('categ');
        }


       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['categ'] = $exp[2];
            $this->data['categ']=$key['categ'];

        }

        $name =$key['name'];
        $categ =$key['categ'];

        $config["base_url"] = base_url() . "Welcome/home/name_".$key['name']."_categ_".$key['categ']."/";
        $config["total_rows"] =$this->Administration_model->products_info_count($name,$categ,'Active',$this->config->item('crop_product_identifier'));
        $config["per_page"] =5;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->products_info($name,$categ,'Active',$this->config->item('crop_product_identifier'),$page,$limit);
        
        $this->data['captcha_img_signup']=$this->generate_captcha_image()['filename'];
        $this->data['captcha_img_signin']=$this->generate_captcha_image()['filename'];
        $this->data['captcha_img_sendemail']=$this->generate_captcha_image()['filename'];
        $this->load->view('welcome_message',$this->data);
    }
    
    //log the user out
    function logout(){

        //log the user out
        $this->ion_auth->logout();

        //redirect them back to the page they came from
        redirect('Welcome/home','refresh');
    }
    
    function validate_user_email(){
        
        $email=$this->input->post('email');
        $array=NULL;
        
        if($email <> NULL){
            
            if($this->Administration_model->get_member_info(NULL,$email,NULL,NULL) <> NULL){
                
                $array=array(
                    'validity'=>false,
                    'message'=>'email address exists!'
                );
            }
        }
        
        echo json_encode($array);
        exit;
    }
    
    function generate_captcha_image(){
        
        $this->load->helper('captcha');
        $vals = array(
                    'word'          => '',
                    'img_path'      => './captcha/',
                    'img_url'       => base_url().'captcha/',
                    'font_path'     => './captcha/fonts/texb.ttf',
                    'img_width'     => '200',
                    'img_height'    => 50,
                    'expiration'    => $this->config->item('captcha_expiration_time'),
                    'word_length'   => 6,
                    'font_size'     => 22,
                    'img_id'        => 'Imageid',
                    'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                    // White background and border, black text and red grid
                    'colors'        => array(
                            'background' => array(255, 255, 255),
                            'border' => array(255, 255, 255),
                            'text' => array(0, 0, 0),
                            'grid' => array(255, 40, 40)
                    )
            );
        
        $cap = create_captcha($vals);
            $data = array(
                        'captcha_time'  => $cap['time'],
                        'ip_address'    => $this->input->ip_address(),
                        'word'          => $cap['word']
                        );
            
        $this->Ion_Auth_model->save_captcha($data);

        return $cap;
    }
    
    function verify_captcha_image(){
        
        //delete all the expired captchas
        $expiration = time() - $this->config->item('captcha_expiration_time');
        
        $this->Ion_Auth_model->delete_captcha($expiration);
        
        $captcha=$this->Ion_Auth_model->retrieve_captcha($this->input->post('captcha'),$this->input->ip_address(),$expiration);
       
        if($captcha <> NULL){
            
            $array=array(
                'message'=>"verified",
                'status'=>TRUE,
                );
        }else{
            
            $array=array(
                'message'=>"please try again",
                'captcha'=>base_url().'captcha/'.$this->generate_captcha_image()['filename'],
                'status'=>FALSE,
                );
        }
        
        echo json_encode($array);
        exit;
    }
    
    //callback function for codeigniter form validation
    function captcha_signup($captcha){
        
         $expiration = time() - $this->config->item('captcha_expiration_time');
         $this->Ion_Auth_model->delete_captcha($expiration);
        if ($this->Ion_Auth_model->retrieve_captcha($captcha,$this->input->ip_address(),$expiration) <> NULL) {
            
            return TRUE;
        } else {
            
            $this->form_validation->set_message('captcha_signup', "The %s is not valid");
            return FALSE;
        }
    }
    
    function captcha_signin($captcha){
        
         $expiration = time() - $this->config->item('captcha_expiration_time');
         $this->Ion_Auth_model->delete_captcha($expiration);
        if ($this->Ion_Auth_model->retrieve_captcha($captcha,$this->input->ip_address(),$expiration) <> NULL) {
            
            return TRUE;
        } else {
            
            $this->form_validation->set_message('captcha_signin', "The %s is not valid");
            return FALSE;
        }
    }
    
    function captcha_sendemail($captcha){
        
         $expiration = time() - $this->config->item('captcha_expiration_time');
         $this->Ion_Auth_model->delete_captcha($expiration);
        if ($this->Ion_Auth_model->retrieve_captcha($captcha,$this->input->ip_address(),$expiration) <> NULL) {
            
            return TRUE;
        } else {
            
            $this->form_validation->set_message('captcha_sendemail', "The %s is not valid");
            return FALSE;
        }
    }
}
