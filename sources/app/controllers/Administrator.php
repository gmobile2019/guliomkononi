<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author gmobile
 */
class Administrator extends CI_Controller{
   
    function __construct(){
        
            parent::__construct();
            $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        
        $this->data['activeMenu']='homemenu';
        $this->data['activeLink']='';
        $this->data['title']='Home';
        $this->data['content']='admin/home';
        $this->load->view('admin/template',$this->data);
    }
    
    function add_user($id=null){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }

        $this->form_validation->set_rules('first_name','First Name','trim|required|xss_clean');
        $this->form_validation->set_rules('middle_name','Middle Name','trim|xss_clean');
        $this->form_validation->set_rules('last_name','Last Name','trim|required|xss_clean');
        $this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_email|xss_clean');
        $this->form_validation->set_rules('mobile', 'Mobile Number', 'trim|required|xss_clean');
        $this->form_validation->set_rules('gender', 'Gender', 'trim|required|xss_clean');
        $this->form_validation->set_rules('group','User Group','trim|required|xss_clean');
           
        if($this->config->item('supplier_group_id') == $this->input->post('group')){

            $this->form_validation->set_rules('center_region','Region','trim|required|xss_clean');
            $this->form_validation->set_rules('center_district','District','trim|required|xss_clean');
            $this->form_validation->set_rules('center_ward','Ward','trim|xss_clean');
            $this->form_validation->set_rules('center_street','Street','trim|xss_clean');
        }
        
        if($this->config->item('farmer_group_id') == $this->input->post('group')){

            $this->form_validation->set_rules('tribe','Tribe','trim|required|xss_clean');
            $this->form_validation->set_rules('farmer_region','Region','trim|required|xss_clean');
            $this->form_validation->set_rules('farmer_district','District','trim|required|xss_clean');
            $this->form_validation->set_rules('farmer_ward','Ward','trim|xss_clean');
            $this->form_validation->set_rules('farmer_street','Street','trim|xss_clean');
        }
        
        if($this->config->item('driver_group_id') == $this->input->post('group')){

            $this->form_validation->set_rules('vehicleno','Vehicle No','trim|required|xss_clean');
            $this->form_validation->set_rules('parking_region','Region','trim|required|xss_clean');
            $this->form_validation->set_rules('parking_district','District','trim|required|xss_clean');
            $this->form_validation->set_rules('parking_ward','Ward','trim|xss_clean');
            $this->form_validation->set_rules('parking_street','Street','trim|xss_clean');
        }
        

        if ($this->form_validation->run() == TRUE){
            $error=FALSE;
            $photoUpload=TRUE;
            $first_name=$this->input->post('first_name');
            $middle_name=$this->input->post('middle_ame');
            $surname=$this->input->post('last_name');
            $fullname=$first_name." ".$middle_name." ".$surname;
            $gender=$this->input->post('gender');
            $email=$this->input->post('email');
            $contact=$this->input->post('mobile');
                $memberID=$this->input->post('memberID')==NULL?str_shuffle(substr(str_shuffle('ABCDEFGHIJKLMNOPQRSTUVWXYZ'),-3).time()):$this->input->post('memberID');
                $password=$id == NULL?str_shuffle(substr(str_shuffle('abcdefghijklmnopqrstuvwxyz!_-'),-3).substr(str_shuffle('0123456789'),-4)):NULL;
                
                $user=array(
                    'first_name'=>$first_name,
                    'middle_name'=>$middle_name,
                    'last_name'=>$surname,
                    'gender'=>$gender,
                    'username'=>$email,
                    'memberid'=>$memberID,
                    'email'=>$email,
                    'msisdn'=>$contact,
                );

                if($this->input->post('group') == $this->config->item('supplier_group_id')){

                    $user['center_residence_region']=$this->input->post('center_region');
                    $user['center_residence_district']=$this->input->post('center_district');
                    $user['center_residence_ward']=$this->input->post('center_ward');
                    $user['center_residence_street']=$this->input->post('center_street');
                }
                
                if($this->input->post('group') == $this->config->item('farmer_group_id')){

                    $user['center_residence_region']=$this->input->post('farmer_region');
                    $user['center_residence_district']=$this->input->post('farmer_district');
                    $user['center_residence_ward']=$this->input->post('farmer_ward');
                    $user['center_residence_street']=$this->input->post('farmer_street');
                }
                
                if($this->input->post('group') == $this->config->item('driver_group_id')){

                    $user['vehicleno']=$this->input->post('vehicleno');
                    $user['center_residence_region']=$this->input->post('parking_region');
                    $user['center_residence_district']=$this->input->post('parking_district');
                    $user['center_residence_ward']=$this->input->post('parking_ward');
                    $user['center_residence_street']=$this->input->post('parking_street');
                }

                $user_group=array(
                    'group_id'=>$this->input->post('group')
                );

                if($_FILES['supplierphoto']['name'] <> NULL || $_FILES['farmerphoto']['name'] <> NULL || $_FILES['driverphoto']['name']){
                
                    $config['upload_path'] = $this->config->item('user_photo_path');
                    $config['allowed_types'] = 'jpg|jpeg';
                    $config['max_size']=1024;
                    $config['overwrite']=true;
                    
                    if($this->input->post('group') == $this->config->item('supplier_group_id')){

                        $img_name=explode('.',$_FILES['supplierphoto']['name']);
                        $img_ext=end($img_name);
                        $img_name="$memberID.".$img_ext;
                        $config['file_name']=$img_name;
                        
                        $this->upload->initialize($config);
                        $upload=$this->upload->do_upload('supplierphoto');
                    }
                    
                    if($this->input->post('group') == $this->config->item('farmer_group_id')){

                        $img_name=explode('.',$_FILES['farmerphoto']['name']);
                        $img_ext=end($img_name);
                        $img_name="$memberID.".$img_ext;
                        $config['file_name']=$img_name;
                        $this->upload->initialize($config);
                        $upload=$this->upload->do_upload('farmerphoto');
                    }
                    
                    if($this->input->post('group') == $this->config->item('driver_group_id')){

                        $img_name=explode('.',$_FILES['driverphoto']['name']);
                        $img_ext=end($img_name);
                        $img_name="$memberID.".$img_ext;
                        $config['file_name']=$img_name;
                        $this->upload->initialize($config);
                        $upload=$this->upload->do_upload('driverphoto');
                        
                        if($_FILES['driverlicense']['name'] <> NULL){
                            
                            $config['upload_path'] = $this->config->item('driver_license_path');
                            $lc_name=explode('.',$_FILES['driverlicense']['name']);
                            $lc_ext=end($lc_name);
                            $lc_name="$memberID.".$lc_ext;
                            $config['file_name']=$lc_name;
                            $this->upload->initialize($config);
                            $this->upload->do_upload('driverlicense');
                        }
                    }
                    
                   if($upload){
                     
                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $this->config->item('user_photo_path').$img_name;
                        $config['create_thumb'] = TRUE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width']         = 300;
                        $config['height']       = 170;

                        $this->load->library('image_lib', $config);

                       if(! $this->image_lib->resize()){

                            $this->data['message']=$this->image_lib->display_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">','<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>');
                            $photoUpload=FALSE;
                       }else{

                           $user['userphoto']=$img_name;
                       }
                      
                    }else{
                      $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">','<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>');
                      $photoUpload=FALSE;
                    }
                }
                
                //if photo uploaded and user is not a supplier
                if(!$photoUpload && ($this->input->post('group') == $this->config->item('supplier_group_id') || $this->input->post('group') == $this->config->item('farmer_group_id'))&& $id == NULL){
                    $error=TRUE;
                }
                 
                if(!$error){
                    
                    $sve=$this->Ion_Auth_model->system_user_registration($user,$user_group,$id,$password);
                    if($sve){
                        
                        if($id == NULL){
                            $mailMessage=sprintf($this->config->item('mailMessage'),$fullname,$email,$password);

                            $this->email->from($this->config->item('from_email_address'),$this->config->item('from_name'),$this->config->item('failure_reply_email_address'));
                            $this->email->reply_to($this->config->item('reply_to_email_address'),$this->config->item('reply_to_name'));
                            $this->email->to($email);
                            $this->email->subject("Kilimo One Account Details");
                            $this->email->message($mailMessage);
                            $send=$this->email->send(TRUE);

                            if($send){

                                $emailNotification='sent';
                            }else{

                                $emailNotification='pending';
                            }
                            $this->Administration_model->save_user_details(array('emailNotificationStatus'=>$emailNotification),NULL,$email);
                        }
                        
                        if($id <> NULL){
                            $this->data['message']='<div class="alert alert-success alert-dismissible fade show" role="alert">data saved.'
                                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>';
                        }else{
                            $this->data['message']='<div class="alert alert-success alert-dismissible fade show" role="alert">account created!please check your email account within 24 hours for your login credentials.'
                                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>';
                        }
                        
                    }else{

                        $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">sorry account not created!'
                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></div>';
                    }
                }else{
                    
                    if($this->data['message'] == NULL){
                        $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">sorry account not created!'
                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></div>';
                    }
                }
       }

       $this->data['activeMenu']='usersmenu';
       $this->data['activeLink']='adduserlnk';
       $this->data['title']="Add/Edit User";
       $this->data['id']="$id";
       $this->data['regions']=$this->Administration_model->locations(NULL,'region');
       $this->data['districts']=$this->Administration_model->locations(NULL,'district');
       $this->data['tribes']=$this->Administration_model->tribes(NULL,'Active');
       $this->data['member']=$this->Administration_model->get_member_info($id,NULL,NULL,NULL);
       $this->data['groups']=$this->Administration_model->groups(NULL,'administrator');
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/add_user';
       $this->load->view('admin/template',$this->data);
    }
    
    function activate_deactivate_user($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('Welcome/logout', 'refresh');
            }

            $this->Administration_model->activate_deactivate_user($id,$status);
            redirect('Administrator/users','refresh');
    }
    
    function users(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->input->post('group')) {
            $key['group'] = $this->input->post('group');
            $this->data['group']=$this->input->post('group');
        }


       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['group'] = $exp[2];
            $this->data['group']=$key['group'];

        }

        $name =$key['name'];
        $group =$key['group'];

        $config["base_url"] = base_url() . "Administrator/users/name_".$key['name']."_grp_".$key['group']."/";
        $config["total_rows"] =$this->Administration_model->users_info_count($name,$group);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->users_info($name,$group,$page,$limit);
                
        $this->data['groups']=$this->Administration_model->groups(NULL,'administrator');
        $this->data['activeMenu']='usersmenu';
        $this->data['activeLink']='viewuserslnk';
        $this->data['title']='Users';
        $this->data['searchform']='searchforms/admin/users_admin_search_form';
        $this->data['content']='admin/users';
        $this->load->view('admin/template',$this->data);
    }
    
    function user_details($id){
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
       $this->data['activeMenu']='usersmenu';
       $this->data['activeLink']='viewuserslnk';
       $this->data['title']="User Details";
       $this->data['member']=$this->Administration_model->get_member_info($id,NULL,NULL,NULL);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/user_details';
       $this->load->view('admin/template',$this->data);
    }
    
    function add_crop($id=null){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }

        $this->form_validation->set_rules('cropname','Crop Name','trim|required|xss_clean');
        $this->form_validation->set_rules('description','Description','trim|xss_clean|required');
        $this->form_validation->set_rules('description','Description','trim|xss_clean|required');
        $this->form_validation->set_rules('cropcategory','Crop Category','trim|required|xss_clean');
        $this->form_validation->set_rules('cropunit','Crop Unit','trim|required|xss_clean');
        $this->form_validation->set_rules('cropprice','Crop Market Price','trim|required|xss_clean|numeric');
        $this->form_validation->set_rules('cropdiscount','Market Price Discount','trim|xss_clean');
        $this->form_validation->set_rules('stocklimit','Lower Stock Limit','trim|xss_clean|required|numeric');
        
        
        if ($this->form_validation->run() == TRUE){
            $error=FALSE;
            $photoUpload=TRUE;
            
                $data=array(
                    'productname'=>$this->input->post('cropname'),
                    'description'=>$this->input->post('description'),
                    'productcategory'=>$this->input->post('cropcategory'),
                    'productunit'=>$this->input->post('cropunit'),
                    'productprice'=>$this->input->post('cropprice'),
                    'productdiscount'=>$this->input->post('cropdiscount'),
                    'stocklimit'=>$this->input->post('stocklimit'),
                    'producttype'=>$this->config->item('crop_product_identifier'),
                );

                if($_FILES['cropphoto']['name'] <> null){

                    $config['upload_path'] = $this->config->item('sellingcrops_img_path');
                    $config['allowed_types'] = 'jpg|jpeg';
                    $img_name=explode('.',$_FILES['cropphoto']['name']);
                    $img_ext=end($img_name);
                    $img_name=time().".".$img_ext;
                    $config['file_name']=$img_name;
                    $config['max_size']=1024;
                    $config['overwrite']=true;
                    $this->upload->initialize($config);

                   if($this->upload->do_upload('cropphoto')){

                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $this->config->item('sellingcrops_img_path').$img_name;
                        $config['create_thumb'] = TRUE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width']         = 300;
                        $config['height']       = 170;

                        $this->load->library('image_lib', $config);

                       if(! $this->image_lib->resize()){

                            $this->data['message']=$this->image_lib->display_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">','<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>');
                            $photoUpload=FALSE;
                       }else{

                           $data['productimage']=$img_name;
                       }

                    }else{
                      $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">','<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>');
                      $photoUpload=FALSE;
                    }
                }
                
                //if not photo uploaded not an editing action
                if(!$photoUpload && $id == NULL){
                    $error=TRUE;
                }
                 
                if(!$error){
                    
                    $sve=$this->Administration_model->save_product($data,$id);
                    if($sve){

                       $this->data['message']='<div class="alert alert-success alert-dismissible fade show" role="alert">crop infromation saved successfully.'
                                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>';
                    }else{

                        $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">sorry data saving error!'
                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></div>';
                    }
                }else{
                    
                    if($this->data['message'] == NULL){
                        $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">sorry data saving error!'
                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></div>';
                    }
                }

       }

       $this->data['activeMenu']='productsmenu';
       $this->data['activeLink']='sellingcropslnk';
       $this->data['title']="Add/Edit Crop";
       $this->data['id']="$id";
       $this->data['crop']=$this->Administration_model->products($id,NULL,NULL,NULL,$this->config->item('crop_product_identifier'));
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/add_crop';
       $this->load->view('admin/template',$this->data);
    }
    
    function process_crop_request($id=null){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }

        if($this->input->post('reject')){
            
            $this->Administration_model->remove_product_request($this->input->post('cropid'));
        }else{
            
            $this->form_validation->set_rules('cropname','Crop Name','trim|required|xss_clean');
            $this->form_validation->set_rules('description','Description','trim|xss_clean|required');
            $this->form_validation->set_rules('cropcategory','Crop Category','trim|required|xss_clean');
            $this->form_validation->set_rules('cropunit','Crop Unit','trim|required|xss_clean');
        
        
            if ($this->form_validation->run() == TRUE){
                $error=FALSE;
                $photoUpload=TRUE;
            
                $data=array(
                    'productname'=>$this->input->post('cropname'),
                    'description'=>$this->input->post('description'),
                    'productcategory'=>$this->input->post('cropcategory'),
                    'productunit'=>$this->input->post('cropunit'),
                    'status'=>'Active',
                    'producttype'=>$this->config->item('crop_product_identifier'),
                );

                if($_FILES['cropphoto']['name'] <> null){

                    $config['upload_path'] = $this->config->item('sellingcrops_img_path');
                    $config['allowed_types'] = 'jpg|jpeg';
                    $img_name=explode('.',$_FILES['cropphoto']['name']);
                    $img_ext=end($img_name);
                    $img_name=time().".".$img_ext;
                    $config['file_name']=$img_name;
                    $config['max_size']=1024;
                    $config['overwrite']=true;
                    $this->upload->initialize($config);

                    if($this->upload->do_upload('cropphoto')){

                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $this->config->item('sellingcrops_img_path').$img_name;
                        $config['create_thumb'] = TRUE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width']         = 300;
                        $config['height']       = 170;

                        $this->load->library('image_lib', $config);

                       if(! $this->image_lib->resize()){

                            $this->data['message']=$this->image_lib->display_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">','<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>');
                            $photoUpload=FALSE;
                       }else{

                           $data['productimage']=$img_name;
                       }

                    }else{
                      $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">','<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>');
                      $photoUpload=FALSE;
                    }
                }
                
                //if not photo uploaded not an editing action
                if(!$photoUpload && $id == NULL){
                    $error=TRUE;
                }
                 
                if(!$error){
                    
                    $sve=$this->Administration_model->save_product($data,$this->input->post('cropid'));
                    if($sve){

                       $this->data['message']='<div class="alert alert-success alert-dismissible fade show" role="alert">crop infromation saved successfully.'
                                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>';
                    }else{

                        $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">sorry data saving error!'
                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></div>';
                    }
                }else{
                    
                    if($this->data['message'] == NULL){
                        $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">sorry data saving error!'
                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></div>';
                    }
                }

            }
        }
        

       $this->data['activeMenu']='productsmenu';
       $this->data['activeLink']='sellingcropslnk';
       $this->data['title']="Process Crop Request";
       $this->data['id']="$id";
       $this->data['crop']=$this->Administration_model->products($id,NULL,NULL,NULL,$this->config->item('crop_product_identifier'));
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/process_crop_request';
       $this->load->view('admin/template',$this->data);
    }
    
    function activate_deactivate_crop($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('Welcome/logout', 'refresh');
            }

            $this->Administration_model->activate_deactivate_product($id,$status);
            redirect('Administrator/crops','refresh');
    }
    
    function crops(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->input->post('categ')) {
            $key['categ'] = $this->input->post('categ');
            $this->data['categ']=$this->input->post('categ');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }


       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['categ'] = $exp[3];
            $this->data['categ']=$key['categ'];
            
            $key['status'] = $exp[5];
            $this->data['status']=$key['status'];
            
            $doc = $exp[7];
        }

        $name =$key['name'];
        $categ =$key['categ'];
        $status =$key['status'];
        
        if($doc == 1){
            
            $data=$this->Administration_model->products(NULL,$name,$categ,$status,$this->config->item('crop_product_identifier'));
            require_once 'reports/products_pdf.php';
        }
        
        $config["base_url"] = base_url() . "Administrator/crops/name_".$key['name']."_categ_".$key['categ']."_status_".$key['status']."/";
        $config["total_rows"] =$this->Administration_model->products_info_count($name,$categ,$status,$this->config->item('crop_product_identifier'));
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->products_info($name,$categ,$status,$this->config->item('crop_product_identifier'),$page,$limit);
                
        $this->data['activeMenu']='productsmenu';
        $this->data['activeLink']='sellingcropslnk';
        $this->data['title']='Crops';
        $this->data['searchform']='searchforms/admin/sellingcrops_admin_search_form';
        $this->data['content']='admin/crops';
        $this->load->view('admin/template',$this->data);
    }
    
    function project_schemes(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }

        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
        }

        $name =$key['name'];

        $config["base_url"] = base_url() . "Administrator/project_schemes/name_".$key['name']."/";
        $config["total_rows"] =$this->Administration_model->project_schemes_info_count($name);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->project_schemes_info($name,$page,$limit);
                
        $this->session->set_userdata('prev_url_path',base_url() . "Administrator/project_schemes/name_".$key['name']."/".$page);
        $this->data['activeMenu']='projectmenu';
        $this->data['activeLink']='prjschmlnk';
        $this->data['searchform']='searchforms/admin/project_scheme_search_form';
        $this->data['title']='Farming Schemes';
        $this->data['content']='admin/project_schemes';
        $this->load->view('admin/template',$this->data);
    }
    
    function add_project_scheme($id){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
        $this->form_validation->set_rules('scheme_name','Scheme Name','trim|required|xss_clean');
        $this->form_validation->set_rules('description','Scheme Description','trim|required|xss_clean');
       
        if ($this->form_validation->run() == TRUE){
            
            $scheme_name=$this->input->post('scheme_name');
            $description=$this->input->post('description');


            $scheme=array(
                'scheme_name'=>$scheme_name,
                'description'=>$description,
            );


            $resp=$this->Administration_model->add_project_scheme($scheme,$id);
            if($resp){

                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">data not saved!</div>';
            }
        }
        
       $this->data['activeMenu']='projectmenu';
       $this->data['activeLink']='prjschmlnk';
       $this->data['title']="Add/Edit Scheme";
       $this->data['id']=$id;
       $this->data['scheme']=$this->Administration_model->project_schemes($id);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/add_project_scheme';
       $this->load->view('admin/template',$this->data);
    }
    
    function activate_deactivate_project_scheme($id,$status,$page){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('Welcome/logout', 'refresh');
            }

            $this->Administration_model->activate_deactivate_project_scheme($id,$status);
            redirect('Administrator/project_schemes'.$page,'refresh');
    }
    
    function sponsorship_schemes(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }

        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
        }

        $name =$key['name'];

        $config["base_url"] = base_url() . "Administrator/sponsorship_schemes/name_".$key['name']."/";
        $config["total_rows"] =$this->Administration_model->sponsorship_schemes_info_count($name);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->sponsorship_schemes_info($name,$page,$limit);
                
        $this->session->set_userdata('prev_url_path',base_url() . "Administrator/sponsorship_schemes/name_".$key['name']."/".$page);
        $this->data['activeMenu']='projectmenu';
        $this->data['activeLink']='spschmlnk';
        $this->data['searchform']='searchforms/admin/sponsorship_schemes_search_form';
        $this->data['title']='Sponsorship Schemes';
        $this->data['content']='admin/sponsorship_schemes';
        $this->load->view('admin/template',$this->data);
    }
    
    function add_sponsorship_scheme($id){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
        $this->form_validation->set_rules('scheme_name','Scheme Name','trim|required|xss_clean');
        $this->form_validation->set_rules('description','Scheme Description','trim|required|xss_clean');
       
        if ($this->form_validation->run() == TRUE){
            
            $scheme_name=$this->input->post('scheme_name');
            $description=$this->input->post('description');


            $scheme=array(
                'scheme_name'=>$scheme_name,
                'description'=>$description,
            );


            $resp=$this->Administration_model->add_sponsorship_scheme($scheme,$id);
            if($resp){

                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">data not saved!</div>';
            }
        }
        
       $this->data['activeMenu']='projectmenu';
       $this->data['activeLink']='spschmlnk';
       $this->data['title']="Add/Edit Scheme";
       $this->data['id']=$id;
       $this->data['scheme']=$this->Administration_model->sponsorship_schemes($id);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/add_sponsorship_scheme';
       $this->load->view('admin/template',$this->data);
    }
    
    function activate_deactivate_sponsorship_scheme($id,$status,$page){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('Welcome/logout', 'refresh');
            }

            $this->Administration_model->activate_deactivate_sponsorship_scheme($id,$status);
            redirect('Administrator/sponsorship_schemes'.$page,'refresh');
    }
    
    function project_stages(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('stage')) {
            $stage = $this->input->post('stage');
            $this->data['stage']=$stage;
        }
       
        $this->session->set_userdata('prev_url_path',base_url() . "Administrator/project_stages");
        $this->data['data'] = $this->Administration_model->project_stages(NULL,$stage);
        $this->data['searchform']='searchforms/admin/project_stage_search_form';
        $this->data['activeMenu']='projectmenu';
        $this->data['activeLink']='prjstglnk';
        $this->data['title']='Project Stages';
        $this->data['content']='admin/project_stages';
        $this->load->view('admin/template',$this->data);
    }
    
    function add_project_stage($id){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
        $this->form_validation->set_rules('stage','Stage Name','trim|required|xss_clean');
        $this->form_validation->set_rules('completion','Completion','trim|required|xss_clean|numeric|greater_than[0]');
        $this->form_validation->set_rules('description','Description','trim|xss_clean');
       
        if ($this->form_validation->run() == TRUE){
           
            $data=array(
                'stage'=>$this->input->post('stage'),
                'percentage_completion'=>$this->input->post('completion'),
                'description'=>$this->input->post('description'),
            );

           
            $resp=$this->Administration_model->add_project_stage($data,$id);
            if($resp){

                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">data not saved!</div>';
            }
        }
        
       $this->data['menuactive']='projectmenu';
       $this->data['activelink']='prjstglnk';
       $this->data['title']="Add/Edit Project Stage";
       $this->data['id']=$id;
       $this->data['crop_stage']=$this->Administration_model->project_stages($id);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/add_project_stage';
       $this->load->view('admin/template',$this->data);
    }
    
    function activate_deactivate_project_stage($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('Welcome/logout', 'refresh');
            }

            $this->Administration_model->activate_deactivate_project_stage($id,$status);
            redirect('Administrator/project_stages','refresh');
    }
    
    function project_agreements(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('farmer')) {
            $key['farmer'] = $this->input->post('farmer');
            $this->data['farmer']=$this->input->post('farmer');
        }
        
        if ($this->input->post('product')) {
            $key['product'] = $this->input->post('product');
            $this->data['product']=$this->input->post('product');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->input->post('start')) {
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['farmer'] = $exp[1];
            $this->data['farmer']=$key['farmer'];
            
            $key['product'] = $exp[3];
            $this->data['product']=$key['product'];
            
            $key['status'] = $exp[5];
            $this->data['status']=$key['status'];
            
            $key['start'] = $exp[7];
            $this->data['start']=$key['start'];
            
            $key['end'] = $exp[9];
            $this->data['end']=$key['end'];
            
        }

        $farmer =$key['farmer'];
        $product =$key['product'];
        $status =$key['status'];
        $start =$key['start'];
        $end =$key['end'];

        $config["base_url"] = base_url() . "Administrator/project_agreements/farmer_".$key['farmer']."_product_".$key['product']."_status_".$key['status']."_start_".$key['start']."_end_".$key['end']."/";
        $config["total_rows"] =$this->Administration_model->project_agreements_info_count($farmer,$product,$status,$start,$end,NULL);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->project_agreements_info($farmer,$product,$status,$start,$end,NULL,$page,$limit);
                
        $this->session->set_userdata('prev_url_path',base_url() . "Administrator/project_agreements/farmer_".$key['farmer']."_product_".$key['product']."_status_".$key['status']."_start_".$key['start']."_end_".$key['end']."/".$page);
        $this->data['activeMenu']='projectmenu';
        $this->data['activeLink']='agrmntlnk';
        $this->data['searchform']='searchforms/admin/project_agreement_search_form';
        $this->data['title']='Project Agreements';
        $this->data['content']='admin/project_agreements';
        $this->load->view('admin/template',$this->data);
    }
    
    function add_project_agreement($id){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
        $this->form_validation->set_rules('farmer','Farmer','trim|required|xss_clean');
        $this->form_validation->set_rules('product','Product','trim|required|xss_clean');
        $this->form_validation->set_rules('farming_scheme','Farming Scheme','trim|required|xss_clean');
        $this->form_validation->set_rules('sponsorship_scheme','Sponsorship Scheme','trim|required|xss_clean');
        $this->form_validation->set_rules('market_price','Market Price','trim|required|xss_clean');
        $this->form_validation->set_rules('farmer_profit_share','Farmer Profit Share','trim|required|xss_clean');
        $this->form_validation->set_rules('org_profit_share','Organization Profit Share','trim|required|xss_clean');
        $this->form_validation->set_rules('org_investment_amount','Organization Investment Amount','trim|required|xss_clean');
        $this->form_validation->set_rules('pvt_investment_amount','Private Investment Amount','trim|required|xss_clean');
        $this->form_validation->set_rules('farm_size','Farm Size','trim|required|xss_clean');
        $this->form_validation->set_rules('farm_region','Farm location Region','trim|required|xss_clean');
        $this->form_validation->set_rules('farm_district','Farm location District','trim|required|xss_clean');
        $this->form_validation->set_rules('farm_ward','Farm location Ward','trim|required|xss_clean');
        $this->form_validation->set_rules('farm_street','Farm location Street','trim|required|xss_clean');
        $this->form_validation->set_rules('farm_longitude_gps','Farm location GPS Longitude','trim|required|xss_clean');
        $this->form_validation->set_rules('farm_latitude_gps','Farm location GPS Latitude','trim|required|xss_clean');
        $this->form_validation->set_rules('comments','Comments','trim|xss_clean');
       
        if ($this->form_validation->run() == TRUE){
           
            $data=array(
                'agreementID'=>$this->getAgreementID(),
                'farmer'=>$this->input->post('farmer'),
                'productid'=>$this->input->post('product'),
                'farming_scheme'=>$this->input->post('farming_scheme'),
                'sponsorship_scheme'=>$this->input->post('sponsorship_scheme'),
                'market_price'=>$this->input->post('market_price'),
                'farmer_profit_share'=>$this->input->post('farmer_profit_share'),
                'org_profit_share'=>$this->input->post('org_profit_share'),
                'org_investment_amount'=>$this->input->post('org_investment_amount'),
                'pvt_investment_amount'=>$this->input->post('pvt_investment_amount'),
                'farm_region'=>$this->input->post('farm_region'),
                'farm_district'=>$this->input->post('farm_district'),
                'farm_ward'=>$this->input->post('farm_ward'),
                'farm_street'=>$this->input->post('farm_street'),
                'farm_size'=>$this->input->post('farm_size'),
                'farm_longitude_gps'=>$this->input->post('farm_longitude_gps'),
                'farm_latitude_gps'=>$this->input->post('farm_latitude_gps'),
                'pa_comments'=>$this->input->post('comments'),
            );

           
            $resp=$this->Administration_model->add_project_agreement($data,$id);
            if($resp){

                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">data not saved!</div>';
            }
        }
        
       $this->data['activeMenu']='projectmenu';
       $this->data['activeLink']='agrmntlnk';
       $this->data['title']="Add/Edit Project Agreement";
       $this->data['id']=$id;
       $this->data['farming_schemes']=$this->Administration_model->project_schemes(NULL,'Active');
       $this->data['sp_schemes']=$this->Administration_model->sponsorship_schemes(NULL,'Active');
       $this->data['products']=$this->Administration_model->products(NULL,NULL,NULL,'Active',$this->config->item('crop_product_identifier'));
       $this->data['regions']=$this->Administration_model->locations(NULL,'region');
       $this->data['districts']=$this->Administration_model->locations(NULL,'district');
       $this->data['farmers']=$this->Administration_model->agro_farmers(NULL,'Active');
       $this->data['agreement']=$this->Administration_model->project_agreements($id);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/add_project_agreement';
       $this->load->view('admin/template',$this->data);
    }
    
    function project_agreement_details($id){
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
       $this->data['activeMenu']='projectmenu';
       $this->data['activeLink']='agrmntlnk';
       $this->data['title']="Agreement Details";
       $this->data['agreement']=$this->Administration_model->project_agreements($id);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/project_agreement_details';
       $this->load->view('admin/template',$this->data);
    }
    
    function project_activities(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('projectID')) {
            $key['projectID'] = $this->input->post('projectID');
            $this->data['projectID']=$this->input->post('projectID');
        }
        
        if ($this->input->post('stage')) {
            $key['stage'] = $this->input->post('stage');
            $this->data['stage']=$this->input->post('stage');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->input->post('start')) {
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['projectID'] = $exp[1];
            $this->data['projectID']=$key['projectID'];
            
            $key['stage'] = $exp[3];
            $this->data['stage']=$key['stage'];
            
            $key['status'] = $exp[5];
            $this->data['status']=$key['status'];
            
            $key['start'] = $exp[7];
            $this->data['start']=$key['start'];
            
            $key['end'] = $exp[9];
            $this->data['end']=$key['end'];
            
        }

        $projectID =$key['projectID'];
        $stage =$key['stage'];
        $status =$key['status'];
        $start =$key['start'];
        $end =$key['end'];

        $config["base_url"] = base_url() . "Administrator/project_activities/projectID_".$key['projectID']."_stage_".$key['stage']."_status_".$key['status']."_start_".$key['start']."_end_".$key['end']."/";
        $config["total_rows"] =$this->Administration_model->project_activities_info_count($projectID,$stage,$status,$start,$end,NULL);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->project_activities_info($projectID,$stage,$status,$start,$end,NULL,$page,$limit);
           
        $this->session->set_userdata('prev_url_path',base_url() . "Administrator/project_activities/projectID_".$key['projectID']."_stage_".$key['stage']."_status_".$key['status']."_start_".$key['start']."_end_".$key['end']."/".$page);
        $this->data['stages']=$this->Administration_model->project_stages();
        $this->data['searchform']='searchforms/admin/project_activity_search_form';
        $this->data['activeMenu']='projectmenu';
        $this->data['activeLink']='prjactlnk';
        $this->data['title']='Project Activities';
        $this->data['content']='admin/project_activities';
        $this->load->view('admin/template',$this->data);
    }
    
    function add_project_activity($id){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
        $this->form_validation->set_rules('projectID','Project ID','trim|required|xss_clean');
        $this->form_validation->set_rules('stage','Stage','trim|required|xss_clean');
        $this->form_validation->set_rules('activity_subject','Activity Subject','trim|required|xss_clean');
        $this->form_validation->set_rules('activity_summary','Activity Summary','trim|required|xss_clean');
        $this->form_validation->set_rules('status','Project Status','trim|required|xss_clean');
       
        if ($this->form_validation->run() == TRUE){
           
            $data=array(
                'projectID'=>$this->input->post('projectID'),
                'project_stage'=>$this->input->post('stage'),
                'activity_subject'=>$this->input->post('activity_subject'),
                'activity_summary'=>$this->input->post('activity_summary'),
                'status'=>$this->input->post('status'),
                'lastupdater'=>$this->session->userdata('user_id'),
                'lastupdate'=>date('Y-m-d H:i:s'),
            );

           
            $resp=$this->Administration_model->add_project_activity($data,$id);
            if($resp){

                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">data not saved!</div>';
            }
        }
        
       $this->data['activeMenu']='projectmenu';
       $this->data['activeLink']='prjactlnk';
       $this->data['title']="Add/Edit Activity";
       $this->data['id']=$id;
       $this->data['projects']=$this->Administration_model->running_projects();
       $this->data['stages']=$this->Administration_model->project_stages(NULL,NULL,'Active');
       $this->data['project_activity']=$this->Administration_model->project_activities($id);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/add_project_activity';
       $this->load->view('admin/template',$this->data);
    }
    
    function remove_project_activity($id){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('Welcome/logout', 'refresh');
            }

            $this->Administration_model->remove_project_activity($id);
            redirect('Administrator/project_activities','refresh');
    }
    
    function harvest_agreements(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('project')) {
            $key['project'] = $this->input->post('project');
            $this->data['project']=$this->input->post('project');
        }
        
        if ($this->input->post('crop')) {
            $key['crop'] = $this->input->post('crop');
            $this->data['crop']=$this->input->post('crop');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->input->post('start')) {
            $key['start'] = $this->input->post('start');
            $this->data['start']=$this->input->post('start');
        }

        if ($this->input->post('end')) {
            $key['end'] = $this->input->post('end');
            $this->data['end']=$this->input->post('end');
        }
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['farmer'] = $exp[1];
            $this->data['farmer']=$key['farmer'];
            
            $key['crop'] = $exp[3];
            $this->data['crop']=$key['crop'];
            
            $key['status'] = $exp[5];
            $this->data['status']=$key['status'];
            
            $key['start'] = $exp[7];
            $this->data['start']=$key['start'];
            
            $key['end'] = $exp[9];
            $this->data['end']=$key['end'];
            
        }

        $project =$key['project'];
        $crop =$key['crop'];
        $status =$key['status'];
        $start =$key['start'];
        $end =$key['end'];

        $config["base_url"] = base_url() . "Administrator/harvest_agreements/project_".$key['project']."_crop_".$key['crop']."_status_".$key['status']."_start_".$key['start']."_end_".$key['end']."/";
        $config["total_rows"] =$this->Administration_model->harvest_agreements_info_count($project,$crop,$status,$start,$end,NULL);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->harvest_agreements_info($project,$crop,$status,$start,$end,NULL,$page,$limit);
                
        $this->data['menuactive']='projectmenu';
        $this->data['activelink']='hrvstlnk';
        $this->data['title']='Harvest Agreements';
        $this->data['content']='admin/harvest_agreements';
        $this->load->view('admin/template',$this->data);
    }
    
    function add_harvest_agreement($id){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
        $this->form_validation->set_rules('project','Project ID','trim|required|xss_clean');
        $this->form_validation->set_rules('crop','Crop','trim|required|xss_clean');
        $this->form_validation->set_rules('cropID','Crop','trim|required|xss_clean');
        $this->form_validation->set_rules('harvestQuantity','Harvest Quantity','trim|required|xss_clean');
        $this->form_validation->set_rules('suitableQuantity','Suitable Quantity','trim|required|xss_clean');
        $this->form_validation->set_rules('market_price','Market Price','trim|required|xss_clean');
        $this->form_validation->set_rules('unsuitableQuantity','Unsuitable Quantity','trim|required|xss_clean');
        $this->form_validation->set_rules('expected_return','Expected Return','trim|required|xss_clean');
        $this->form_validation->set_rules('comments','Comments','trim|xss_clean');
       
        if ($this->form_validation->run() == TRUE){
           
            $data=array(
                'projectID'=>$this->input->post('project'),
                'cropID'=>$this->input->post('cropID'),
                'harvestQuantity'=>$this->input->post('harvestQuantity'),
                'suitableQuantity'=>$this->input->post('suitableQuantity'),
                'unsuitableQuantity'=>$this->input->post('unsuitableQuantity'),
                'market_price'=>$this->input->post('market_price'),
                'expected_return'=>$this->input->post('expected_return'),
                'ha_comments'=>$this->input->post('comments'),
            );

           
            $resp=$this->Administration_model->add_harvest_agreement($data,$id);
            if($resp){

                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">data not saved!</div>';
            }
        }
        
       $this->data['menuactive']='projectmenu';
       $this->data['activelink']='hrvstlnk';
       $this->data['title']="Add/Edit Harvest Agreement";
       $this->data['id']=$id;
       $this->data['projects']=$this->Administration_model->harvest_candidate_agreements();
       $this->data['agreement']=$this->Administration_model->harvest_agreements($id);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/add_harvest_agreement';
       $this->load->view('admin/template',$this->data);
    }
    
    function harvest_agreement_details($id){
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
       $this->data['menuactive']='projectmenu';
       $this->data['activelink']='hrvstlnk';
       $this->data['title']="Harvest Agreement Details";
       $this->data['agreement']=$this->Administration_model->harvest_agreements($id);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/harvest_agreement_details';
       $this->load->view('admin/template',$this->data);
    }
    
    function cancel_harvest_agreement($id){
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
        $data=array(
                'status'=>'Cancelled'
            );
        
        $this->Administration_model->add_harvest_agreement($data,$id);
        redirect('Administrator/harvest_agreements','refresh');
    }
    
    function delivery_destinations(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('destination')) {
            $key['destination'] = $this->input->post('destination');
            $this->data['destination']=$this->input->post('destination');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['destination'] = $exp[1];
            $this->data['destination']=$key['destination'];
            
            $key['status'] = $exp[3];
            $this->data['status']=$key['status'];
            
        }
        
        $destination =$key['destination'];
        $status =$key['status'];
        
        $config["base_url"] = base_url() . "Administrator/delivery_destinations/dst_".$key['destination']."_st_".$key['status']."/";
        $config["total_rows"] =$this->Administration_model->delivery_destinations_info_count($destination,$status);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->delivery_destinations_info($destination,$status,$page,$limit);
        
        $this->session->set_userdata('prev_url_path',base_url() . "Administrator/delivery_destinations/dst_".$key['destination']."_st_".$key['status']."/".$page);
        $this->data['activeMenu']='servicesmenu';
        $this->data['activeLink']='dlrdstlnk';
        $this->data['searchform']='searchforms/admin/delivery_destination_search_form';
        $this->data['title']='Delivery Destinations';
        $this->data['content']='admin/delivery_destinations';
        $this->load->view('admin/template',$this->data);
    }
    
    function add_delivery_destination($id){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
        
        $this->form_validation->set_rules('destination','Destination Name','trim|required|xss_clean');
        $this->form_validation->set_rules('cost','Delivery Cost','trim|required|xss_clean|numeric|greater_than[0]');
        
        if ($this->form_validation->run() == TRUE){
           
            $data=array(
                'destination'=>$this->input->post('destination'),
                'cost'=>$this->input->post('cost'),
            );

           
            $resp=$this->Administration_model->add_delivery_destination($data,$id);
            if($resp){

                $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">data saved!</div>';
            }else{
                $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">data not saved!</div>';
            }
        }
        
       $this->data['activeMenu']='servicesmenu';
       $this->data['activeLink']='dlrdstlnk';
       $this->data['title']="Add/Edit Delivery Destination";
       $this->data['id']=$id;
       $this->data['dlr_dst']=$this->Administration_model->delivery_destinations($id,NULL,NULL);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='admin/add_delivery_destination';
       $this->load->view('admin/template',$this->data);
    }
    
    function orders(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('orderid')) {
            $key['orderid'] = $this->input->post('orderid');
            $this->data['orderid']=$this->input->post('orderid');
        }
        
        if ($this->input->post('dst')) {
            $key['dst'] = $this->input->post('dst');
            $this->data['dst']=$this->input->post('dst');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->input->post('buyer')) {
            $key['buyer'] = $this->input->post('buyer');
            $this->data['buyer']=$this->input->post('buyer');
        }
        
        if ($this->input->post('from')) {
            $key['from'] = $this->input->post('from');
            $this->data['from']=$this->input->post('from');
        }
        
        if ($this->input->post('to')) {
            $key['to'] = $this->input->post('to');
            $this->data['to']=$this->input->post('to');
        }
        

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['orderid'] = $exp[1];
            $this->data['orderid']=$key['orderid'];
            
            $key['dst'] = $exp[3];
            $this->data['dst']=$key['dst'];
            
            $key['status'] = $exp[5];
            $this->data['status']=$key['status'];
            
            $key['buyer'] = $exp[7];
            $this->data['buyer']=$key['buyer'];
            
            $key['from'] = $exp[9];
            $this->data['from']=$key['from'];
            
            $key['to'] = $exp[11];
            $this->data['to']=$key['to'];
            
            $doc= $exp[13];
        }

        $orderid =$key['orderid'];
        $dst =$key['dst'];
        $status =$key['status'];
        $buyer =$key['buyer'];
        $from =$key['from'];
        $to =$key['to'];

        if($doc == 1){
            
            $data=$this->Administration_model->agro_orders(NULL,$orderid,$dst,$status,$buyer,NULL,$from,$to);
            require_once 'reports/orders_pdf.php';
        }
        
        $config["base_url"] = base_url() . "Administrator/orders/ord_".$key['orderid']."_dst_".$key['dst']."_status_".$key['status']."_by_".$key['buyer']."_st_".$key['from']."_end_".$key['to']."/";
        $config["total_rows"] =$this->Administration_model->agro_orders_info_count($orderid,$dst,$status,$buyer,NULL,$from,$to);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->agro_orders_info($orderid,$dst,$status,$buyer,NULL,$from,$to,$page,$limit);
        
        $this->session->set_userdata('prev_url_path',base_url() . "Administrator/orders/ord_".$key['orderid']."_dst_".$key['dst']."_status_".$key['status']."_by_".$key['buyer']."_st_".$key['from']."_end_".$key['to']."/".$page);        
        $this->data['destinations'] = $this->Administration_model->delivery_destinations(NULL,NULL,'Active');
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['title']='Orders';
        $this->data['searchform']='searchforms/admin/orders_search_form';
        $this->data['content']='admin/orders';
        $this->load->view('admin/template',$this->data);
    }
    
    function transactions(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('transactionid')) {
            $key['txnid'] = $this->input->post('transactionid');
            $this->data['transactionid']=$this->input->post('transactionid');
        }
        
        if ($this->input->post('orderid')) {
            $key['orderid'] = $this->input->post('orderid');
            $this->data['orderid']=$this->input->post('orderid');
        }
        
        if ($this->input->post('msisdn')) {
            $key['msisdn'] = $this->input->post('msisdn');
            $this->data['msisdn']=$this->input->post('msisdn');
        }
        
        if ($this->input->post('from')) {
            $key['from'] = $this->input->post('from');
            $this->data['from']=$this->input->post('from');
        }
        
        if ($this->input->post('to')) {
            $key['to'] = $this->input->post('to');
            $this->data['to']=$this->input->post('to');
        }
        

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['orderid'] = $exp[1];
            $this->data['orderid']=$key['orderid'];
            
            $key['txnid'] = $exp[3];
            $this->data['txnid']=$key['txnid'];
            
            $key['msisdn'] = $exp[5];
            $this->data['msisdn']=$key['msisdn'];
            
            $key['from'] = $exp[7];
            $this->data['from']=$key['from'];
            
            $key['to'] = $exp[9];
            $this->data['to']=$key['to'];
            
            $doc= $exp[11];
        }

        $orderid =$key['orderid'];
        $txnid =$key['txnid'];
        $msisdn =$key['msisdn'];
        $from =$key['from'];
        $to =$key['to'];

        if($doc == 1){
            
            $data=$this->Administration_model->agro_transactions(NULL,$orderid,$txnid,$msisdn,$from,$to);
            require_once 'reports/transactions_pdf.php';
        }
        
        $config["base_url"] = base_url() . "Administrator/transactions/ord_".$key['orderid']."_txnid_".$key['txnid']."_msisdn_".$key['msisdn']."_st_".$key['from']."_end_".$key['to']."/";
        $config["total_rows"] =$this->Administration_model->agro_transactions_info_count($orderid,$txnid,$msisdn,$from,$to,NULL);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->agro_transactions_info($orderid,$txnid,$msisdn,$from,$to,NULL,$page,$limit);
        
        $this->session->set_userdata('prev_url_path',base_url() . "Administrator/transactions/ord_".$key['orderid']."_txnid_".$key['txnid']."_msisdn_".$key['msisdn']."_st_".$key['from']."_end_".$key['to']."/".$page);        
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='txnlnk';
        $this->data['title']='Transactions';
        $this->data['searchform']='searchforms/admin/transactions_search_form';
        $this->data['content']='admin/transactions';
        $this->load->view('admin/template',$this->data);
    }
    
    function orderdetails($orderid){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
           
        if($this->input->post('confirm_order')){
            $error=FALSE;
            $orderid=$this->input->post('orderid');
            $ord_items=$this->Administration_model->agro_order_items($orderid,NULL,NULL,'pending');
            
            if($ord_items == NULL){
                $err_msg="no active products in the order!";
                $error=TRUE;
            }
            
            foreach($ord_items AS $vl){
                
                if($vl->productqty <> (0-$this->Inventory_model->total_stock($vl->productid,NULL,NULL,$vl->aoiID)->ttlStk)){
                    $err_msg="please reserve stock for $vl->productname before confirming the order";
                    $error=TRUE;
                    break;
                }
                
            }
            
            if(!$error){
                
                $data=array(
                'deliverydriver'=>$this->input->post('driver'),
                'deliverydate'=>$this->input->post('deliverydate'),
                'orderstatus'=>'confirmed',
                'modifiedby'=>$this->session->userdata('user_id'),
                'modifiedon'=>date('Y-m-d H:i:s'),
                );
                
                $item=array(
                        'status'=>'confirmed',
                        'modifiedby'=>$this->session->userdata('user_id'),
                        'modifiedon'=>date('Y-m-d H:i:s'),
                        );
                
                $text=sprintf($this->config->item('order_confirmation_message'),$ord_items[0]->buyer,$orderid,number_format(($ord_items[0]->productcost+$ord_items[0]->deliverycost),2)." /=",$this->config->item('order_expiry_hours'));
             
                $message=array(
                    'textmessage'=>$text,
                    'textrecipient'=>$ord_items[0]->buyercontact,
                    'status'=>'pending',
                    'createdon'=>date('Y-m-d H:i:s')
                );
                
                $this->Administration_model->update_order($data,$item,$message,$orderid);
            }else{
               
                $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">'.$err_msg
                                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>';
            }
            
            
        }
        
        if($this->input->post('cancel_order')){
            
            $orderid=$this->input->post('orderid');
            $ord_items=$this->Administration_model->agro_order_items($orderid,NULL,NULL,'pending');
            
            $data=array(
                        'reason'=>$this->input->post('reason'),
                        'orderstatus'=>'cancelled',
                        'modifiedby'=>$this->session->userdata('user_id'),
                        'modifiedon'=>date('Y-m-d H:i:s'),
                    );
            
            $item=array(
                        'status'=>'cancelled',
                        'modifiedby'=>$this->session->userdata('user_id'),
                        'modifiedon'=>date('Y-m-d H:i:s'),
                        );
            $text=sprintf($this->config->item('order_cancellation_message'),$orderid);
             
            $message=array(
                'textmessage'=>$text,
                'textrecipient'=>$ord_items[0]->buyercontact,
                'status'=>'pending',
                'createdon'=>date('Y-m-d H:i:s')
            );
            $this->Administration_model->update_order($data,$item,$message,$orderid);
            
        }
        
        $ord_items=$this->Administration_model->agro_order_items($orderid,NULL,NULL,NULL);
        //$this->session->set_userdata('prev_url_path',base_url() . "Administrator/orderdetails/".$orderid);
        $this->data['ord'] = $ord_items;
        $this->data['drivers'] = $this->Administration_model->get_member_info(NULL,NULL,NULL,$this->config->item('driver_group_id'));
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['title']='Order Details';
        $this->data['content']='admin/orderdetails';
        $this->load->view('admin/template',$this->data);
    }
    
    function remove_agro_order_item($id){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
           
        $ord_items=$this->Administration_model->agro_order_items(NULL,NULL,$id,'pending');
        
        $text=sprintf($this->config->item('order_item_removal_message'),$ord_items[0]->productname,$ord_items[0]->orderid);

         $message=array(
             'textmessage'=>$text,
             'textrecipient'=>$ord_items[0]->buyercontact,
             'status'=>'pending',
             'createdon'=>date('Y-m-d H:i:s')
         );

        $this->Administration_model->remove_agro_item_order($id,$ord_items,$message);
        redirect($this->session->userdata('prev_url_path'),'refresh');
    }
    
    function view_supply_details($item_order){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
        }
        
       
        $this->data['data'] = $this->Inventory_model->inventory_details(NULL,NULL,NULL,'Reserved',$item_order);
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['title']='Supply Details';
        $this->data['content']='admin/view_supply_details';
        $this->load->view('admin/template',$this->data);
    }
    
    function available_stock($product){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
        }
        
        if($this->uri->segment('3') <> NULL){
            
            $details=explode("_",$this->uri->segment('3'));
            $product=$details[0];
            
            $ord_itm_info=$this->Administration_model->agro_order_items($details[1],$details[0],$details[2]);
            
            if($ord_itm_info == NULL){
                
                redirect($this->session->userdata('prev_url_path'),'refresh');
            }
        }
       
        $this->data['data'] = $this->Inventory_model->available_stock($product,$supplier);
        $this->data['ordered_qty'] = $ord_itm_info[0]->productqty+$this->Inventory_model->total_stock($ord_itm_info[0]->productid,NULL,NULL,$ord_itm_info[0]->aoiID)->ttlStk;
        $this->data['order_item'] =$ord_itm_info;
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['title']='Available Stock';
        $this->data['content']='admin/available_stock';
        $this->load->view('admin/template',$this->data);
    }
    
    function reserve_stock(){
       if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }
      
            $b_detls=$this->Inventory_model->inventory_details($this->input->post('productid'),$this->input->post('supplierid'),$this->input->post('productbatch'));
            $data=array(
                'productid'=>$this->input->post('productid'),
                'supplier'=>$this->input->post('supplierid'),
                'quantity'=>0-$this->input->post('quantity'),
                'availabilityPeriod'=>$b_detls[0]->availabilityPeriod,
                'availableDate'=>$b_detls[0]->availableDate2,
                'productbatch'=>$this->input->post('productbatch'),
                'reference_orderID'=>$this->input->post('item_order_id'),
                'status'=>'Reserved',
                'createdon'=>date('Y-m-d H:i:s'),
                'createdby'=>$this->session->userdata('user_id'),
            );
            
            $sve=$this->Inventory_model->save_product_stock($data);
            if($sve){

               $message='product stock reserved.';
            }else{

                $message='sorry data saving error!';
            }
            
            echo $message;
            exit;
    }
    
    function inventory_stock(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('product')) {
            $key['product'] = $this->input->post('product');
            $this->data['product']=$this->input->post('product');
        }
        
        if ($this->input->post('batch')) {
            $key['batch'] = $this->input->post('batch');
            $this->data['batch']=$this->input->post('batch');
        }
        
        if ($this->input->post('supplier')) {
            $key['supplier'] = $this->input->post('supplier');
            $this->data['supplier']=$this->input->post('supplier');
        }
        
        if ($this->input->post('availability')) {
            $key['availability'] = $this->input->post('availability');
            $this->data['availability']=$this->input->post('availability');
        }
        
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['product'] = $exp[1];
            $this->data['product']=$key['product'];

            $key['batch'] = $exp[3];
            $this->data['batch']=$key['batch'];
            
            $key['availability'] = $exp[5];
            $this->data['availability']=$key['availability'];
            
            $key['supplier'] = $exp[7];
            $this->data['supplier']=$key['supplier'];
            
            $doc = $exp[9];
            
        }
        
        $product =$key['product'];
        $batch =$key['batch'];
        $availability =$key['availability'];
        $supplier=$key['supplier'];
        
        if($doc == 1){
            
            $data=$this->Inventory_model->inventory_stock($product,$supplier,$batch,$availability);
            require_once 'reports/products_inventory_pdf.php';
        }
        
        $config["base_url"] = base_url() . "Administrator/inventory_stock/product_".$key['product']."_batch_".$key['batch']."_av_".$key['availability']."_sp_".$key['supplier']."/";
        $config["total_rows"] =$this->Inventory_model->inventory_stock_info_count($product,$supplier,$batch,$availability);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Inventory_model->inventory_stock_info($product,$supplier,$batch,$availability,$page,$limit);
        
        $this->session->set_userdata('prev_url_path',base_url() . "Administrator/inventory_stock/product_".$key['product']."_batch_".$key['batch']."_av_".$key['availability']."_sp_".$key['supplier']."/".$page);
        $this->data['suppliers']=$this->Administration_model->product_suppliers();
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='inventorystocklnk';
        $this->data['searchform']='searchforms/admin/inventory_search_form';
        $this->data['title']='Product Stock';
        $this->data['content']='admin/inventory_stock';
        $this->load->view('admin/template',$this->data);
    }
    
    function inventory_details($product,$batch,$export=FALSE){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
        }
        
        $data=$this->Inventory_model->inventory_details($product,NULL,$batch);
        
        if($export){
        
            require_once 'reports/stock_inventory_details_pdf.php';
        }
        
        $this->data['data'] = $data;
        $this->data['product']=$product;
        $this->data['batch']=$batch;
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='inventorystocklnk';
        $this->data['title']='Inventory Stock Details';
        $this->data['content']='admin/inventory_details';
        $this->load->view('admin/template',$this->data);
    }
    
    function getAgreementID(){
       
        while(true){
            $d=date('D');
            switch ($d){
                case "Mon":
                    $agreementID="A";
                    break;
                case "Tue":
                    $agreementID="B";
                    break;
                case "Wed":
                    $agreementID="C";
                    break;
                case "Thu":
                    $agreementID="D";
                    break;
                case "Fri":
                    $agreementID="E";
                    break;
                case "Fri":
                    $agreementID="F";
                    break;
                default :
                    $agreementID="G";
            }

            $agreementID .="".substr(str_shuffle("0123456789"), -3);
            $agreementID .="-".date('Y-md');
            if($this->Administration_model->project_agreements($agreementID) == NULL){
                return $agreementID;
            }
        }
    }
    
    function check_session_account_validity(){
        if (!$this->ion_auth->logged_in()){
                return FALSE;
        }

        if(!$this->ion_auth->in_group('Administrator')){

            return FALSE;
        }

        return TRUE;
    }
    
    function getOutOfStockProducts(){
        
        $data=$this->Inventory_model->outOfStockProducts();
        
        $str="<table class='table table-bordered table-hover'>"
                . "<tr>"
                . "<th>No</th>"
                . "<th>Product</th>"
                . "<th>Quantity</th>"
                . "</tr>";
        
        if($data <> NULL){
            $i=1;
            foreach($data as $key=>$value){
                
                $str .="<tr>"
                        . "<td>&nbsp;&nbsp;".$i++."</td>"
                        . "<td>&nbsp;&nbsp;$value->productname</td>"
                        . "<td>&nbsp;&nbsp;$value->qty</td>"
                        . "</tr>";
            
            } 
        }else{
           $str .="<tr><td colspan='3' style='text-align:center'>No Out of Stock Product</td></tr>"; 
        }
        
        
        $str .="</table>";
        echo trim($str);
        exit;
    }
    
}