<?php defined('BASEPATH') OR exit('No direct script access allowed');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Home
 *
 * @author gmobile
 */
class Buyer extends CI_Controller{
   
    function __construct(){
        
            parent::__construct();
            $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
           
        $data=$this->Inventory_model->agro_cart_items(NULL,NULL,'active',$this->session->userdata('user_id'));
        $buyer=$this->Administration_model->get_member_info($this->session->userdata('user_id'),NULL,NULL,NULL);
        
        if($this->input->post('press_order')){
            
            if($data <> NULL){
                
                $agro_itms=array();
                $orderid=$this->getOrderID();
                foreach($data AS $v){
                    $ttl +=$v->productprice;

                    $agro_itms[]=array(
                            'orderid'=>$orderid,
                            'productid'=>$v->productid,
                            'productprice'=>$v->productprice,
                            'productqty'=>$v->quantity,
                            'status'=>'pending',
                            'createdby'=>$this->session->userdata('user_id'),
                            'createdon'=>date('Y-m-d H:i:s'),
                        );

                }
            
                $agro_order=array(
                    'orderid'=>$orderid,
                    'deliverytype'=>$this->input->post('dlvryType'),
                    'deliverydestination'=>$this->input->post('deliverydestination'),
                    'productcost'=>$ttl,
                    'orderstatus'=>'pending',
                    'orderedby'=>$this->session->userdata('user_id'),
                    'orderdate'=>date('Y-m-d H:i:s')
                );
               
                $agro_cart=array(
                    'refOrderID'=>$orderid,
                    'status'=>'processed',
                    'modifiedby'=>$this->session->userdata('user_id'),
                    'modifiedon'=>date('Y-m-d H:i:s')
                );
               
                $text=sprintf($this->config->item('buyer_order_message'),$buyer[0]->first_name.' '.$buyer[0]->last_name,$orderid);
             
                $message=array(
                    'textmessage'=>$text,
                    'textrecipient'=>$buyer[0]->msisdn,
                    'status'=>'pending',
                    'createdon'=>date('Y-m-d H:i:s')
                );
                
                if($this->Administration_model->save_order($agro_itms,$agro_order,$agro_cart,$message)){
                    
                    $this->data['message']='<div class="alert alert-success alert-dismissible fade show" role="alert">order saved!wait for confirmation from agent before making payment!'
                                                         . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                         <span aria-hidden="true">&times;</span>
                                                       </button></div>';
                }else{
                    
                    $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">order not saved!'
                                                         . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                         <span aria-hidden="true">&times;</span>
                                                       </button></div>';
                }
                
                $data=$this->Inventory_model->agro_cart_items(NULL,NULL,'active',$this->session->userdata('user_id'));
            }
        }
       
        $this->data['activeMenu']='homemenu';
        $this->data['activeLink']='';
        $this->data['data']=$data;
        $this->data['dlr_dstns']=$this->Administration_model->delivery_destinations(NULL,NULL,'Active');
        $this->data['searchform']='searchforms/buyer/cart_checkout';
        $this->data['title']='Home';
        $this->data['content']='buyer/home';
        $this->load->view('buyer/template',$this->data);
    }
    
    function crops(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
      
        if ($this->input->post('name')) {
            echo $this->input->post('name');
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->input->post('categ')) {
            $key['categ'] = $this->input->post('categ');
            $this->data['categ']=$this->input->post('categ');
        }
        

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['categ'] = $exp[3];
            $this->data['categ']=$key['categ'];
        }

        $name =$key['name'];
        $categ =$key['categ'];

        $config["base_url"] = base_url() . "Buyer/crops/name_".$key['name']."_categ_".$key['categ']."/";
        $config["total_rows"] =$this->Administration_model->products_info_count($name,$categ,'Active',$this->config->item('crop_product_identifier'));
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->products_info($name,$categ,'Active',$this->config->item('crop_product_identifier'),$page,$limit);
                
        $this->data['activeMenu']='productsmenu';
        $this->data['activeLink']='sellingcropslnk';
        $this->data['title']='Crops';
        $this->data['searchform']='searchforms/buyer/sellingcrops_search_form';
        $this->data['content']='buyer/crops';
        $this->load->view('buyer/template',$this->data);
    }
    
    function add_product_to_cart(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
           
           $data=array(
               'productid'=>$this->input->post('product'),
               'quantity'=>$this->input->post('quantity'),
               'productprice'=>$this->input->post('cost'),
               'status'=>'active',
               'createdon'=>date('Y-m-d H:i:s'),
               'createdby'=>$this->session->userdata('user_id'),
            );
           
           
           //check if product is not cleared in the cart
           $cart_item=$this->Inventory_model->agro_cart_items(NULL,$this->input->post('product'),'Active',$this->session->userdata('user_id'));
           if($cart_item == NULL){
               
                if(count($cart_item) > 5){
                    
                    $message='<div class="alert alert-danger alert-dismissible fade show" role="alert">maximum number of cart products reached!<br/>product not added to cart!'
                                                         . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                         <span aria-hidden="true">&times;</span>
                                                       </button></div>';
                }else{
                    
                    if($this->Inventory_model->save_cart_details($data)){
               
                        $message='<div class="alert alert-success alert-dismissible fade show" role="alert">product added to cart!please press your order within 24 hours before your product is removed from the cart.'
                                                         . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                         <span aria-hidden="true">&times;</span>
                                                       </button></div>';
                    }else{

                        $message='<div class="alert alert-danger alert-dismissible fade show" role="alert">product not added to cart!'
                                                         . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                         <span aria-hidden="true">&times;</span>
                                                       </button></div>';
                    }
                }
                
           }else{
               
               $message='<div class="alert alert-danger alert-dismissible fade show" role="alert">'.$cart_item[0]->productname.' exists in the cart!<br/>product not added to cart!'
                                                     . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                     <span aria-hidden="true">&times;</span>
                                                   </button></div>';
           }
           
           echo $message;
           exit;
    }
    
    function remove_cart_product($id){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
           
           $this->Inventory_model->remove_cart_agro_product($id);
           redirect('Buyer','refresh');
    }
    
    function orders(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('orderid')) {
            $key['orderid'] = $this->input->post('orderid');
            $this->data['orderid']=$this->input->post('orderid');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->input->post('from')) {
            $key['from'] = $this->input->post('from');
            $this->data['from']=$this->input->post('from');
        }
        
        if ($this->input->post('to')) {
            $key['to'] = $this->input->post('to');
            $this->data['to']=$this->input->post('to');
        }
        

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['orderid'] = $exp[1];
            $this->data['orderid']=$key['orderid'];
            
            $key['status'] = $exp[3];
            $this->data['status']=$key['status'];
            
            $key['from'] = $exp[5];
            $this->data['from']=$key['from'];
            
            $key['to'] = $exp[7];
            $this->data['to']=$key['to'];
        }

        $orderid =$key['orderid'];
        $status =$key['status'];
        $from =$key['from'];
        $to =$key['to'];

        $config["base_url"] = base_url() . "Buyer/orders/ord_".$key['orderid']."_dst_".$key['dst']."_status_".$key['status']."_st_".$key['from']."_end_".$key['to']."/";
        $config["total_rows"] =$this->Administration_model->agro_orders_info_count($orderid,NULL,$status,$this->session->userdata('user_id'),NULL,$from,$to);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->agro_orders_info($orderid,NULL,$status,$this->session->userdata('user_id'),NULL,$from,$to,$page,$limit);
        
        $this->session->set_userdata('prev_url_path',base_url() . "Buyer/orders/ord_".$key['orderid']."_dst_".$key['dst']."_status_".$key['status']."_st_".$key['from']."_end_".$key['to']."/".$page);        
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['title']='Orders';
        $this->data['searchform']='searchforms/buyer/orders_search_form';
        $this->data['content']='buyer/orders';
        $this->load->view('buyer/template',$this->data);
    }
    
    function pay($orderid){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['orderid']=$orderid;
        $this->data['title']='Order Payment';
        $this->data['content']='buyer/paymentPage';
        $this->load->view('buyer/template',$this->data);
    }
    
    function transactions(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('transactionid')) {
            $key['txnid'] = $this->input->post('transactionid');
            $this->data['transactionid']=$this->input->post('transactionid');
        }
        
        if ($this->input->post('orderid')) {
            $key['orderid'] = $this->input->post('orderid');
            $this->data['orderid']=$this->input->post('orderid');
        }
        
        if ($this->input->post('msisdn')) {
            $key['msisdn'] = $this->input->post('msisdn');
            $this->data['msisdn']=$this->input->post('msisdn');
        }
        
        if ($this->input->post('from')) {
            $key['from'] = $this->input->post('from');
            $this->data['from']=$this->input->post('from');
        }
        
        if ($this->input->post('to')) {
            $key['to'] = $this->input->post('to');
            $this->data['to']=$this->input->post('to');
        }
        

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['orderid'] = $exp[1];
            $this->data['orderid']=$key['orderid'];
            
            $key['txnid'] = $exp[3];
            $this->data['txnid']=$key['txnid'];
            
            $key['msisdn'] = $exp[5];
            $this->data['msisdn']=$key['msisdn'];
            
            $key['from'] = $exp[7];
            $this->data['from']=$key['from'];
            
            $key['to'] = $exp[9];
            $this->data['to']=$key['to'];
            
            $doc= $exp[11];
        }

        $orderid =$key['orderid'];
        $txnid =$key['txnid'];
        $msisdn =$key['msisdn'];
        $from =$key['from'];
        $to =$key['to'];

        if($doc == 1){
            
            $data=$this->Administration_model->agro_transactions(NULL,$orderid,$txnid,$msisdn,$from,$to,$this->session->userdata('user_id'));
            require_once 'reports/transactions_pdf.php';
        }
        
        $config["base_url"] = base_url() . "Buyer/transactions/ord_".$key['orderid']."_txnid_".$key['txnid']."_msisdn_".$key['msisdn']."_st_".$key['from']."_end_".$key['to']."/";
        $config["total_rows"] =$this->Administration_model->agro_transactions_info_count($orderid,$txnid,$msisdn,$from,$to,$this->session->userdata('user_id'));
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->agro_transactions_info($orderid,$txnid,$msisdn,$from,$to,$this->session->userdata('user_id'),$page,$limit);
        
        $this->session->set_userdata('prev_url_path',base_url() . "Buyer/transactions/ord_".$key['orderid']."_txnid_".$key['txnid']."_msisdn_".$key['msisdn']."_st_".$key['from']."_end_".$key['to']."/".$page);        
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='txnlnk';
        $this->data['title']='Transactions';
        $this->data['searchform']='searchforms/buyer/transactions_search_form';
        $this->data['content']='buyer/transactions';
        $this->load->view('buyer/template',$this->data);
    }
    
    function orderdetails($orderid){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
       
        $this->data['ord'] = $this->Administration_model->agro_order_items($orderid);
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['title']='Order Details';
        $this->data['content']='buyer/orderdetails';
        $this->load->view('buyer/template',$this->data);
    }
    
    function getOrderID(){
        
        while(true){
            switch (date('D')){
            case 'Mon':
                $orderid='ABC';
                break;
            case 'Tue':
                $orderid='EFG';
                break;
            case 'Wed':
                $orderid='MNO';
                break;
            case 'Thu':
                $orderid='JKL';
                break;
            case 'Fri':
                $orderid='RST';
                break;
            case 'Sat':
                $orderid='XYZ';
                break;
            case 'Sun':
                $orderid='UVW';
                break;
            default :
                return FALSE;
            }

            $orderid =$orderid.mt_rand(100,999);

            if($this->Administration_model->agro_orders(NULL,$orderid,NULL,NULL,NULL,NULL,NULL,NULL) <> NULL){
                continue;
            }

            return $orderid;
        }
        
    }
    
    function check_session_account_validity(){
        if (!$this->ion_auth->logged_in()){
                return FALSE;
        }

        if(!$this->ion_auth->in_group('Buyer')){

            return FALSE;
        }

        return TRUE;
    }
}