<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->pdf->start_pdf();
$this->pdf->SetSubject('gmobile');
$this->pdf->SetKeywords('gmobile');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Kilimo One Transactions</h3>';
$html .='<table border="1">';
$html .='<tr>'
            . '<th style="text-align:center;width:120px;font-weight:bold">S/NO</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Transaction ID</th>'
            . '<th style="text-align:center;width:250px;font-weight:bold">Order ID</th>'
            . '<th style="text-align:center;width:400px;font-weight:bold">Mobile No</th>'
            . '<th style="text-align:center;width:420px;font-weight:bold">Channel</th>'
            . '<th style="text-align:center;width:400px;font-weight:bold">Transaction Date</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Amount</th>'
        . '</tr>';
$i=1;
    foreach ($data as $vl) {
            $total +=$vl->amount;
            $html .='<tr>'
                        . '<td>&nbsp;&nbsp;'.$i++.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->transactionid.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->orderid.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->msisdn.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->channel.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->transactiondate.'</td>'
                        . '<td style="text-align:right">'.number_format($vl->amount,2).'&nbsp;&nbsp;</td>'
                    . '</tr>';
      }

     $html .='<tr>'
                . '<th style="text-align:right" colspan="6">Total&nbsp;&nbsp;</th>'
                . '<th style="text-align:right">'.number_format($total,2).'&nbsp;&nbsp;</th>'
            .'</tr>';
$html .='</table>';
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Transactions.pdf', 'D');
exit;
?>