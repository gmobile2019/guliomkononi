<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->pdf->start_pdf();
$this->pdf->SetSubject('gmobile');
$this->pdf->SetKeywords('gmobile');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Project Agreements</h3>';
$html .='<table border="1">';
$html .='<tr>'
            . '<th style="text-align:center;width:150px;font-weight:bold">S/NO</th>'
            . '<th style="text-align:center;width:260px;font-weight:bold">Agreement ID</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Product</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Farming Scheme</th>'
            . '<th style="text-align:center;width:350px;font-weight:bold">Farmer</th>'
            . '<th style="text-align:center;width:220px;font-weight:bold">Status</th>'
            . '<th style="text-align:center;width:330px;font-weight:bold">Formulation Date</th>'
            . '<th style="text-align:center;width:330px;font-weight:bold">Agreement Date</th>'
        . '</tr>';
$i=1;
    foreach ($data as $vl) {
            
            $html .='<tr>'
                        . '<td>&nbsp;&nbsp;'.$i++.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->agreementID.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->productname.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->farming_scheme.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->fullname.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->status.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->createdon.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->agreedon.'</td>'
                    . '</tr>';
      }

$html .='</table>';
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Project Agreements.pdf', 'D');
exit;