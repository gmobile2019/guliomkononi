<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->pdf->start_pdf();
$this->pdf->SetSubject('gmobile');
$this->pdf->SetKeywords('gmobile');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Kilimo One Orders</h3>';
$html .='<table border="1">';
$html .='<tr>'
            . '<th style="text-align:center;width:150px;font-weight:bold">S/NO</th>'
            . '<th style="text-align:center;width:200px;font-weight:bold">Order ID</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Destination</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Status</th>'
            . '<th style="text-align:center;width:350px;font-weight:bold">Order Date</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Delivery Fee</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Order Amount</th>'
        . '</tr>';
$i=1;
    foreach ($data as $vl) {
            
            $html .='<tr>'
                        . '<td>&nbsp;&nbsp;'.$i++.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->orderid.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->deliverydestination.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->orderstatus.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->orderdate.'</td>'
                        . '<td style="text-align:right">'.number_format($vl->deliverycost,2).'&nbsp;&nbsp;</td>'
                        . '<td style="text-align:right">'.number_format($vl->productcost,2).'&nbsp;&nbsp;</td>'
                    . '</tr>';
      }

$html .='</table>';
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Kilimo One Orders.pdf', 'D');
exit;
?>