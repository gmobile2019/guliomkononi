<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->pdf->start_pdf();
$this->pdf->SetSubject('gmobile');
$this->pdf->SetKeywords('gmobile');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Kilimo One Produts Inventory Details</h3>';
$html  .= '<h4 align="left">Product Name : '.$data[0]->productname.'</h4>';
$html  .= '<h4 align="left">Batch : '.$data[0]->productbatch.'</h4>';

$html .='<table border="1">';
$html .='<tr>'
            . '<th style="text-align:center;width:150px;font-weight:bold">S/NO</th>'
            . '<th style="text-align:center;width:500px;font-weight:bold">Supplier</th>'
            . '<th style="text-align:center;width:350px;font-weight:bold">Available Date</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Status</th>'
            . '<th style="text-align:center;width:350px;font-weight:bold">Action Date</th>'
            . '<th style="text-align:center;width:450px;font-weight:bold">Quantity</th>'
        . '</tr>';
$i=1;
    foreach ($data as $vl) {
        
            $html .='<tr>'
                        . '<td>&nbsp;&nbsp;'.$i++.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->supplier.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->availableDate.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->status.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->createdon.'</td>'
                        . '<td style="text-align:right">'.number_format($vl->quantity).' '.$vl->productunit.'&nbsp;&nbsp;</td>'
                    . '</tr>';
      }

$html .='</table>';
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Kilimo One Products Inventory Details.pdf', 'D');
exit;
?>