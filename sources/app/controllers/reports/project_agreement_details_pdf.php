<?php defined('BASEPATH') OR exit('No direct script access allowed');

$this->pdf->start_pdf();
$this->pdf->SetSubject('gmobile');
$this->pdf->SetKeywords('gmobile');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Project Agreements</h3>';
$html .='<table border="1">';
$html .='<tr>'
            .'<th style="text-align:center;width:1800px;font-weight:bold" colspan="2">Basic Details</th>'
        . '</tr>'
        .'<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Agreement ID</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->agreementID.'</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Farmer</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->fullname.'</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Crop</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->productname.'</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Farming Scheme</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->farming_scheme.'</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Sponsorship Scheme</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->sp_scheme.'</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Estimated Organization Investment Amount</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.number_format($agreement[0]->org_investment_amount, 2).' /= Tsh</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Estimated Farmer Investment Amount</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.number_format($agreement[0]->pvt_investment_amount, 2).' /= Tsh</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Market Price per unit</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.number_format($agreement[0]->market_price, 2).' /= Tsh</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Farmer\'s Profit Share</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->farmer_profit_share.' %</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Organization\'s Profit Share</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->org_profit_share.' %</td>'
        . '</tr>'
        .'<tr>'
            .'<th style="text-align:center;width:1800px;font-weight:bold" colspan="2">Farm Details</th>'
        . '</tr>'
        .'<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Farm Size</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->farm_size.' acres</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Farm Location Region</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->region.'</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Farm Location District</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->district.'</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Farm Location Ward</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->farm_ward.'</td>'
        . '</tr>'
        . '<tr>'
            .'<th style="width:900px;font-weight:bold">&nbsp;&nbsp;Farm Location Street</th>'
            .'<td style="width:900px;">&nbsp;&nbsp;'.$agreement[0]->farm_street.'</td>'
        . '</tr>';

$html .='</table>';
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Project Agreement Details.pdf', 'D');
exit;