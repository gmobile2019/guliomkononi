<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->pdf->start_pdf();
$this->pdf->SetSubject('gmobile');
$this->pdf->SetKeywords('gmobile');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Kilimo One Produts Inventory</h3>';
$html .='<table border="1">';
$html .='<tr>'
            . '<th style="text-align:center;width:150px;font-weight:bold">S/NO</th>'
            . '<th style="text-align:center;width:500px;font-weight:bold">Product Name</th>'
            . '<th style="text-align:center;width:500px;font-weight:bold">Product Batch</th>'
            . '<th style="text-align:center;width:400px;font-weight:bold">Available Date</th>'
            . '<th style="text-align:center;width:450px;font-weight:bold">Quantity</th>'
        . '</tr>';
$i=1;
    foreach ($data as $vl) {
            $prod=$this->Administration_model->products($vl->productid,NULL,NULL,NULL,NULL);
            $inv_dtl=$this->Inventory_model->inventory_details($vl->productid,NULL,$vl->productbatch);
                        
            $html .='<tr>'
                        . '<td>&nbsp;&nbsp;'.$i++.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->productname.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->productbatch.'</td>'
                        . '<td>&nbsp;&nbsp;'.$inv_dtl[0]->availableDate.'</td>'
                        . '<td style="text-align:right">'.number_format($vl->quantity).' '.$prod[0]->productunit.'&nbsp;&nbsp;</td>'
                    . '</tr>';
      }

$html .='</table>';
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Kilimo One Products Inventory.pdf', 'D');
exit;
?>