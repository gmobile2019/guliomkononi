<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$this->pdf->start_pdf();
$this->pdf->SetSubject('gmobile');
$this->pdf->SetKeywords('gmobile');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Kilimo One Crops</h3>';
$html .='<table border="1">';
$html .='<tr>'
            . '<th style="text-align:center;width:120px;font-weight:bold">S/NO</th>'
            . '<th style="text-align:center;width:400px;font-weight:bold">Crop Name</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Category</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Status</th>'
            . '<th style="text-align:center;width:300px;font-weight:bold">Unit</th>'
            . '<th style="text-align:center;width:400px;font-weight:bold">Unit Price</th>'
        . '</tr>';
$i=1;
    foreach ($data as $vl) {
            
            $html .='<tr>'
                        . '<td>&nbsp;&nbsp;'.$i++.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->productname.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->productcategory.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->status.'</td>'
                        . '<td>&nbsp;&nbsp;'.$vl->productunit.'</td>'
                        . '<td style="text-align:right">'.number_format($vl->productprice,2).'&nbsp;&nbsp;</td>'
                    . '</tr>';
      }

$html .='</table>';
$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Products.pdf', 'D');
exit;
?>