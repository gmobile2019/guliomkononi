<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Description of Supplier
 *
 * @author gmobile
 */
class Supplier extends CI_Controller{
    
    function __construct(){
        
            parent::__construct();
            $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
            $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
            $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
            $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        $this->data['activeMenu']='homemenu';
        $this->data['activeLink']='';
        $this->data['title']='Home';
        $this->data['content']='supplier/home';
        $this->load->view('supplier/template',$this->data);
    }
    
    function send_crop_request($id=null){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }

        $this->form_validation->set_rules('cropname','Crop Name','trim|required|xss_clean');
        $this->form_validation->set_rules('description','Description','trim|xss_clean|required');
        $this->form_validation->set_rules('cropcategory','Crop Category','trim|required|xss_clean');
        
        
        if ($this->form_validation->run() == TRUE){
            $error=FALSE;
            $photoUpload=TRUE;
            
                $data=array(
                    'productname'=>$this->input->post('cropname'),
                    'description'=>$this->input->post('description'),
                    'productcategory'=>$this->input->post('cropcategory'),
                    'status'=>'Pending',
                    'producttype'=>$this->config->item('crop_product_identifier'),
                );

                if($_FILES['cropphoto']['name'] <> null){

                    $config['upload_path'] = $this->config->item('sellingcrops_img_path');
                    $config['allowed_types'] = 'jpg|jpeg';
                    $img_name=explode('.',$_FILES['cropphoto']['name']);
                    $img_ext=end($img_name);
                    $img_name=time().".".$img_ext;
                    $config['file_name']=$img_name;
                    $config['max_size']=1024;
                    $config['overwrite']=true;
                    $this->upload->initialize($config);

                   if($this->upload->do_upload('cropphoto')){

                        $config['image_library'] = 'gd2';
                        $config['source_image'] = $this->config->item('sellingcrops_img_path').$img_name;
                        $config['create_thumb'] = TRUE;
                        $config['maintain_ratio'] = TRUE;
                        $config['width']         = 300;
                        $config['height']       = 170;

                        $this->load->library('image_lib', $config);

                       if(! $this->image_lib->resize()){

                            $this->data['message']=$this->image_lib->display_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">','<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>');
                            $photoUpload=FALSE;
                       }else{

                           $data['productimage']=$img_name;
                       }

                    }else{
                      $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger alert-dismissible fade show" role="alert">','<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>');
                      $photoUpload=FALSE;
                    }
                }
                
                //if not photo uploaded not an editing action
                if(!$photoUpload && $id == NULL){
                    $error=TRUE;
                }
                 
                if(!$error){
                    
                    $sve=$this->Administration_model->save_product($data,$id);
                    if($sve){

                       $this->data['message']='<div class="alert alert-success alert-dismissible fade show" role="alert">crop request sent.'
                                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                              </button></div>';
                    }else{

                        $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">sorry data saving error!'
                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></div>';
                    }
                }else{
                    
                    if($this->data['message'] == NULL){
                        $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">sorry data saving error!'
                                . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                  </button></div>';
                    }
                }

       }

       $this->data['activeMenu']='productsmenu';
       $this->data['activeLink']='sellingcropslnk';
       $this->data['title']="Send Crop Request";
       $this->data['id']="$id";
       $this->data['crop']=$this->Administration_model->products($id,NULL,NULL,NULL,$this->config->item('crop_product_identifier'));
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='supplier/crop_request';
       $this->load->view('supplier/template',$this->data);
    }
    
    function crops(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('name')) {
            $key['name'] = $this->input->post('name');
            $this->data['name']=$this->input->post('name');
        }
        
        if ($this->input->post('categ')) {
            $key['categ'] = $this->input->post('categ');
            $this->data['categ']=$this->input->post('categ');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }


       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['name'] = $exp[1];
            $this->data['name']=$key['name'];
            
            $key['categ'] = $exp[3];
            $this->data['categ']=$key['categ'];
            
            $key['status'] = $exp[5];
            $this->data['status']=$key['status'];

        }

        $name =$key['name'];
        $categ =$key['categ'];
        $status =$key['status'];

        $config["base_url"] = base_url() . "Supplier/crops/name_".$key['name']."_categ_".$key['categ']."_status_".$key['status']."/";
        $config["total_rows"] =$this->Administration_model->products_info_count($name,$categ,$status,$this->config->item('crop_product_identifier'));
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Administration_model->products_info($name,$categ,$status,$this->config->item('crop_product_identifier'),$page,$limit);
                
        $this->data['activeMenu']='productsmenu';
        $this->data['activeLink']='sellingcropslnk';
        $this->data['title']='Selling Crops';
        $this->data['searchform']='searchforms/supplier/sellingcrops_search_form';
        $this->data['content']='supplier/crops';
        $this->load->view('supplier/template',$this->data);
    }
    
    function product_stocking($id=null){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
        {
                //redirect them to the login page
                redirect('Welcome/logout', 'refresh');
        }

        $this->form_validation->set_rules('product','Product','trim|required|xss_clean');
        $this->form_validation->set_rules('quantity','Quantity','trim|xss_clean|required');
        $this->form_validation->set_rules('availabilityPeriod','Availability','trim|required|xss_clean');
        
        
        if ($this->form_validation->run() == TRUE){
            
            if($this->input->post('availabilityPeriod') == 'Now'){
                
                $availableDate=date('Y-m-d');
                
            }else{
                $date = new DateTime(date('Y-m-d'));
                $date->add(new DateInterval('P'.$this->input->post('availabilityPeriod').'D'));
                $availableDate=$date->format('Y-m-d');
            }
          
                $data=array(
                    'productid'=>$this->input->post('product'),
                    'supplier'=>$this->session->userdata('user_id'),
                    'quantity'=>$this->input->post('quantity'),
                    'availabilityPeriod'=>$this->input->post('availabilityPeriod'),
                    'availableDate'=>$availableDate,
                    'productbatch'=>$this->getProductBatch(),
                    'status'=>'Stocking',
                    'comments'=>$this->input->post('comments'),
                    'createdon'=>date('Y-m-d H:i:s'),
                    'createdby'=>$this->session->userdata('user_id'),
                );
                
                $sve=$this->Inventory_model->save_product_stock($data);
                if($sve){

                   $this->data['message']='<div class="alert alert-success alert-dismissible fade show" role="alert">product stock saved.'
                                            . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button></div>';
                }else{

                    $this->data['message']='<div class="alert alert-danger alert-dismissible fade show" role="alert">sorry data saving error!'
                            . '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button></div>';
                }

       }

       $this->data['activeMenu']='inventorymenu';
       $this->data['activeLink']='inventorystocklnk';
       $this->data['title']="Product Stocking";
       $this->data['products']=$this->Administration_model->products($id,NULL,NULL,NULL,NULL);
       $this->data['userInfo']=$this->Administration_model->current_user_info();
       $this->data['content']='supplier/product_stocking';
       $this->load->view('supplier/template',$this->data);
    }
    
    function inventory_stock(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
           }
        
        if ($this->input->post('product')) {
            $key['product'] = $this->input->post('product');
            $this->data['product']=$this->input->post('product');
        }
        
        if ($this->input->post('batch')) {
            $key['batch'] = $this->input->post('batch');
            $this->data['batch']=$this->input->post('batch');
        }
        
        if ($this->input->post('availability')) {
            $key['availability'] = $this->input->post('availability');
            $this->data['availability']=$this->input->post('availability');
        }
        
        
        if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['product'] = $exp[1];
            $this->data['product']=$key['product'];

            $key['batch'] = $exp[3];
            $this->data['batch']=$key['batch'];
            
            $key['availability'] = $exp[5];
            $this->data['availability']=$key['availability'];
            
        }
        
        $product =$key['product'];
        $batch =$key['batch'];
        $availability =$key['availability'];
        $supplier=$this->session->userdata('user_id');
        
        $config["base_url"] = base_url() . "Supplier/inventory_stock/product_".$key['product']."_batch_".$key['batch']."_av_".$key['availability']."/";
        $config["total_rows"] =$this->Inventory_model->inventory_stock_info_count($product,$supplier,$batch,$availability);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Inventory_model->inventory_stock_info($product,$supplier,$batch,$availability,$page,$limit);
        
        $this->session->set_userdata('prev_url_path',base_url() . "Supplier/inventory_stock/product_".$key['product']."_batch_".$key['batch']."_av_".$key['availability']."/".$page);
        $this->data['activeMenu']='inventorymenu';
        $this->data['activeLink']='inventorystocklnk';
        $this->data['searchform']='searchforms/supplier/inventory_search_form';
        $this->data['title']='My Stock';
        $this->data['content']='supplier/inventory_stock';
        $this->load->view('supplier/template',$this->data);
    }
    
    function inventory_details($product,$batch){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
        }
        
        $this->data['data'] = $this->Inventory_model->inventory_details($product,$this->session->userdata('user_id'),$batch);
        $this->data['product']=$product;
        $this->data['activeMenu']='inventorymenu';
        $this->data['activeLink']='inventorystocklnk';
        $this->data['title']='Inventory Stock Details';
        $this->data['content']='supplier/inventory_details';
        $this->load->view('supplier/template',$this->data);
    }
    
    function orders(){
        if (!$this->check_session_account_validity()){

                   //redirect them to the login page
                   redirect('Welcome/logout', 'refresh');
        }
       
        if ($this->input->post('product')) {
            $key['product'] = $this->input->post('product');
            $this->data['product']=$this->input->post('product');
        }
        
        if ($this->input->post('batch')) {
            $key['batch'] = $this->input->post('batch');
            $this->data['batch']=$this->input->post('batch');
        }
        
        if ($this->input->post('status')) {
            $key['status'] = $this->input->post('status');
            $this->data['status']=$this->input->post('status');
        }
        
        if ($this->input->post('from')) {
            $key['from'] = $this->input->post('from');
            $this->data['from']=$this->input->post('from');
        }
        
        if ($this->input->post('to')) {
            $key['to'] = $this->input->post('to');
            $this->data['to']=$this->input->post('to');
        }
        

       if ($this->uri->segment(3)) {
            $exp = explode("_", $this->uri->segment(3));

            $key['product'] = $exp[1];
            $this->data['product']=$key['product'];
            
            $key['batch'] = $exp[3];
            $this->data['batch']=$key['batch'];
            
            $key['status'] = $exp[5];
            $this->data['status']=$key['status'];
            
            $key['from'] = $exp[7];
            $this->data['from']=$key['from'];
            
            $key['to'] = $exp[9];
            $this->data['to']=$key['to'];
        }

        $product =$key['product'];
        $batch =$key['batch'];
        $status =$key['status'];
        $from =$key['from'];
        $to =$key['to'];

        $config["base_url"] = base_url() . "Supplier/orders/prd_".$key['product']."_status_".$key['status']."_st_".$key['from']."_end_".$key['to']."/";
        $config["total_rows"] =$this->Inventory_model->inventory_details_info_count($product,$this->session->userdata('user_id'),$batch,$status,$from,$to);
        $config["per_page"] =20;
        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['num_tag_open'] = '<li class="page-item">';
        $config['num_tag_close'] = '</li>';


        $config['prev_tag_open'] = '<li class="page-item">';
        $config['prev_tag_close'] = '</li>';

        $config['next_tag_open'] = '<li class="page-item">';
        $config['next_tag_close'] = '</li>';

        $config['next_link']='&raquo;';
        $config['prev_link']='&laquo;';
        $config['cur_tag_open'] = '<span class="btn btn-info">';
        $config['cur_tag_close'] = '</span>';
        $config["uri_segment"] =4;
        $config["num_links"] = 3; // round($choice);

        $this->pagination->initialize($config);
        $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $limit=$config["per_page"];
        $this->data['per_page']=$page;
        $this->data['links'] = $this->pagination->create_links();
        $this->data['data'] = $this->Inventory_model->inventory_details_info($product,$this->session->userdata('user_id'),$batch,$status,$from,$to,$page,$limit);
        
        $this->data['searchform']='searchforms/supplier/orders_search_form';
        $this->data['activeMenu']='businessmenu';
        $this->data['activeLink']='ordlnk';
        $this->data['title']='Orders';
        $this->data['content']='supplier/orders';
        $this->load->view('supplier/template',$this->data);
    }
    
    function getProductBatch(){
        
        while(true){
            $batch=substr(str_shuffle("ABCDEFGHIJKLMNOPQ"),-3).substr(str_shuffle("0123456789"),-3);
            
            if($this->Inventory_model->inventory_details(NULL,NULL,$batch) == NULL){
                return $batch;
            }
        }
    }
    
    function check_session_account_validity(){
        if (!$this->ion_auth->logged_in()){
                return FALSE;
        }

        if(!$this->ion_auth->in_group('Supplier')){

            return FALSE;
        }

        return TRUE;
    }
}
