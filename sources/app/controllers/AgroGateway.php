<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

defined('BASEPATH') OR exit('No direct script access allowed');

require(APPPATH.'/libraries/REST_Controller.php');

/**
 * Description of AgroGateway
 *
 * @author gmobile
 */
class AgroGateway extends REST_Controller{
    
    
    function __construct(){
        
        parent::__construct();
    }
    
    
    function agroAPI_post(){
        $logf='./logs/request_'.date('Y-m-d').'.log';
        
        $logfile=file_exists($logf);

        if($logfile){

            $filesize=filesize($logf);

            if($filesize > 1048576){
                rename($logf,"./logs/request_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        
        $reqid=uniqid();
        
        //receive request
        $request =$this->input->raw_input_stream;
        error_log("$request ".date('Y-m-d H:i:s')."[$reqid] \n", 3, $logf);
        $data=json_decode($request);
        
        if(!$data){
            
            $arr=array('respSTATUS'=>'invalid_data');
            error_log(json_encode($arr)." [$reqid]".date('Y-m-d H:i:s')."\n", 3, $logf);
            $this->response($arr, 200);
            exit;
        }
        
        //verify request header
        if(sha1($this->config->item('authorization_code').' '.$data) <> $this->input->get_request_header('CRC')){
            
            $arr=array('respSTATUS'=>'unauthorized_request');
            error_log(json_encode($arr)." [$reqid]".date('Y-m-d H:i:s')."\n", 3, $logf);
            $this->response($arr, 200);
            exit;
        }
        
        if((string)$data->method === "getOrder"){
            
            $order=$this->fetchOrder((string)$data->orderid);
            
            if($order <> NULL){
                
                $arr=array('respSTATUS'=>'success','orderID'=>$order[0]->orderid,'transactionID'=>$order[0]->refTransaction,'orderSTATUS'=>$order[0]->orderstatus,'orderAMOUNT'=>$order[0]->productcost+$order[0]->deliverycost);
                error_log(json_encode($arr)." [$reqid]".date('Y-m-d H:i:s')."\n", 3, $logf);
            }else{
                
                $arr=array('respSTATUS'=>'unknown_orderid');
                error_log(json_encode($arr)." [$reqid]".date('Y-m-d H:i:s')."\n", 3, $logf);
            }
            
        }else if((string)$data->method === "postTransaction"){
            
            $save=$this->processTransaction((string)$data->paymentMode,(string)$data->transactionID,(string)$data->msisdn,(string)$data->orderID,(string)$data->transactionDate,(float)$data->amount);
            
            if($save){
                
                $arr=array('respSTATUS'=>'success');
                error_log(json_encode($arr)." [$reqid]".date('Y-m-d H:i:s')."\n", 3, $logf);
            }else{
                
                $arr=array('respSTATUS'=>'server_error');
                error_log(json_encode($arr)." [$reqid]".date('Y-m-d H:i:s')."\n", 3, $logf);
            }
        }else{
            
            $arr=array('respSTATUS'=>'unknown_method');
            error_log(json_encode($arr)." [$reqid]".date('Y-m-d H:i:s')."\n", 3, $logf);
        }
        
        $this->response($arr, 200);
        exit;
    }
    
    private function processTransaction($paymentMode,$txnID,$msisdn,$orderID,$transactionDate,$amount){
        
        $str=null;
        $ord_items=$this->Administration_model->agro_order_items($orderID,NULL,NULL,'confirmed');
        
        foreach($ord_items AS $vl){
            
            $str .="'$vl->aoiID',";
        }
        
        $order_item_ids=rtrim($str, ",");
        
        $array=array(
            'channel'=>$paymentMode,
            'orderid'=>$orderID,
            'amount'=>$amount,
            'msisdn'=>$msisdn,
            'transactionid'=>$txnID,
            'transactiondate'=>$transactionDate,
            'lastupdate'=>date('Y-m-d H:i:s'),
        );
        
        $text=sprintf($this->config->item('order_payment_message'),$ord_items[0]->buyer,$orderID);
             
        $message=array(
            'textmessage'=>$text,
            'textrecipient'=>$ord_items[0]->buyercontact,
            'status'=>'pending',
            'createdon'=>date('Y-m-d H:i:s')
        );
          
        return $this->Administration_model->save_transaction($array,$message,$order_item_ids);
    }
    
    private function fetchOrder($orderid){
        
        return $this->Administration_model->agro_orders(NULL,$orderid,NULL,NULL,NULL,NULL,NULL,NULL);
        
    }
}
