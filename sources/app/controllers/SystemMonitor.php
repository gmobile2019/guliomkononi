<?php defined('BASEPATH') OR exit('No direct script access allowed');

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class SystemMonitor extends CI_Controller{
    
    function __construct(){
        
        parent::__construct();
        $this->load->model('SystemMonitor_model');
    }
    
    /*
     * cancel expired cart products
     */
    function cancelExpiredCartItems(){
        $logf='./logs/expired_cart_items_'.date('Y-m-d').'.log';
        
        $lock =file_exists('./locks/crt.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/crt.lock','w'); 
        }
        
        $logfile=file_exists($logf);

        if($logfile){

            $filesize=filesize($logf);

            if($filesize > 1048576){
                rename($logf,"./logs/expired_cart_items_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        error_log("started ".date('Y-m-d H:i:s')."\n", 3, $logf);
        echo "started ".date('Y-m-d H:i:s')."\n";
        
        $date = new DateTime(date('Y-m-d H:i:s'));
        $date->sub(new DateInterval('PT'.$this->config->item('cart_item_expiry_hours').'H'));
        $limittimestamp=$date->format('Y-m-d H:i:s');
        
        
        $data=$this->SystemMonitor_model->cartItems('active',$limittimestamp);
        $ids=NULL;
        
        error_log("Items count : ".count($data)."\n", 3, $logf);
        echo "Items count : ".count($data)."\n";
        
        foreach($data AS $vl){
            
            $ids .="'$vl->id',";
        }
        
        if($ids <> NULL){
            
            $ids=  rtrim($ids,',');
            if($this->SystemMonitor_model->cancel_cartItem($vl->id)){
                
                error_log("Items ($ids) cancelled \n", 3, $logf);
                echo "Items ($ids) cancelled \n";
            }else{
                
                error_log("Error item $vl->id \n", 3, $logf);
                echo "Error item $vl->id \n";
            }
        }
        error_log("completed ".date('Y-m-d H:i:s')."\n", 3, $logf);
        echo "completed ".date('Y-m-d H:i:s')."\n";
        
        fclose($lk);
        unlink('./locks/crt.lock');
        exit;
    }
    
    /*
     * cancel expired orders
     */
    function cancelExpiredOrders(){
        $logf='./logs/expired_orders_'.date('Y-m-d').'.log';
        
        $lock =file_exists('./locks/ord.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/ord.lock','w'); 
        }
        
        $logfile=file_exists($logf);

        if($logfile){

            $filesize=filesize($logf);

            if($filesize > 1048576){
                rename($logf,"./logs/expired_orders_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        error_log("started ".date('Y-m-d H:i:s')."\n", 3, $logf);
        echo "started ".date('Y-m-d H:i:s')."\n";
        
       
        $date = new DateTime(date('Y-m-d H:i:s'));
        $date->sub(new DateInterval('PT'.$this->config->item('order_expiry_hours').'H'));
        $limittimestamp=$date->format('Y-m-d H:i:s');
        
        
        $data=$this->SystemMonitor_model->agro_orders('confirmed',$limittimestamp);
       
        error_log("Orders count : ".count($data)."\n", 3, $logf);
        echo "Orders count : ".count($data)."\n";
        $items=NULL;
        $orderids=NULL;
        foreach($data AS $vl){
            
            $orderids .="'$vl->orderid',";
            $ord_itms=$this->SystemMonitor_model->agro_order_items($vl->orderid);
            
            foreach($ord_itms as $v){
                
                $items .="'$v->id',";
            }
            
        }
        
        if($orderids <> NULL){
            
            $orderids=rtrim($orderids,',');
            $items=rtrim($items,',');
          
            if($this->SystemMonitor_model->cancel_order($orderids,$items)){
                
                error_log("Cancelled orders ($orderids) & items ($items) \n", 3, $logf);
                echo "Cancelled orders ($orderids) & items ($items) \n";
            }else{
                
                error_log("Error orders ($orderids) & items ($items) \n", 3, $logf);
                echo "Error orders ($orderids) & items ($items) \n";
            }
        }
        
        error_log("completed ".date('Y-m-d H:i:s')."\n", 3, $logf);
        echo "completed ".date('Y-m-d H:i:s')."\n";
        
        fclose($lk);
        unlink('./locks/ord.lock');
        exit;
    }
    
    /*
     * send messages
     */
    function agroMessaging(){
        $logf='./logs/msgs_'.date('Y-m-d').'.log';
        
        $lock =file_exists('./locks/msg.lock');
        if ($lock) {

            echo "LOCKED\n";
            exit;

        }else{
            
            $lk=fopen('./locks/msg.lock','w'); 
        }
        
        $logfile=file_exists($logf);

        if($logfile){

            $filesize=filesize($logf);

            if($filesize > 1048576){
                rename($logf,"./logs/msgs_".date('His')."_".date('Y-m-d').'.log');
            }
        }
        
        $counter=0;
        while($counter <= 100){
            $counter++;
            error_log("started ".date('Y-m-d H:i:s')."\n", 3, $logf);
            echo "started ".date('Y-m-d H:i:s')."\n";

            $data=$this->SystemMonitor_model->agro_messages('pending');

            error_log("Message count : ".count($data)."\n", 3, $logf);
            echo "Message count : ".count($data)."\n";
            $ids=NULL;

            foreach($data as $vl){

                if($this->sendMessage($vl->textmessage,$vl->textrecipient)){

                    $ids .="'$vl->id',";
                }
            }

            if($ids <> NULL){

                $ids= rtrim($ids,',');
                if($this->SystemMonitor_model->update_agro_message($ids)){

                    error_log("sent messages id ($ids) \n", 3, $logf);
                    echo "sent messages id ($ids) \n";
                }else{

                    error_log("Error ids ($ids) \n", 3, $logf);
                    echo "Error ids ($ids) \n";
                }
            }

            error_log("completed ".date('Y-m-d H:i:s')."\n", 3, $logf);
            echo "completed ".date('Y-m-d H:i:s')."\n";
        }
        
        
        fclose($lk);
        unlink('./locks/msg.lock');
        exit;
    }
    
    function sendMessage($text,$recipient){
        $logf='./logs/msgs_'.date('Y-m-d').'.log';
        
        $msg='<?xml version="1.0" encoding="UTF-8"?>
                <request>
                <token>'.$this->config->item('message_access_token').'</token>
                <username>'.$this->config->item('message_access_username').'</username>
                <password>'.$this->config->item('message_access_password').'</password>
                <data>
                    <sendercode>'.$this->config->item('message_sender').'</sendercode>
                    <recipient>'.$recipient.'</recipient>
                    <sms>'.$text.'</sms>
                    <timestamp></timestamp>
                    <dlr>0</dlr>
                    <messageid></messageid>
                    <dlrurl></dlrurl>
                </data>
            </request>';
        
        error_log($msg,3, $logf);
        echo $msg."\n";
        
        $header= array('Content-Type: text/xml','Content-Length: '.strlen($msg));
               
        $ch=curl_init($this->config->item('message_server_url'));

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $msg);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch,CURLOPT_TIMEOUT,5);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

        $err_no=curl_errno($ch);
        $err=curl_error($ch);
        
        $response=curl_exec($ch);
        
        error_log("$err_no|$err ".date('Y-m-d H:i:s')."\n", 3, $logf);
        echo "$err_no|$err ".date('Y-m-d H:i:s')."\n"; 

        error_log($response."\n", 3, $logf);
        echo $response."\n"; 
        curl_close($ch);
        
        if($err_no == 0 && (string)simplexml_load_string($response)->code == '100'){
            
            return TRUE;
        }
       
        return FALSE;
    }
}
