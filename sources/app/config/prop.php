<?php defined('BASEPATH') OR exit('No direct script access allowed');
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$config['admin_group_id']=1;

$config['buyer_group_id']=2;

$config['supplier_group_id']=3;

$config['manager_group_id']=4;

$config['farmer_group_id']=5;

$config['driver_group_id']=6;

$config['agent_group_id']=7;

$config['crop_product_identifier']='Crop';

$config['sellingcrops_img_path']='./imgs/products/sellingcrops/';

$config['user_photo_path']='./imgs/user/';

$config['driver_license_path']='./imgs/driverlicense/';


$config['mailMessage']="Hello %s,\nKaribu kwenye tovuti ya Gulio Mkononi. Chini ni sifa zako za kuingia kufikia akaunti yako.\n Jina lako : %s\n Neno La Siri : %s\n\nBidhaa za ubora na kuridhika kwa Wateja ni kipaumbele chetu\n\n\n\n Wako mwaminifu.\n Admin,\nCEO,\n Gulio Mkononi";


$config['from_email_address']='safiely@gmail.com';

$config['failure_reply_email_address']='guliomkononi@info.co.tz';

$config['from_name']='Gulio mkononi';

$config['reply_to_email_address']='guliomkononi@info.co.tz';

$config['reply_to_name']='No Reply';

$config['captcha_expiration_time']=900;

/*************************system monitor configurations**************************************************/

$config['cart_item_expiry_hours']=2;

$config['order_expiry_hours']=2;

$config['authorization_code']='2625';
//for the agro gateway interface

/********************************message configurations**********************************************/

$config['message_server_url']='https://188.64.190.121/gsms/Api/normalReceiver';

$config['message_access_token']='MSR48EUD9j0AgoT5Jf1VchL3nrsPayxlwOBbqp6iFzZ2KveCNd';

$config['message_access_username']='Gmobile_ERM';

$config['message_access_password']='p893qv@4';

$config['message_sender']='10000';

$config['order_confirmation_message']="Hello %s,your order has been confirmed.\nReference number : %s\nAmount : %s.Please make payment within %s hours.Thank you";

$config['order_cancellation_message']="Hello,your order with order number %s has been cancelled.\nThank you";

$config['order_item_removal_message']="Hello,%s has been removed from your order with id %s.\nThank you";

$config['buyer_order_message']="Hello %s,your order has been received, our agent will contact you for more details.\nOrder number is %s.Please do not make payment before confirmation.Thank you";

$config['order_payment_message']="Hello %s,your order has been paid, our agent will contact you for more delivery details.\nOrder number is %s.Thank you";