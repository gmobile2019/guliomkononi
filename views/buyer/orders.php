<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-12">
	<table class="table table-condensed table-hover table-striped table-bordered">

            <thead>
                <tr>
                    <th style="text-align:center;">S/NO</th>
                    <th style="text-align:center;">Order ID</th>
                    <th style="text-align:center;">Order Amount</th>
                    <th style="text-align:center;">Destination</th>
                    <th style="text-align:center;">Delivery Fee</th>
                    <th style="text-align:center;">Order Date</th>
                    <th style="text-align:center;">Status</th>
                    <th style="text-align:center;">Action</th>
                 </tr>
            </thead>
            <tbody>
                <?php if($data != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($data as $key=>$value){ ?>
                        <tr>
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<?php echo anchor('Buyer/orderdetails/'.$value->orderid,$value->orderid); ?></td>
                            <td>&nbsp;&nbsp;<?php echo number_format($value->productcost,2); ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->deliverydestination; ?></td>
                            <td>&nbsp;&nbsp;<?php echo number_format($value->deliverycost, 2); ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->orderdate; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->orderstatus; ?></td>
                            <td>&nbsp;&nbsp;
                                <?php echo $value->orderstatus=='confirmed'?anchor('Buyer/pay/'.$value->orderid,'<span class="fab fa-amazon-pay fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Pay"></span>'):''; ?>
                         </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>