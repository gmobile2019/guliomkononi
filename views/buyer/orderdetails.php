<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-12">
        <?php echo anchor($this->session->userdata('prev_url_path'),'<span class="fas fa-reply fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?>
    </div>
</div>
<div class="row">
    <div class="col-4"><b>Order ID</b>&nbsp;:&nbsp;<?php echo $ord[0]->orderid; ?> </div>
    <div class="col-4"><b>Order Date</b>&nbsp;:&nbsp;<?php echo $ord[0]->orderdate; ?> </div>
    <div class="col-4"><b>Order Status</b>&nbsp;:&nbsp;<?php echo $ord[0]->orderstatus; ?> </div>
    <div class="col-4"><b>Product Cost</b>&nbsp;:&nbsp;<?php echo number_format($ord[0]->productcost,2)." /="; ?> </div>
    <div class="col-4"><b>Delivery Fee</b>&nbsp;:&nbsp;<?php echo number_format($ord[0]->deliverycost,2)." /="; ?> </div>
    <div class="col-4"><b>Total Cost </b>&nbsp;:&nbsp;<?php echo number_format($ord[0]->deliverycost+$ord[0]->productcost,2)." /="; ?> </div>
    <div class="col-4"><b>Destination</b>&nbsp;:&nbsp;<?php echo $ord[0]->deliverydestination; ?> </div>
    <div class="col-4"><b>Delivery Date</b>&nbsp;:&nbsp;<?php echo $ord[0]->deliverydate; ?> </div>
    <div class="col-4"><b>Driver Name</b>&nbsp;:&nbsp;<?php echo $ord[0]->drivername; ?> </div>
    <div class="col-4"><b>Vehicle No</b>&nbsp;:&nbsp;<?php echo $ord[0]->vehicleno; ?> </div>
    
    <div class="col-12">
	<table class="table table-condensed table-hover table-bordered">

            <tbody>
                <?php if($ord != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($ord as $v){ ?>
                        
                        <tr>
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<img src="<?php echo base_url().'imgs/products/sellingcrops/'.$v->productimage ?>" alt="cropImage" class="imgviewdsplay"/></td>
                            <td>
                                &nbsp;&nbsp;<span><b>Product Name : </b><?php echo $v->productname; ?></span><br/>
                                &nbsp;&nbsp;<span><b>Quantity : </b><?php echo number_format($v->productqty)." ".$v->productunit; ?></span><br/>
                                &nbsp;&nbsp;<span><b>Status : </b><?php echo $v->status; ?></span><br/>
                                &nbsp;&nbsp;<span><b>Cost : </b><?php echo number_format($v->productprice,2)." /="; ?></span><br/>
                            </td>
                        </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="3" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
</div>