<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
       
       $('a.cartLnk').each(function(){
            var id=$(this).attr('id');
            $(this).click(function(){
                
                var productid=id.split('_');
                
                $('div#cartModal_'+productid[1]).modal();
            });
       });
       
       $('input[name=quantity]').each(function(){
           
            var id=$(this).attr('id');
            var productid=id.split('_');

            var qty=parseFloat($(this).val());
            var totalcost=parseFloat($('input#cost_'+productid[1]).val());
          
            if(isNaN(totalcost) || totalcost <= 0 || totalcost === null){
               
               $('input#add_to_cart_'+productid[1]).hide();
            }
           
            $(this).keyup(function(){
                
               var unitprice=$('input#prodprice_'+productid[1]).val();
               qty=$(this).val();
               
               if(!isNaN(qty)){
                   totalcost=parseFloat(qty*unitprice);
               }
               
               if(totalcost <= 0){
               
                    $('input#add_to_cart_'+productid[1]).hide('slow');
                }else{
                    $('input#add_to_cart_'+productid[1]).show('slow');
                }
                
               $('input#cost_'+productid[1]).val(totalcost);
            });
        });
       
        $("form.cart").submit(function(e){
            var id=$(this).attr('id');
            var productid=id.split('_');
           
            $.ajax({
            type:'POST',
            url: '<?php echo site_url('Buyer/add_product_to_cart'); ?>',
            data:$(this).serialize(),
            success: function(data){

                $('div#usrFeedbackMessage').empty();
                $('div#usrFeedbackMessage').html(data);
                $('div#cartModal_'+productid[1]).modal('hide');
            }
            });
             e.preventDefault();
        }); 
      
    });
</script>
<div class="row">
        <?php if($data != null){ 
            
    foreach($data AS $ky=>$vl){ ?>
    <div class="col-12 crpdsply">
                
        <img src="<?php echo base_url().'imgs/products/sellingcrops/'.$vl->productimage ?>" alt="cropImage" class="imgviewdsplay"/>
        <div><b>Name</b>&nbsp;:&nbsp;<?php echo $vl->productname; ?></div>
        <div><b>Category</b>&nbsp;:&nbsp;<?php echo $vl->productcategory; ?></div>
        <div><b>Price </b>&nbsp;:&nbsp;<?php echo number_format($vl->productprice,2).'/= Tsh per '.$vl->productunit; ?></div>
        <div><b>Description</b>&nbsp;:&nbsp;<?php echo $vl->description; ?></div>
        <div style="text-align: left">
            <a href="#" class="fas fa-cart-plus fa-2x cartLnk" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to add product to cart" id="cartLink_<?php echo $vl->id; ?>"></a>
        </div>
        <!--------------product to cart modal--->
        <div class="modal fade cartModal" id="cartModal_<?php echo $vl->id; ?>" tabindex="-1" role="dialog" aria-labelledby="cart_<?php echo $vl->id; ?>" aria-hidden="true" style="font-family: cursive">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="cart_<?php echo $vl->id; ?>" style="text-align: center;">Fill <?php echo $vl->productname;?> Cart details!!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
                <form class="form-horizontal cart" id="cartForm_<?php echo $vl->id; ?>" role="form" method="POST">
                <div class="modal-body">
                  
                    <input type="hidden" name="product" id="product_<?php echo $vl->id; ?>" value="<?php echo $vl->id; ?>" required/>
                    <div class="form-group row">
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                            <img src="<?php echo base_url().'imgs/products/sellingcrops/'.$vl->productimage ?>" alt="cropImage" class="imgviewdsplay"/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="qty_<?php echo $vl->id; ?>" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Quantity</label>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <input type="number" class="form-control" name="quantity" id="qty_<?php echo $vl->id; ?>" placeholder="Quantity" value="" required/>
                        </div>
                        <label for="qty_<?php echo $vl->id; ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $vl->productunit; ?></label>
                    </div>
                    <div class="form-group row">
                        <label for="cost_<?php echo $vl->id; ?>" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Cost</label>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <input type="text" class="form-control" id="cost_<?php echo $vl->id; ?>" placeholder="Cost" readonly/>
                            <input type="hidden" name="cost" id="cost_<?php echo $vl->id; ?>" value="" required/>
                            <input type="hidden" name="productprice" id="prodprice_<?php echo $vl->id; ?>" value="<?php echo $vl->productprice; ?>" required/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-outline-danger" data-dismiss="modal" id="cancel_<?php echo $vl->id; ?>">Cancel</button>
                  <input type="submit" class="btn btn-outline-success" name="add_to_cart" id="add_to_cart_<?php echo $vl->id; ?>" value="Add To Cart"/>
                </div>
            </form>
                
            </div>
          </div>
        </div>
    </div>
    <hr>
    <?php }
        }?>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>