<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
    echo form_open('Administrator/crops/',$attributes); 
?>

<div class="form-group row">
    <label class="sr-only" for="name"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="name" id="name" placeholder="Crop Name" value="<?php echo $name; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="categ"></label>
    <div class="col-12">
        <select name="categ" id="categ" class="form-control" >
            <option value="">All Categories</option>
            <option value="Cereal" <?php echo ('Cereal' == $categ)?'selected="selected"':""; ?>>Cereal</option>
            <option value="Fruit" <?php echo ('Fruit' == $categ)?'selected="selected"':""; ?>>Fruit</option>
            <option value="Vegetable" <?php echo ('Vegetable' == $categ)?'selected="selected"':""; ?>>Vegetable</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="status"></label>
    <div class="col-12">
        <select name="status" id="status" class="form-control" >
            <option value="">All Status</option>
            <option value="Active" <?php echo ('Active' == $status)?'selected="selected"':""; ?>>Active</option>
            <option value="Pending" <?php echo ('Pending' == $status)?'selected="selected"':""; ?>>Pending</option>
            <option value="Suspended" <?php echo ('Suspended' == $status)?'selected="selected"':""; ?>>Suspended</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
    </div>
</div>
                
<?php echo form_close();