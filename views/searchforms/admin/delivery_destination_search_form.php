<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
    echo form_open('Administrator/delivery_destinations/',$attributes); 
?>

<div class="form-group row">
    <label class="sr-only" for="destination"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="destination" id="destination" placeholder="Destination Name" value="<?php echo $destination; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="status"></label>
    <div class="col-12">
        <select name="status" id="status" class="form-control" >
            <option value="">All Status</option>
            <option value="Active" <?php echo ('Active' == $status)?'selected="selected"':""; ?>>Active</option>
            <option value="Suspended" <?php echo ('Suspended' == $status)?'selected="selected"':""; ?>>Suspended</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
    </div>
</div>
                
<?php echo form_close();