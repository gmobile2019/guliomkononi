<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
    echo form_open('Administrator/orders/',$attributes); 
?>

<div class="form-group row">
    <label class="sr-only" for="name"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="orderid" id="orderid" placeholder="Order ID" value="<?php echo $orderid; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="dst"></label>
    <div class="col-12">
        <select name="dst" id="dst" class="form-control" >
            <option value="">All Destinations</option>
            <?php foreach($destinations AS $v){?>
            
            <option value="<?php echo $v->destination; ?>" <?php echo $dst == $v->destination?"selected='selected'":""; ?>><?php echo $v->destination; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="status"></label>
    <div class="col-12">
        <select name="status" id="status" class="form-control" >
            <option value="">All Status</option>
            <option value="pending" <?php echo ('pending' == $status)?'selected="selected"':""; ?>>Pending</option>
            <option value="confirmed" <?php echo ('confirmed' == $status)?'selected="selected"':""; ?>>Confirmed</option>
            <option value="cancelled" <?php echo ('cancelled' == $status)?'selected="selected"':""; ?>>Cancelled</option>
            <option value="paid" <?php echo ('paid' == $status)?'selected="selected"':""; ?>>Paid</option>
            <option value="delivered" <?php echo ('delivered' == $status)?'selected="selected"':""; ?>>Delivered</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="buyer"></label>
    <div class="col-12">
        <select name="buyer" id="buyer" class="form-control" >
            <option value="">All Customers</option>
            <?php foreach($buyers AS $v){?>
            
            <option value="<?php echo $v->id; ?>" <?php echo $buyer == $v->id ?"selected='selected'":""; ?>><?php echo $v->first_name.''.$v->last_name; ?></option>
            <?php } ?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="from"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="from" id="from" placeholder="Start Date" value="<?php echo $from; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="to"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="to" id="to" placeholder="End Date" value="<?php echo $to; ?>" />
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
    </div>
</div>

<?php echo form_close(); ?>