<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
    echo form_open('Administrator/project_activities/',$attributes); 
?>

<div class="form-group row">
    <label class="sr-only" for="farmer"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="projectID" id="projectID" placeholder="Project ID" value="<?php echo $projectID; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="stage"></label>
    <div class="col-12">
        <select name="stage" class="form-control">
            <option value="">All Stages</option>
            <?php foreach($stages AS $key=>$value){ ?>
            <option value="<?php echo $value->id ?>" <?php echo $stage == $value->id?"selected='selected'":"" ?>><?php echo $value->stage ?></option>    
            <?php }?>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="status"></label>
    <div class="col-12">
        <select name="status" class="form-control">
            <option value="">All Status</option>
            <option value="Ongoing" <?php echo $status == "Ongoing"?"selected='selected'":"" ?>>Ongoing</option>
            <option value="Completed" <?php echo $status == "Completed"?"selected='selected'":"" ?>>Completed</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="start"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="end"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
    </div>
</div>
                
<?php echo form_close();