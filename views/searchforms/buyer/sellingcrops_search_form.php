<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<h5 style="text-align: center;font-style: italic;font-family: cursive">Filter Data</h5>
<?php
    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
    echo form_open('Buyer/crops/',$attributes); 
?>

<div class="form-group row">
    <label class="sr-only" for="name"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="name" id="name" placeholder="Crop Name" value="<?php echo $name; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="categ"></label>
    <div class="col-12">
        <select name="categ" id="categ" class="form-control" >
            <option value="">All Categories</option>
            <option value="Cereal" <?php echo ('Cereal' == $categ)?'selected="selected"':""; ?>>Cereal</option>
            <option value="Fruit" <?php echo ('Fruit' == $categ)?'selected="selected"':""; ?>>Fruit</option>
            <option value="Vegetable" <?php echo ('Vegetable' == $categ)?'selected="selected"':""; ?>>Vegetable</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
    </div>
</div>
                
<?php echo form_close();