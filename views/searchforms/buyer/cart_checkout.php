<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">

    $(document).ready(function () {

        $('a#cartCheckout').click(function () {

            $('div#orderModal').modal();
        });
        
        
        var dst;

        dst = parseInt($('select#dlvryType').val());

        if (dst !== null && dst !== '') {

            if (dst === 2) {

                $('input#dlvrydestination').show();
                $('input#press_order').show();
            } else {
                $('input#dlvrydestination').hide();
                $('input#dlvrydestination').val(null);
                
            }
        } else {
            $('input#press_order').hide();
            $('input#dlvrydestination').val(null);
            
        }


        $('select#dlvryType').change(function () {

            dst = parseInt($(this).val());

            if (dst !== null && dst !== '') {

                if (dst === 2) {

                    $('div#dlvrydestination').show('slow');
                    $('input#press_order').show('slow');
                } else {
                    
                    $('div#dlvrydestination').hide('slow');
                    $('input[name=dlvrydestination]').val(null);
                    
                }
            } else {
                
                $('input#press_order').hide('slow');
                $('input[name=dlvrydestination]').val(null);
                
            }

        });
    });
</script>
<h5 style="text-align: center;font-style: italic;font-family: cursive">My Cart</h5>
<div class="row">
    <div class="col-12">
        <table class="table table-condensed table-hover table-striped table-bordered">

            <thead>
                <tr>
                    <th style="text-align:center;">S/NO</th>
                    <th style="text-align:center;">Product</th>
                    <th style="text-align:center;">Quantity</th>
                    <th style="text-align:center;">Cost</th>
                    <th style="text-align:center;">Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if ($data != null) {

                    if ($per_page == null) {
                        $i = 1;
                    } else {
                        $i = $per_page + 1;
                    }
                    foreach ($data as $key => $value) {
                        $total +=$value->productprice;
                        ?>
                        <tr>
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->productname; ?></td>
                            <td>&nbsp;&nbsp;<?php echo number_format($value->quantity) . " " . $value->productunit; ?></td>
                            <td style="text-align: right"><?php echo number_format($value->productprice, 2) . "/="; ?>&nbsp;&nbsp;</td>
                            <td>&nbsp;&nbsp;
                        <?php echo anchor('Buyer/remove_cart_product/' . $value->id, '<span class="fas fa-times-circle fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="remove product"></span>'); ?>
                        </tr>  
    <?php } ?>
                    <tr>
                        <th style="text-align: right" colspan="3">Total&nbsp;&nbsp;</th>
                        <th style="text-align: right"><?php echo number_format($total, 2) . "/="; ?>&nbsp;&nbsp;</th>
                        <th>
                            <a href="#" id="cartCheckout" class="btn btn-outline-success" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to press order">Press Order</a>
                        </th>
                    </tr>  
<?php } else { ?>
                    <tr>
                        <td colspan="5" style="text-align:center"> NO DATA FOUND</td>
                    </tr>  
<?php } ?>
            </tbody>
        </table>
    </div>
    <div class="offset-4 col-8">
<?php echo $links; ?>
    </div>
</div>
<!--------------cart chekout modal--->
<div class="modal fade orderModal" id="orderModal" tabindex="-1" role="dialog" aria-labelledby="orderModal" aria-hidden="true" style="font-family: cursive">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="orderModaltitle" style="text-align: center;">Fill <?php echo $vl->productname; ?> Destination Details!!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="form-horizontal" id="orderForm" role="form" method="POST" action="<?php echo base_url(); ?>Buyer">
                <div class="modal-body">
                    <div class="form-group row">
                        <label for="dlvryType" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Type of Delivery</label>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <select name="dlvryType" id="dlvryType" class="form-control">
                                <option value=""></option>
                                <option value="1">Self Pickup</option>
                                <option value="2">Door Delivery</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row" id="dlvrydestination">
                        <label for="dlvrydestination" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Delivery Destination</label>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                            <input type="text" class="form-control" name="dlvrydestination" placeholder="Delivery Destination"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-danger" data-dismiss="modal" >Cancel</button>
                    <input type="submit" class="btn btn-outline-success" name="press_order" id="press_order" value="Submit"/>
                </div>
            </form>

        </div>
    </div>
</div>