<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
    echo form_open('Agent/project_stages/',$attributes); 
?>

<div class="form-group row">
    <label class="sr-only" for="stage"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="stage" id="stage" placeholder="Stage" value="<?php echo $stage; ?>" />
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
    </div>
</div>
                
<?php echo form_close();