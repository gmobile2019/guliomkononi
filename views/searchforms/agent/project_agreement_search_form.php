<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
    echo form_open('Agent/project_agreements/',$attributes); 
?>

<div class="form-group row">
    <label class="sr-only" for="farmer"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="farmer" id="farmer" placeholder="Farmer's Name" value="<?php echo $farmer; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="product"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="product" id="product" placeholder="Product's Name" value="<?php echo $product; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="status"></label>
    <div class="col-12">
        <select name="status" class="form-control">
            <option value="">All Status</option>
            <option value="Pending" <?php echo $status == "Pending"?"selected='selected'":"" ?>>Pending</option>
            <option value="Closed" <?php echo $status == "Closed"?"selected='selected'":"" ?>>Closed</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="start"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="end"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
    </div>
</div>
                
<?php echo form_close();