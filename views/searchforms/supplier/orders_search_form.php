<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
    echo form_open('Supplier/orders/',$attributes); 
?>

<div class="form-group row">
    <label class="sr-only" for="product"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="product" id="product" placeholder="Product" value="<?php echo $product; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="batch"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="batch" id="batch" placeholder="Product Batch" value="<?php echo $batch; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="status"></label>
    <div class="col-12">
        <select name="status" id="status" class="form-control" >
            <option value="">All Status</option>
            <option value="Reserved" <?php echo ('Reserved' == $status)?'selected="selected"':""; ?>>Reserved</option>
            <option value="Sold" <?php echo ('Sold' == $status)?'selected="selected"':""; ?>>Sold</option>
        </select>
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="from"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="from" id="from" placeholder="Start Date" value="<?php echo $from; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="to"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="to" id="to" placeholder="End Date" value="<?php echo $to; ?>" />
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
    </div>
</div>

<?php echo form_close(); ?>