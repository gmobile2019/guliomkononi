<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
    echo form_open('Manager/users/',$attributes); 
?>

<div class="form-group row">
    <label class="sr-only" for="name"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="name" id="name" placeholder="First or Middle or Last Name" value="<?php echo $name; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="group"></label>
    <div class="col-12">
        <select name="group" id="group" class="form-control" >
            <option value="">All User Groups</option>
            <?php foreach($groups as $key=>$value){ ?>

            <option value="<?php echo $value->id; ?>" <?php echo (trim($value->id) == $group)?'selected="selected"':""; ?>><?php echo $value->name; ?></option>

                <?php } ?>

        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
    </div>
</div>
                
<?php echo form_close();