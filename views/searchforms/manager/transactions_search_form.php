<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
    echo form_open('Manager/transactions/',$attributes); 
?>

<div class="form-group row">
    <label class="sr-only" for="transactionid"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="transactionid" id="transactionid" placeholder="Transaction ID" value="<?php echo $transactionid; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="orderid"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="orderid" id="orderid" placeholder="Order ID" value="<?php echo $orderid; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="from"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="from" id="from" placeholder="Start Date" value="<?php echo $from; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="to"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="to" id="to" placeholder="End Date" value="<?php echo $to; ?>" />
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
    </div>
</div>

<?php echo form_close(); ?>