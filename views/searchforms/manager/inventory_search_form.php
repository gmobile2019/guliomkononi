<?php defined('BASEPATH') OR exit('No direct script access allowed'); 

    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
    echo form_open('Manager/inventory_stock/',$attributes); 
?>

<div class="form-group row">
    <label class="sr-only" for="product"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="product" id="product" placeholder="Product Name" value="<?php echo $product; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="batch"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="batch" id="batch" placeholder="Product Batch" value="<?php echo $batch; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="searchavailability"></label>
    <div class="col-12">
        <input type="text" class="form-control" name="availability" id="searchavailability" placeholder="Available Date" value="<?php echo $availability; ?>" />
    </div>
</div>
<div class="form-group row">
    <label class="sr-only" for="supplier"></label>
    <div class="col-12">
        <select name="supplier" id="categ" class="form-control" >
            <option value="">All Suppliers</option>
            <?php foreach($suppliers AS $val){ ?>
                <option value="<?php echo $val->id; ?>" <?php echo ($val->id == $supplier)?'selected="selected"':""; ?>><?php echo $val->supplier?></option>
            <?php }?>
        </select>
    </div>
</div>
<div class="form-group row">
    <div class="col-12">
        <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
    </div>
</div>
                
<?php echo form_close();