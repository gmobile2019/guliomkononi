<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
      
    });
</script>
<div class="">
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Supplier/send_crop_request/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="cropname" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Crop Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="cropname" id="cropname" placeholder="Crop Name" value="<?php echo $id <> NULL?$crop[0]->cropname:set_value('cropname'); ?>" required/>
                        <?php echo form_error('cropname'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cropcategory" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Crop Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="cropcategory" id="cropcategory" class="form-control" required>
                        <option value=""></option>
                        <option value="Cereal" <?php echo ($id != null && 'Cereal' == trim($crop[0]->cropcategory))?'selected="selected"':set_select('cropcategory','Cereal'); ?>>Cereal</option>
                        <option value="Fruit" <?php echo ($id != null && 'Fruit' == trim($crop[0]->cropcategory))?'selected="selected"':set_select('cropcategory','Fruit'); ?>>Fruit</option>
                        <option value="Vegetable" <?php echo ($id != null && 'Vegetable' == trim($crop[0]->cropcategory))?'selected="selected"':set_select('cropcategory','Vegetable'); ?>>Vegetable</option>
                    </select>
                        <?php echo form_error('cropcategory'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Description</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea  rows="3" class="form-control" name="description" id="description" placeholder="Description"><?php echo $id <> NULL?$crop[0]->cropname:set_value('description'); ?></textarea>
                        <?php echo form_error('description'); ?>
                    </div>
                </div>
                <div class="form-group row supplier">
                    <label for="cropphoto" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Crop Image</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="cropphoto" id="cropphoto" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-6">
                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
