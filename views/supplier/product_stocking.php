<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
      
        var prodtype=$('#prodtype').val();
      
        $('#product').find('option').each(function(){
           
            var id=$(this).attr('class');
            
            if(id === 'prodtype_'+prodtype){
                
                $(this).show();
            }else{
                $(this).hide();
            }
        });
        
       
       $('#prodtype').change(function(){
           
            var prodtype=$(this).val();
        
            $('#product').find('option').each(function(){

                var id=$(this).attr('class');
                
                if(id === 'prodtype_'+prodtype){

                    $(this).show();
                }else{
                    $(this).hide();
                }
            });
       });
       
    });
</script>
<div class="">
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Supplier/product_stocking/',$attributes); 
                ?>
                
                <div class="form-group row">
                    <label for="prodtype" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Product Type&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="prodtype" id="prodtype" class="form-control" required>
                        <option value=""></option>
                        <option value="Crop" <?php echo set_select('prodtype','Crop')?>>Crop</option>
                    </select>
                    <?php echo form_error('prodtype'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="product" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Product&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="product" id="product" class="form-control" required>
                        <option></option>
                        <?php foreach($products as $key=>$value){ ?>
                        
                        <option class="prodtype_<?php echo $value->producttype; ?>" value="<?php echo $value->id; ?>" <?php echo set_select('product',$value->id); ?>><?php echo $value->productname."($value->productunit)"; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('product'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="quantity" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Quantity&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="quantity" id="quantity" placeholder="Quantity" value="<?php echo set_value('quantity'); ?>" required/>
                        <?php echo form_error('quantity'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="availabilityPeriod" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Availability&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="availabilityPeriod" id="availabilityPeriod" class="form-control" required>
                        <option value=""></option>
                        <option value="Now" <?php echo set_select('availabilityPeriod','Now')?>>Now</option>
                        <option value="10" <?php echo set_select('availabilityPeriod','10')?>>After 10 days</option>
                        <option value="20" <?php echo set_select('availabilityPeriod','20')?>>After 20 days</option>
                        <option value="30" <?php echo set_select('availabilityPeriod','30')?>>After 30 days</option>
                        <option value="40" <?php echo set_select('availabilityPeriod','40')?>>After 40 days</option>
                        <option value="50" <?php echo set_select('availabilityPeriod','50')?>>After 50 days</option>
                        <option value="60" <?php echo set_select('availabilityPeriod','60')?>>After 60 days</option>
                    </select>
                    <?php echo form_error('availabilityPeriod'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-6">
                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>