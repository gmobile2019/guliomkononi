<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        
        <link href="<?php echo base_url() ?>styles/css/fontawesome-all.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>styles/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>styles/css/jquery-ui.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>styles/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.min.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
              $('input[name=user_signup]').hide();
              $('input[name=user_signin]').hide();
              $('input[name=sendEmail]').hide();
              $('.carousel').carousel({
                interval: 5000
              });
              
              $('a#signUpMdl').click(function(){
                  $('#signUpModal').modal();
              });
              
              
              $('input[name=emailAddress]').keyup(function(){
                  
                    var email=$(this).val();
                    
                    $.ajax({

                        type:'POST',
                        url:'<?php echo site_url('Welcome/validate_user_email'); ?>',
                        data:{email:email},
                        dataType:"json",
                        success:function(data){
                           
                        if(!data.validity){
                            
                            $('input[name=user_signup]').hide('slow');
                            $('div#emailValidity').empty();
                            $('div#emailValidity').text(data.message);
                        }else{
                            
                            $('input[name=user_signup]').show('slow');
                            $('div#emailValidity').empty(); 
                        }
                    }

                    });
              });
              
              $('a#verifycaptcha_signup').click(function(){
                   
                    verify_captcha($('input#captcha_signup').val(),'signup');
              });
              
              $('a#verifycaptcha_signin').click(function(){
                   
                    verify_captcha($('input#captcha_signin').val(),'signin');
              });
              
              $('a#verifycaptcha_sendemail').click(function(){
                   
                    verify_captcha($('input#captcha_sendemail').val(),'sendemail');
              });
              
              function verify_captcha(captcha,input){
               
                    $.ajax({

                        type:'POST',
                        url:'<?php echo site_url('Welcome/verify_captcha_image'); ?>',
                        data:{captcha:captcha},
                        dataType:"json",
                        success:function(data){
                           
                           if(input === 'signup'){
                               
                               if(!data.status){
                            
                                    $('input[name=user_signup]').hide('slow');
                                    $('input#captcha_signup').val(null);
                                    $('input[name=captcha_signup]').val(null);
                                    $('img#captchaimg_signup').attr('src',data.captcha);
                                    $('div#captchaInValid_signup').empty(); 
                                    $('div#captchaValid_signup').empty();
                                    $('div#captchaInValid_signup').text(data.message); 
                                }else{
                                    
                                    $('a#verifycaptcha_signup').hide('slow'); 
                                    $('input#captcha_signup').attr('readonly',true);
                                    $('input[name=captcha_signup]').val(captcha);
                                    $('input[name=user_signup]').show('slow');
                                    $('div#captchaInValid_signup').empty(); 
                                    $('div#captchaValid_signup').empty(); 
                                    $('div#captchaValid_signup').text(data.message);  
                                }
                           }
                           
                           if(input === 'signin'){
                               
                               if(!data.status){
                            
                                    $('input[name=user_signin]').hide('slow');
                                    $('input#captcha_signin').val(null);
                                    $('input[name=captcha_signin]').val(null);
                                    $('img#captchaimg_signin').attr('src',data.captcha);
                                    $('div#captchaInValid_signin').empty(); 
                                    $('div#captchaValid_signin').empty();
                                    $('div#captchaInValid_signin').text(data.message); 
                                }else{
                                    
                                    $('a#verifycaptcha_signin').hide('slow'); 
                                    $('input#captcha_signin').attr('readonly',true);
                                    $('input[name=captcha_signin]').val(captcha);
                                    $('input[name=user_signin]').show('slow');
                                    $('div#captchaInValid_signin').empty(); 
                                    $('div#captchaValid_signin').empty(); 
                                    $('div#captchaValid_signin').text(data.message);  
                                }
                           }
                           
                           if(input === 'sendemail'){
                               
                               if(!data.status){
                            
                                    $('input[name=sendEmail]').hide('slow');
                                    $('input#captcha_sendemail').val(null);
                                    $('input[name=captcha_sendemail]').val(null);
                                    $('img#captchaimg_sendemail').attr('src',data.captcha);
                                    $('div#captchaInValid_signin').empty(); 
                                    $('div#captchaValid_sendemail').empty();
                                    $('div#captchaInValid_sendemail').text(data.message); 
                                }else{
                                    
                                    $('a#verifycaptcha_sendemail').hide('slow'); 
                                    $('input#captcha_sendemail').attr('readonly',true);
                                    $('input[name=captcha_sendemail]').val(captcha);
                                    $('input[name=sendEmail]').show('slow');
                                    $('div#captchaInValid_sendemail').empty(); 
                                    $('div#captchaValid_sendemail').empty(); 
                                    $('div#captchaValid_sendemail').text(data.message);  
                                }
                           }
                         
                        }

                    });
                    
              }
                    
            });
        </script>
        <style type="text/css">
            
            
        </style>
        <title>Agriculture Center</title>
    </head>
    <body id="pageBody">
      <!-----top banner comes here----->
      <div class="container-fluid" id="banner">
          <img src="<?php echo base_url()?>imgs/GULIOMKONONI.png" class="rounded mx-auto d-block" alt="banner image">
      </div>
      <div class="container-fluid row" id="topMenu">
          <div class="col-12">
              <!----login, signup and top menu links------>
                <nav class="navbar navbar-expand-lg navbar-light bg-success text-dark" id="navigationBar">
                    <a class="navbar-brand" href="#">Quality</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                          <a class="nav-link" href="<?php echo base_url() ?>Welcome/home">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#products">Products</a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#getInTouch">Get In Touch</a>
                        </li>
                      </ul>
                      <form class="form-inline my-2 my-lg-0">
                        <div class="my-2 my-sm-0" id="feedbackMessage"><?php echo $message; ?></div>
                        <a class="btn btn-link my-2 my-sm-0" data-toggle="modal" data-target="#signInModal"><i class="fas fa-sign-in-alt"></i>&nbsp;Sign In</a>
                        <a class="btn btn-link my-2 my-sm-0"  id="signUpMdl">Sign Up</a>
                      </form>
                    </div>
                </nav>
          </div>
           <div class="offset-2 col-8" id="slideShow">
              <!--------------carousel property for latest uploaded crops----------------->
              <div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
                <ol class="carousel-indicators">
                  <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                  <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                </ol>
				
				<center>
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img class="slideShow" src="<?php echo base_url()?>imgs/products/slider/LIVING.png" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                        <h5>   </h5>
                        <p>   </p>
                    </div>
                  </div>
                  <div class="carousel-item">
                    <img class="slideShow" src="<?php echo base_url()?>imgs/products/slider/FTH.png" alt="Second slide">
                    <div class="carousel-caption d-none d-md-block">
                        <h5></h5>
                        <p>           </p>
                    </div>
                  </div>
                  <div class="carousel-item">
				  
                    <img class="slideShow" src="<?php echo base_url()?>imgs/products/slider/LIVING.png" alt="Third slide">
                    <div class="carousel-caption d-none d-md-block">
                        <h5></h5>
                        <p></p>
                    </div>
                  </div>
                </div>
				</center>                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
          </div>
      </div>
      <div class="container-fluid" style="text-align: center;font-family: cursive;font-style: italic;color:darkred">
            <h2>Pr<span class="deco1">od</span>uc<span class="deco1">ts</span></h2>
            <hr/>
        </div>
      <div class="container-fluid row" id="products">
          <div class="col-3" id="filterArea">
              <h4 style="text-align: center;font-style: italic;font-family: cursive">Refine your product Search</h4>
              <!------filter area for products------>
              <?php 
                $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                echo form_open('Welcome/home/',$attributes); 
            ?>

            <div class="form-group row">
                <label class="sr-only" for="name"></label>
                <div class="col-12">
                    <input type="text" class="form-control" name="name" id="name" placeholder="Crop Name" value="<?php echo $name; ?>" />
                </div>
            </div>
            <div class="form-group row">
                <label class="sr-only" for="categ"></label>
                <div class="col-12">
                    <select name="categ" id="categ" class="form-control" >
                        <option value="">All Categories</option>
                        <option value="Cereal" <?php echo ('Cereal' == $categ)?'selected="selected"':""; ?>>Cereal</option>
                        <option value="Fruit" <?php echo ('Fruit' == $categ)?'selected="selected"':""; ?>>Fruit</option>
                        <option value="Vegetable" <?php echo ('Vegetable' == $categ)?'selected="selected"':""; ?>>Vegetable</option>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <button type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here search">Search</button>
                </div>
            </div>

            <?php echo form_close(); ?>
          </div>
          <div class="col-9" id="kilimocrops">
              <!--------------------kilimo products content----->
              <div class="row">
                <?php if($data != null){ 

                foreach($data AS $ky=>$vl){ ?>
                <div class="col-12 crpdsply">

                    <img src="<?php echo base_url().'imgs/products/sellingcrops/'.$vl->productimage ?>" alt="cropImage" class="imgviewdsplay"/>
                    <div><b>Name</b>&nbsp;:&nbsp;<?php echo $vl->productname; ?></div>
                    <div><b>Category</b>&nbsp;:&nbsp;<?php echo $vl->productcategory; ?></div>
                    <div><b>Price</b>&nbsp;:&nbsp;<?php echo $vl->productprice.' per '.$vl->productunit; ?></div>
                    <div><b>Description</b>&nbsp;:&nbsp;<?php echo $vl->description; ?></div>
                </div>
                <?php }
                    }?>
                <div class="offset-4 col-8">
                        <?php echo $links; ?>
                </div>
            </div>
          </div>
      </div>
      <div class="container-fluid row" id="getInTouch">
          <!------------------contact details------------------>
          <div class="col-12" style="text-align: center;color:darkred;font-style: italic;">
              <h2>G<span class="deco1">et</span> In <span class="deco1">To</span>uch</h2>
              <hr/>
          </div>
          <div class="col-5" style="font-size: 120%">
                <p>
                    <span><i class="fas fa-envelope"></i>&nbsp;Email Address</span>&nbsp;:&nbsp;<span>info@kilimo.co.tz</span>
                </p>
                <p>
                    <span><i class="fas fa-phone "></i>&nbsp;Call Us</span>&nbsp;:&nbsp;<span>255...</span>
                </p>
                <p>
                    <span>Our Offices</span>&nbsp;:&nbsp;<span></span>
                </p>
              <p></p>
          </div>
          <div class="offset-1 col-6">
              <h4 style="text-align: center;font-style: italic;font-family: cursive">Drop us an email</h4>
              <?php 
                    echo $feedbackResponse;
                    echo form_open('Welcome/home/',$attributes); 
                ?>
                
                <div class="form-group row">
                    <label for="emailAddress" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Your Email Address</label>
                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                        <input type="email" class="form-control" name="emailAddress" id="emailAddress" placeholder="Email Address" value="<?php echo set_value('emailAddress'); ?>" required/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="emailSubject" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Subject</label>
                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                        <input type="text" class="form-control" name="emailSubject" id="emailSubject" placeholder="Subject" value="<?php echo set_value('emailSubject'); ?>" required/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="emailContent" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Message</label>
                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                        <textarea rows="8" class="form-control" name="emailContent" id="emailContent" placeholder="please share your feedback" required><?php echo set_value('emailContent'); ?></textarea>
                    </div>
                </div>
              <div class="form-group row">
                    <label for="captchaimg_sendemail" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 control-label">
                        <img src="<?php echo base_url()."captcha/$captcha_img_sendemail"; ?>" width="200" height="50" id="captchaimg_sendemail"/>
                    </label>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <input type="text" class="form-control" id="captcha_sendemail" placeholder="Captcha" value="" required/>
                        <input type="hidden" name="captcha_sendemail" required/>
                        <div style="color: darkgreen" id="captchaValid_sendemail"></div>
                        <div style="color: darkred" id="captchaInValid_sendemail"><?php echo form_error('captcha_sendemail'); ?></div>
                    </div>
                    <div class="col-xs-12 col-sm- col-md-1 col-lg-1">
                        <a href="#" class="btn btn-dark" id="verifycaptcha_sendemail">Verify</a>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <input type="submit" class="btn btn-outline-success btn-block" data-toggle="tooltip" data-placement="top" title="please click here to send email" value="Send Email" name="sendEmail"/>
                    </div>
                </div>
        
        <?php echo form_close(); ?>
          </div>
      </div>
        <!-- Sign Up Modal -->
        <div class="modal fade" id="signUpModal" tabindex="-1" role="dialog" aria-labelledby="signUpModalTitle" aria-hidden="true" style="font-family: cursive">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="signUpModalTitle" style="text-align: center;">Sign Up now to start shopping!!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                 <?php 
                    echo form_open('Welcome/home',$attributes); 
                ?>
       
                <div class="form-group row">
                    <label for="firstName" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">First Name</label>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <input type="text" class="form-control" name="firstName" id="firstName" placeholder="First Name" value="<?php echo set_value('firstName'); ?>" required/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="middleName" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Middle Name</label>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <input type="text" class="form-control" name="middleName" id="middleName" placeholder="Middle Name" value="<?php echo set_value('middleName'); ?>"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="surname" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Surname</label>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <input type="text" class="form-control" name="surname" id="surname" placeholder="Surname" value="<?php echo set_value('surname'); ?>" required/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gender" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Gender</label>
                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="male" name="gender" value="Male" <?php echo set_radio('gender', 'Male')?>/>
                            <label class="form-check-label" for="male">Male</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="female" name="gender" value="Female" <?php echo set_radio('gender', 'Female')?>/>
                          <label class="form-check-label" for="female">Female</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="emailAddress" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Email Address</label>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <input type="email" class="form-control" name="emailAddress" id="emailAddress" placeholder="info@kilimo.co.tz" value="<?php echo set_value('emailAddress'); ?>" required/>
                        <div style="color: darkred" id="emailValidity"></div>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mobile" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Mobile Number</label>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="255..." value="<?php echo set_value('mobile'); ?>" required/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="captchaimg_signup" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 control-label">
                        <img src="<?php echo base_url()."captcha/$captcha_img_signup"; ?>" width="200" height="50" id="captchaimg_signup"/>
                    </label>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <input type="text" class="form-control" id="captcha_signup" placeholder="Captcha" value="" required/>
                        <input type="hidden" name="captcha_signup" required/>
                        <div style="color: darkgreen" id="captchaValid_signup"></div>
                        <div style="color: darkred" id="captchaInValid_signup"><?php echo form_error('captcha_signup'); ?></div>
                        
                    </div>
                    <div class="col-xs-12 col-sm- col-md-1 col-lg-1">
                        <a href="#" class="btn btn-dark" id="verifycaptcha_signup">Verify</a>
                    </div>
                </div>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">Close</button>
                <input type="submit" class="btn btn-outline-success" name="user_signup" value="Save changes">
              </div>
              <?php echo form_close(); ?>
            </div>
          </div>
        </div>
        
        
        <!-- Sign In Modal -->
        <div class="modal fade" id="signInModal" tabindex="-1" role="dialog" aria-labelledby="signInModalTitle" aria-hidden="true" style="font-family: cursive">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="signInModalTitle" style="text-align: center;">Sign In now to have access to your account!!</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                 <?php 
                    echo form_open('Welcome/home/',$attributes); 
                ?>
                <div class="form-group row">
                    <label for="email" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Email Address</label>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <input type="text" class="form-control" name="email" id="email" placeholder="info@kilimo.co.tz" value="<?php echo set_value('email'); ?>" required/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="userPassword" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Password</label>
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <input type="password" class="form-control" name="userPassword" id="userPassword" placeholder="Password" value="<?php echo set_value('userPassword'); ?>" required/>
                    </div>
                </div>
                </div>
                <div class="form-group row">
                    <label for="captchaimg_signin" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 control-label">
                        <img src="<?php echo base_url()."captcha/$captcha_img_signin"; ?>" width="200" height="50" id="captchaimg_signin"/>
                    </label>
                    <div class="col-xs-12 col-sm-3 col-md-3 col-lg-3">
                        <input type="text" class="form-control" id="captcha_signin" placeholder="Captcha" value="" required/>
                        <input type="hidden" name="captcha_signin" required/>
                        <div style="color: darkgreen" id="captchaValid_signin"></div>
                        <div style="color: darkred" id="captchaInValid_signin"><?php echo form_error('captcha_signin'); ?></div>
                    </div>
                    <div class="col-xs-12 col-sm- col-md-1 col-lg-1">
                        <a href="#" class="btn btn-dark" id="verifycaptcha_signin">Verify</a>
                    </div>
                </div>
              <div class="modal-footer">
                <input type="submit" class="btn btn-outline-success btn-block" value="Sign In" name="user_signin"/>
              </div>
              <?php echo form_close(); ?>
            </div>
          </div>
        </div>
    </body>
</html>