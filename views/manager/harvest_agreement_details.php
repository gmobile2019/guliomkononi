<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="offset-5 col-4">
    <?php echo anchor('Manager/crop_details','<span class="fas fa-file-pdf fa-2x" style="color:darkred"aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to download pdf file"></span>'); ?>
</div>
<div class="col-12">
    <table class="table table-condensed table-hover table-striped table-bordered">
        <tbody>
            <tr class="table-warning">
                <th style="text-align: left" colspan="2"><?php echo anchor('Manager/harvest_agreements','<span class="fas fa-reply fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?></th>
           </tr>
            <tr class="<?php echo $agreement[0]->status == 'Closed'?'table-success':($agreement[0]->status =='Cancelled'?'table-danger':'table-info'); ?>">
                <th>&nbsp;&nbsp;Project ID</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->projectID; ?></td>
            </tr>
            <tr class="<?php echo $agreement[0]->status == 'Closed'?'table-success':($agreement[0]->status =='Cancelled'?'table-danger':'table-info'); ?>">
                <th>&nbsp;&nbsp;Crop</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->common_name; ?></td>
            </tr>
            <tr class="<?php echo $agreement[0]->status == 'Closed'?'table-success':($agreement[0]->status =='Cancelled'?'table-danger':'table-info'); ?>">
                <th>&nbsp;&nbsp;Market Price per unit</th>
                <td>&nbsp;&nbsp;<?php echo number_format($agreement[0]->market_price, 2); ?> Tsh</td>
            </tr> 
            <tr class="<?php echo $agreement[0]->status == 'Closed'?'table-success':($agreement[0]->status =='Cancelled'?'table-danger':'table-info'); ?>">
                <th>&nbsp;&nbsp;Harvest quantity</th>
                <td>&nbsp;&nbsp;<?php echo number_format($agreement[0]->harvestQuantity).' '.$agreement[0]->measurement_unit; ?></td>
            </tr>  
            <tr class="<?php echo $agreement[0]->status == 'Closed'?'table-success':($agreement[0]->status =='Cancelled'?'table-danger':'table-info'); ?>">
                <th>&nbsp;&nbsp;Suitable market quantity</th>
                <td>&nbsp;&nbsp;<?php echo number_format($agreement[0]->suitableQuantity).' '.$agreement[0]->measurement_unit; ?></td>
            </tr>  
            <tr class="<?php echo $agreement[0]->status == 'Closed'?'table-success':($agreement[0]->status =='Cancelled'?'table-danger':'table-info'); ?>">
                <th>&nbsp;&nbsp;Not Suitable quantity</th>
                <td>&nbsp;&nbsp;<?php echo number_format($agreement[0]->unsuitableQuantity).' '.$agreement[0]->measurement_unit; ?></td>
            </tr>  
            <tr class="<?php echo $agreement[0]->status == 'Closed'?'table-success':($agreement[0]->status =='Cancelled'?'table-danger':'table-info'); ?>">
                <th>&nbsp;&nbsp;Expected return</th>
                <td>&nbsp;&nbsp;<?php echo number_format($agreement[0]->expected_return, 2); ?> Tsh</td>
            </tr>  
            <tr class="<?php echo $agreement[0]->status == 'Closed'?'table-success':($agreement[0]->status =='Cancelled'?'table-danger':'table-info'); ?>">
                <th>&nbsp;&nbsp;Comments</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->ha_comments; ?></td>
            </tr>
            <tr class="<?php echo $agreement[0]->status == 'Closed'?'table-success':($agreement[0]->status =='Cancelled'?'table-danger':'table-info'); ?>">
                <th>&nbsp;&nbsp;Status</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->status; ?></td>
            </tr>
            <?php if($agreement[0]->status == 'Closed'){ ?>
            <tr class="<?php echo $agreement[0]->status == 'Closed'?'table-success':($agreement[0]->status =='Cancelled'?'table-danger':'table-info'); ?>">
                <th>&nbsp;&nbsp;Agreed On</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->agreedon; ?></td>
            </tr>
            <tr class="<?php echo $agreement[0]->status == 'Closed'?'table-success':($agreement[0]->status =='Cancelled'?'table-danger':'table-info'); ?>">
                <th>&nbsp;&nbsp;Agreed By</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->farmername; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    
</div>