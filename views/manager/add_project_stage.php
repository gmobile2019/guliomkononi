<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
            
            
    });
</script>
<div class="">
   
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Manager/add_project_stage/'.$id); 
                ?>
                <div class="form-group row">
                    <div class="col-xs-12 col-4">
                        <?php echo anchor($this->session->userdata('prev_url_path'),'<span class="fas fa-reply fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="stage" class="col-xs-12 col-5 control-label">Stage Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="text" class="form-control" name="stage" id="stage" placeholder="Stage Name" value="<?php echo $id != null?$crop_stage[0]->stage:set_value('stage'); ?>" required/>
                        <?php echo form_error('stage'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="completion" class="col-xs-12 col-5 control-label">Completion (%)&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="text" class="form-control" name="completion" id="compleion" placeholder="Completion" value="<?php echo $id != null?$crop_stage[0]->percentage_completion:set_value('completion'); ?>" required/>
                        <?php echo form_error('completion'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-xs-12 col-5 control-label">Descripton</label>
                    <div class="col-xs-12 col-4">
                        <textarea class="form-control" name="description" rows="4" id="description" placeholder="Description"><?php echo $id != null?$crop_stage[0]->description:set_value('description'); ?></textarea>
                        <?php echo form_error('description'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>