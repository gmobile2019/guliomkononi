<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-12">
        <?php echo anchor($this->session->userdata('prev_url_path'),'<span class="fas fa-reply fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?>
    </div>
    <div class="col-12">
	<table class="table table-condensed table-hover table-striped table-bordered">

            <thead>
                <tr>
                    <th style="text-align:center;">S/NO</th>
                    <th style="text-align:center;">Product</th>
                    <th style="text-align:center;">Batch</th>
                    <th style="text-align:center;">Quantity</th>
                    <th style="text-align:center;">Status</th>
                    <th style="text-align:center;">Available Date</th>
                    <th style="text-align:center;">Supplier</th>
                 </tr>
            </thead>
            <tbody>
                <?php if($data != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($data as $key=>$value){ 
                       
                        ?>
                        <tr class="<?php echo $class; ?>">
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->productname; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->productbatch; ?></td>
                            <td>&nbsp;&nbsp;<?php echo number_format($value->quantity); ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->status; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->availableDate; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->supplier; ?></td>
                        </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>