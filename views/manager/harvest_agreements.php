<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-12" style="padding: 5px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Manager/harvest_agreements',$attributes); 
                ?>
        <div class="form-row align-items-start">
            <div class="col-auto">
                    <label class="sr-only" for="project"></label>
                    <input type="text" class="form-control" name="project" id="project" placeholder="Project ID" value="<?php echo $project; ?>" />
            </div>
            <div class="col-auto">
                    <label class="sr-only" for="crop"></label>
                    <input type="text" class="form-control" name="crop" id="crop" placeholder="Crop's Name" value="<?php echo $crop; ?>" />
            </div>
            <div class="col-auto">
                    <label class="sr-only" for="status"></label>
                    <select name="status" class="form-control">
                        <option value="">All Status</option>
                        <option value="Pending" <?php echo $status == "Pending"?"selected='selected'":"" ?>>Pending</option>
                        <option value="Cancelled" <?php echo $status == "Cancelled"?"selected='selected'":"" ?>>Cancelled</option>
                        <option value="Closed" <?php echo $status == "Closed"?"selected='selected'":"" ?>>Closed</option>
                    </select>
            </div>
            <div class="col-auto">
                    <label class="sr-only" for="start"></label>
                    <input type="text" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
            </div>
            <div class="col-auto">
                    <label class="sr-only" for="end"></label>
                    <input type="text" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
            </div>
            <div class="col-auto">
               <button type="submit" class="btn btn-success">Search</button> 
            </div>
        </div>
                
        
    <?php echo form_close(); ?>
       
    </div>
    <div class="col-12">
	<table class="table table-condensed table-hover table-striped table-bordered">

            <thead>
                <tr>
                    <th style="text-align:right;" colspan="7"><?php echo anchor('Manager/add_harvest_agreement', '<span class="btn-link" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="add new agreement">Add New Harvest Agreement</span>')?></th>
                 </tr>
                <tr>
                    <th style="text-align:center;">S/NO</th>
                    <th style="text-align:center;">Project</th>
                    <th style="text-align:center;">Crop</th>
                    <th style="text-align:center;">Harvest Quantity</th>
                    <th style="text-align:center;">Market Price (Tsh)</th>
                    <th style="text-align:center;">Status</th>
                    <th style="text-align:center;">Action</th>
                 </tr>
            </thead>
            <tbody>
                <?php if($data != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($data as $key=>$value){ ?>
                        <tr class="<?php echo $value->status == 'Closed'?'table-success':($value->status =='Cancelled'?'table-danger':'table-info'); ?>" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $value->description; ?>">
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<?php echo anchor('Manager/harvest_agreement_details/'.$value->id,'<span aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here for more details">'.$value->projectID.'</span>'); ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->common_name; ?></td>
                            <td>&nbsp;&nbsp;<?php echo number_format($value->harvestQuantity).' '.$value->measurement_unit; ?></td>
                            <td>&nbsp;&nbsp;<?php echo number_format($value->market_price,2); ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->status; ?></td>
                            <td> &nbsp;&nbsp;
                                <?php if($value->status <> 'Closed' && $value->status <> 'Cancelled'){ 
                                
                                echo anchor('Manager/add_harvest_agreement/'.$value->id,'<span class="fas fa-edit fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                                &nbsp;&nbsp;
                                <?php echo anchor('Manager/cancel_harvest_agreement/'.$value->id,'<span class="fas fa-times-circle fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Cancel"></span>'); 
                                }
                                ?>
                            </td>
                        </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="7" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>