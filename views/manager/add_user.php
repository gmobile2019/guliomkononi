<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
      var grp=$('select[name=group]').val();
      
        if(grp === "<?php echo $this->config->item('supplier_group_id'); ?>"){
                
            $('.supplier').show('slow');
            $('.farmer').hide('hide');
            $('.driver').hide('hide');

        }else if(grp === "<?php echo $this->config->item('farmer_group_id'); ?>"){

            $('.supplier').hide('slow');
            $('.farmer').show('slow');
            $('.driver').hide('slow');
        }else if(grp === "<?php echo $this->config->item('driver_group_id'); ?>"){

            $('.supplier').hide('slow');
            $('.farmer').hide('slow');
            $('.driver').show('slow');
        }else{

            $('.supplier').hide('slow');
            $('.farmer').hide('slow');
            $('.driver').hide('slow');
        }
      
      $('select[name=group]').change(function(){
          grp=$(this).val();
          
            if(grp === "<?php echo $this->config->item('supplier_group_id'); ?>"){
                
                $('.supplier').show('slow');
                $('.farmer').hide('hide');
                $('.driver').hide('hide');

            }else if(grp === "<?php echo $this->config->item('farmer_group_id'); ?>"){

                $('.supplier').hide('slow');
                $('.farmer').show('slow');
                $('.driver').hide('slow');
            }else if(grp === "<?php echo $this->config->item('driver_group_id'); ?>"){

                $('.supplier').hide('slow');
                $('.farmer').hide('slow');
                $('.driver').show('slow');
            }else{

                $('.supplier').hide('slow');
                $('.farmer').hide('slow');
                $('.driver').hide('slow');
            }
      });
      
      var reg=$('#center_region').val();
      
        $('#center_district').find('option').each(function(){
           
            var id=$(this).attr('class');
            
            if(id === 'rg_'+reg){
                
                $(this).show();
            }else{
                $(this).hide();
            }
        });
        
       
       $('#center_region').change(function(){
           
            var reg=$(this).val();
        
            $('#center_district').find('option').each(function(){

                var id=$(this).attr('class');

                if(id === 'rg_'+reg){

                    $(this).show();
                }else{
                    $(this).hide();
                }
            });
       });
       
       var reg=$('#farmer_region').val();
      
        $('#farmer_district').find('option').each(function(){
           
            var id=$(this).attr('class');
            
            if(id === 'rg_'+reg){
                
                $(this).show();
            }else{
                $(this).hide();
            }
        });
        
       
       $('#farmer_region').change(function(){
           
            var reg=$(this).val();
        
            $('#farmer_district').find('option').each(function(){

                var id=$(this).attr('class');

                if(id === 'rg_'+reg){

                    $(this).show();
                }else{
                    $(this).hide();
                }
            });
       });
       
       var reg=$('#parking_region').val();
      
        $('#parking_district').find('option').each(function(){
           
            var id=$(this).attr('class');
            
            if(id === 'rg_'+reg){
                
                $(this).show();
            }else{
                $(this).hide();
            }
        });
        
       
       $('#parking_region').change(function(){
           
            var reg=$(this).val();
        
            $('#parking_district').find('option').each(function(){

                var id=$(this).attr('class');

                if(id === 'rg_'+reg){

                    $(this).show();
                }else{
                    $(this).hide();
                }
            });
       });
       
    });
</script>
<div class="">
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Manager/add_user/'.$id,$attributes); 
                ?>
       
                <div class="form-group row">
                    <label for="first_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">First Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="<?php echo $id != null?$member[0]->first_name:set_value('first_name'); ?>" required/>
                        <input type="hidden" name="memberID" value="<?php echo $id <> NULL?$member[0]->memberid:NULL; ?>"/>
                        <?php echo form_error('first_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="middle_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Middle Name</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="middle_name" id="middle_name" placeholder="Middle Name" value="<?php echo $id != null?$member[0]->middle_name:set_value('middle_name'); ?>"/>
                        
                            <?php echo form_error('middle_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="last_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Last Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="<?php echo $id != null?$member[0]->last_name:set_value('last_name'); ?>" required/>
                        <?php echo form_error('last_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Email Address&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="email" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $id != null?$member[0]->email:set_value('email'); ?>" required/>
                        <?php echo form_error('email'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="mobile" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Mobile&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile" value="<?php echo $id != null?$member[0]->msisdn:set_value('mobile'); ?>" required/>
                        <?php echo form_error('mobile'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="gender" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Gender&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="male" name="gender" value="Male" <?php echo $id <> NULL && $member[0]->gender == 'Male'?"checked='checked'":set_radio('gender', 'Male')?>/>
                            <label class="form-check-label" for="male">Male</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" id="female" name="gender" value="Female" <?php echo $id <> NULL && $member[0]->gender == 'Female'?"checked='checked'":set_radio('gender', 'Female')?>/>
                          <label class="form-check-label" for="female">Female</label>
                        </div>
                        <?php echo form_error('gender'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="group" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">User Group&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="group" id="group" class="form-control" >
                        <option></option>
                        <?php foreach($groups as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($member[0]->group_id))?'selected="selected"':set_select('group',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('group'); ?>
                    </div>
                </div>
                <div class="form-group row supplier">
                    <label for="supplierphoto" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Supplier Photo&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="supplierphoto" id="supplierphoto" value=""/>
                        <?php echo form_error('supplierphoto'); ?>
                    </div>
                </div>
                <h5 class="supplier" style="text-align: center;font-style: italic">Center Details</h5>
                <hr class="supplier"/>
                <div class="form-group row supplier">
                    <label for="center_region" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Center Region&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="center_region" id="center_region" class="form-control">
                        <option></option>
                        <?php foreach($regions as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($member[0]->center_residence_region))?'selected="selected"':set_select('center_region',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('center_region'); ?>
                    </div>
                </div>
                <div class="form-group row supplier">
                    <label for="center_district" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Center District&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="center_district" id="center_district" class="form-control">
                        <option></option>
                        <?php foreach($districts as $key=>$value){ ?>
                        
                        <option class="rg_<?php echo $value->parent_region; ?>" value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($member[0]->center_residence_district))?'selected="selected"':set_select('center_district',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('center_district'); ?>
                    </div>
                </div>
                <div class="form-group row supplier">
                    <label for="center_ward" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Center Ward</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3" >
                        <input type="text" class="form-control" name="center_ward" id="center_ward" placeholder="Ward" value="<?php echo $id != null?$member[0]->center_residence_ward:set_value('center_ward'); ?>"/>
                        <?php echo form_error('center_ward'); ?>
                    </div>
                </div>
                <div class="form-group row supplier">
                    <label for="center_street" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Center Street</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="center_street" id="center_street" placeholder="Street" value="<?php echo $id != null?$member[0]->center_residence_street:set_value('center_street'); ?>"/>
                        <?php echo form_error('center_street'); ?>
                    </div>
                </div>
                <div class="form-group row farmer">
                    <label for="tribe" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Tribe&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="tribe" id="tribe" class="form-control">
                        <option></option>
                        <?php foreach($tribes as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($member[0]->tribe))?'selected="selected"':set_select('tribe',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('tribe'); ?>
                    </div>
                </div>
                <div class="form-group row farmer">
                    <label for="farmerphoto" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Farmer Photo&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="farmerphoto" id="farmerphoto" />
                    </div>
                </div>
                <div class="form-group row driver">
                    <label for="driverphoto" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Driver Photo&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="driverphoto" id="driverphoto" />
                    </div>
                </div>
                <div class="form-group row driver">
                    <label for="driverlicense" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Driver License</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="driverlicense" id="driverlicense" />
                    </div>
                </div>
                <div class="form-group row driver">
                    <label for="vehicleno" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Vehicle No&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="vehicleno" id="vehicleno" placeholder="Vehicle No" value="<?php echo $id != null?$member[0]->vehicleno:set_value('vehicleno'); ?>"/>
                        <?php echo form_error('vehicleno'); ?>
                    </div>
                </div>
                <h5 class="farmer" style="text-align: center;font-style: italic">Residence Details</h5>
                <hr class="farmer"/>
                <div class="form-group row farmer">
                    <label for="farmer_region" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Residence Region&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="farmer_region" id="farmer_region" class="form-control">
                        <option></option>
                        <?php foreach($regions as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($member[0]->center_residence_region))?'selected="selected"':set_select('farmer_region',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('farmer_region'); ?>
                    </div>
                </div>
                <div class="form-group row farmer">
                    <label for="farmer_district" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Residence District&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="farmer_district" id="farmer_district" class="form-control">
                        <option></option>
                        <?php foreach($districts as $key=>$value){ ?>
                        
                        <option class="rg_<?php echo $value->parent_region; ?>" value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($member[0]->center_residence_district))?'selected="selected"':set_select('farmer_district',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('farmer_district'); ?>
                    </div>
                </div>
                <div class="form-group row farmer">
                    <label for="farmer_ward" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Residence Ward</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="farmer_ward" id="farmer_ward" placeholder="Ward" value="<?php echo $id != null?$member[0]->center_residence_ward:set_value('farmer_ward'); ?>"/>
                        <?php echo form_error('farmer_ward'); ?>
                    </div>
                </div>
                <div class="form-group row farmer">
                    <label for="farmer_street" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Residence Street</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="farmer_street" id="farmer_street" placeholder="Street" value="<?php echo $id != null?$member[0]->center_residence_street:set_value('farmer_street'); ?>"/>
                        <?php echo form_error('farmer_street'); ?>
                    </div>
                </div>
                <h5 class="driver" style="text-align: center;font-style: italic">Parking Details</h5>
                <hr class="driver"/>
                <div class="form-group row driver">
                    <label for="parking_region" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Parking Region&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="parking_region" id="parking_region" class="form-control">
                        <option></option>
                        <?php foreach($regions as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($member[0]->center_residence_region))?'selected="selected"':set_select('parking_region',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('center_region'); ?>
                    </div>
                </div>
                <div class="form-group row driver">
                    <label for="parking_district" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Parking District&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="parking_district" id="parking_district" class="form-control">
                        <option></option>
                        <?php foreach($districts as $key=>$value){ ?>
                        
                        <option class="rg_<?php echo $value->parent_region; ?>" value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($member[0]->center_residence_district))?'selected="selected"':set_select('parking_district',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('parking_district'); ?>
                    </div>
                </div>
                <div class="form-group row driver">
                    <label for="parking_ward" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Parking Ward</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3" >
                        <input type="text" class="form-control" name="parking_ward" id="parking_ward" placeholder="Ward" value="<?php echo $id != null?$member[0]->center_residence_ward:set_value('parking_ward'); ?>"/>
                        <?php echo form_error('parking_ward'); ?>
                    </div>
                </div>
                <div class="form-group row driver">
                    <label for="parking_street" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Parking Street</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="parking_street" id="parking_street" placeholder="Street" value="<?php echo $id != null?$member[0]->center_residence_street:set_value('parking_street'); ?>"/>
                        <?php echo form_error('parking_street'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-6">
                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>