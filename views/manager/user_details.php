<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="col-12">
    <table class="table table-condensed table-hover table-striped table-bordered">
        <tbody>
            <tr class="table-warning">
                <th style="text-align: left"><?php echo anchor('Manager/users','<span class="fas fa-reply fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?></th>
                <th style="text-align: right"><?php echo anchor('Manager/add_user/'.$member[0]->id,'<span class="fas fa-edit fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to edit details"></span>'); ?></th>
            </tr>
            <tr class="table-success">
                <th style="text-align: center" colspan="2">Basic Details</th>
            </tr>
            <?php if($member[0]->group_id == $this->config->item('supplier_group_id') || $member[0]->group_id == $this->config->item('farmer_group_id') || $member[0]->group_id == $this->config->item('driver_group_id')){ ?>
            <tr>
                <th>&nbsp;&nbsp;Photo</th>
                <td>&nbsp;&nbsp;<img src="<?php echo base_url().'imgs/user/'.$member[0]->userphoto; ?>" alt="Photo Image" style="height: 60px;width:60px"/></td>
            </tr>
            <?php } ?>
            <tr>
                <th>&nbsp;&nbsp;Membership ID</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->memberid; ?></td>
            </tr>
            <tr>
                <th>&nbsp;&nbsp;First Name</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->first_name; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Middle Name</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->middle_name; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Last Name</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->last_name; ?></td>
            </tr>
            <tr>
                <th>&nbsp;&nbsp;Gender</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->gender; ?></td>
            </tr>
            <tr>
                <th>&nbsp;&nbsp;Tribe</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->tribe; ?></td>
            </tr>
            <?php if($member[0]->group_id == $this->config->item('driver_group_id')){ ?>
            <tr>
                <th>&nbsp;&nbsp;Vehicle No</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->vehicleno; ?></td>
            </tr>
            <?php } ?>
            <tr class="table-success">
                <th style="text-align: center" colspan="2">Contact Details</th>
            </tr>
            <tr>
                <th>&nbsp;&nbsp;Email Address</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->email; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Mobile No</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->msisdn; ?></td>
            </tr>
            <tr>
                <th>&nbsp;&nbsp;Postal Address</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->pst_address; ?></td>
            </tr>
            <?php if($member[0]->group_id == $this->config->item('farmer_group_id')){ ?>
            <tr class="table-success">
                <th style="text-align: center" colspan="2">Residence Details</th>
            </tr>
            <tr>
                <th>&nbsp;&nbsp;Residence Region</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->region; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Residence District</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->district; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Residence Ward</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->center_residence_ward; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Residence Street</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->center_residence_street; ?></td>
            </tr>
            <?php } 
            
            if($member[0]->group_id == $this->config->item('driver_group_id')){ ?>
            <tr class="table-success">
                <th style="text-align: center" colspan="2">Parking Details</th>
            </tr>
            <tr>
                <th>&nbsp;&nbsp;Parking Region</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->region; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Parking District</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->district; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Parking Ward</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->center_residence_ward; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Parking Street</th>
                <td>&nbsp;&nbsp;<?php echo $member[0]->center_residence_street; ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
</div>

