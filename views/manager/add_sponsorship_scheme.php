<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        
    });
</script>
<div class="">
   
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Manager/add_sponsorship_scheme/'.$id); 
                ?>
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-8 col-md-4 col-lg-4">
                        <?php echo anchor('Manager/sponsorship_schemes','<span class="fas fa-reply fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="scheme_name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Scheme Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-5 col-lg-5">
                        <input type="text" class="form-control" name="scheme_name" id="scheme_name" placeholder="Scheme Name" value="<?php echo $id != null?$scheme[0]->scheme_name:set_value('scheme_name'); ?>" required/>
                        <?php echo form_error('scheme_name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Scheme description&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-5 col-lg-5">
                        <textarea class="form-control" name="description" id="description" rows="6" placeholder="Description" required><?php echo $id != null?$scheme[0]->description:set_value('description'); ?></textarea>
                        <?php echo form_error('description'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="offset-2 col-10">
                        <button type="submit" class="btn btn-success">Submit</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>