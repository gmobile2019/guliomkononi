<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="offset-9 col-3">
        <?php echo anchor('Manager/add_project_scheme','<span class="btn btn-outline-primary">Click here to add new scheme</span>'); ?>
    </div>
    <div class="col-12">
	<table class="table table-condensed table-hover table-striped table-bordered">

            <thead>
                <tr>
                    <th style="text-align:center;">S/NO</th>
                    <th style="text-align:center;">Name</th>
                    <th style="text-align:center;">Status</th>
                    <th style="text-align:center;">Action</th>
                 </tr>
            </thead>
            <tbody>
                <?php if($data != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($data as $key=>$value){ ?>
                        <tr class="<?php echo $value->status == 'Suspended'?'table-danger':''; ?>" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $value->description; ?>">
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->scheme_name; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->status; ?></td>
                            <?php 

                            $active_status=$value->status == 'Active'?'<span class="fas fa-minus-circle fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Suspend"></span>':'<span class="fas fa-plus-circle fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 

                            ?>
                            <td>&nbsp;&nbsp;
                                <?php echo anchor('Manager/add_project_scheme/'.$value->id,'<span class="fas fa-edit fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                                &nbsp;&nbsp;
                                <?php echo anchor('Manager/activate_deactivate_project_scheme/'.$value->id.'/'.$value->status,$active_status); ?></td>
                        </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="4" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>