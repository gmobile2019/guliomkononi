<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="">
   
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Manager/add_delivery_destination/'.$id); 
                ?>
                <div class="form-group row">
                    <div class="col-xs-12 col-4">
                        <?php echo anchor($this->session->userdata('prev_url_path'),'<span class="fas fa-reply fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="destination" class="col-xs-12 col-5 control-label">Destination Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="text" class="form-control" name="destination" id="destination" placeholder="Destination Name" value="<?php echo $id != null?$dlr_dst[0]->destination:set_value('destination'); ?>" required/>
                        <?php echo form_error('destination'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cost" class="col-xs-12 col-5 control-label">Destination Cost&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="number" class="form-control" name="cost" id="cost" placeholder="Destination Cost" value="<?php echo $id != null?$dlr_dst[0]->cost:set_value('cost'); ?>" required/>
                        <?php echo form_error('cost'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>