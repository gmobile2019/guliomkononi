<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
            
            var farm_region=$('select[name=farm_region]').val();
            
            $('select[name=farm_district]').find('option').each(function(){
                
                var css_class=$(this).attr('class');
                
                if('rg_'+farm_region === css_class){
                    $(this).show();
                }else{
                    $(this).hide();
                }
            });
            
            $('select[name=farm_region]').change(function(){
                
                farm_region=$(this).val();
                $('select[name=farm_district]').val(null);
                
                $('select[name=farm_district]').find('option').each(function(){
                    
                    var css_class=$(this).attr('class');

                    if('rg_'+farm_region === css_class){
                        $(this).show();
                    }else{
                        $(this).hide();
                    }
                });
            });
    });
</script>
<div class="">
   
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Manager/add_project_agreement/'.$id); 
                ?>
                <div class="form-group row">
                    <div class="col-xs-12 col-4">
                        <?php echo anchor($this->session->userdata('prev_url_path'),'<span class="fas fa-reply fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="farmer" class="col-xs-12 col-5 control-label">Farmer&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                    <select name="farmer" id="farmer" class="form-control" required>
                        <option></option>
                        <?php foreach($farmers as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($agreement[0]->farmer))?'selected="selected"':set_select('farmer',$value->id); ?>><?php echo $value->farmer; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('farmer'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="product" class="col-xs-12 col-5 control-label">Candidate Crop&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                    <select name="product" id="product" class="form-control" required>
                        <option></option>
                        <?php foreach($products as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($agreement[0]->productid))?'selected="selected"':set_select('product',$value->id); ?>><?php echo $value->productname; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('product'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="farming_scheme" class="col-xs-12 col-5 control-label">Farming Scheme&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                    <select name="farming_scheme" id="farming_scheme" class="form-control" required>
                        <option></option>
                        <?php foreach($farming_schemes as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($agreement[0]->f_scheme))?'selected="selected"':set_select('farming_scheme',$value->id); ?>><?php echo $value->scheme_name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('farming_scheme'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sponsorship_scheme" class="col-xs-12 col-5 control-label">Sponsorship Scheme&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                    <select name="sponsorship_scheme" id="sponsorship_scheme" class="form-control" required>
                        <option></option>
                        <?php foreach($sp_schemes as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($agreement[0]->s_scheme))?'selected="selected"':set_select('sponsorship_scheme',$value->id); ?>><?php echo $value->scheme_name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('sponsorship_scheme'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="org_investment_amount" class="col-xs-12 col-5 control-label">Estimated Organization Investment Amount (Tzs) &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="number" class="form-control" name="org_investment_amount" id="org_investment_amount" placeholder="Organization Investment" value="<?php echo $id != null?$agreement[0]->org_investment_amount:set_value('org_investment_amount'); ?>" required/>
                        <?php echo form_error('org_investment_amount'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="pvt_investment_amount" class="col-xs-12 col-5 control-label">Estimated Farmer Investment Amount (Tzs) &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="number" class="form-control" name="pvt_investment_amount" id="market_price" placeholder="Farmer Investment Amount" value="<?php echo $id != null?$agreement[0]->pvt_investment_amount:set_value('pvt_investment_amount'); ?>" required/>
                        <?php echo form_error('pvt_investment_amount'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="market_price" class="col-xs-12 col-5 control-label">Market Price per unit (Tzs) &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="number" class="form-control" name="market_price" id="market_price" placeholder="Market Price" value="<?php echo $id != null?$agreement[0]->market_price:set_value('market_price'); ?>" required/>
                        <?php echo form_error('market_price'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="org_profit_share" class="col-xs-12 col-5 control-label">Organization Profit Share (%) &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="number" class="form-control" name="org_profit_share" id="org_profit_share" placeholder="Organization Profit Share" value="<?php echo $id != null?$agreement[0]->org_profit_share:set_value('org_profit_share'); ?>" required/>
                        <?php echo form_error('org_profit_share'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="farmer_profit_share" class="col-xs-12 col-5 control-label">Farmer Profit Share (%) &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="number" class="form-control" name="farmer_profit_share" id="farmer_profit_share" placeholder="Farmer Profit Share" value="<?php echo $id != null?$agreement[0]->farmer_profit_share:set_value('farmer_profit_share'); ?>" required/>
                        <?php echo form_error('farmer_profit_share'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="farm_size" class="col-xs-12 col-5 control-label">Farm size (acres) &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="number" class="form-control" name="farm_size" id="farm_size" placeholder="Farm Size" value="<?php echo $id != null?$agreement[0]->farm_size:set_value('farm_size'); ?>" required/>
                        <?php echo form_error('farm_size'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="farm_region" class="col-xs-12 col-5 control-label">Farm location Region&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <select name="farm_region" id="farm_region" class="form-control" required>
                        <option></option>
                        <?php foreach($regions as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($agreement[0]->farm_region))?'selected="selected"':set_select('farm_region',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('farm_region'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="farm_district" class="col-xs-12 col-5 control-label">Farm location District&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                    <select name="farm_district" id="farm_district" class="form-control" required>
                        <option></option>
                        <?php foreach($districts as $key=>$value){ ?>
                        
                        <option class="rg_<?php echo $value->parent_region; ?>" value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($agreement[0]->farm_district))?'selected="selected"':set_select('farm_district',$value->id); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                        <?php echo form_error('farm_district'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="farm_ward" class="col-xs-12 col-5 control-label">Farm location Ward &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="text" class="form-control" name="farm_ward" id="farm_ward" placeholder="Farm Ward" value="<?php echo $id != null?$agreement[0]->farm_ward:set_value('farm_ward'); ?>" required/>
                        <?php echo form_error('farm_ward'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="farm_street" class="col-xs-12 col-5 control-label">Farm location Street &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="text" class="form-control" name="farm_street" id="farm_street" placeholder="Farm Street" value="<?php echo $id != null?$agreement[0]->farm_street:set_value('farm_street'); ?>" required/>
                        <?php echo form_error('farm_street'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="farm_longitude_gps" class="col-xs-12 col-5 control-label">Farm location Longitude GPS Coordinates&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="text" class="form-control" name="farm_longitude_gps" id="farm_street" placeholder="Longitude Coordinates" value="<?php echo $id != null?$agreement[0]->farm_longitude_gps:set_value('farm_longitude_gps'); ?>" required/>
                        <?php echo form_error('farm_longitude_gps'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="farm_latitude_gps" class="col-xs-12 col-5 control-label">Farm location Latitude GPS Coordinates &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="text" class="form-control" name="farm_latitude_gps" id="farm_latitude_gps" placeholder="Latitude Coordinates" value="<?php echo $id != null?$agreement[0]->farm_latitude_gps:set_value('farm_latitude_gps'); ?>" required/>
                        <?php echo form_error('farm_latitude_gps'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="comments" class="col-xs-12 col-5 control-label">Comments</label>
                    <div class="col-xs-12 col-4">
                        <textarea class="form-control" name="comments" rows="4" id="comments" placeholder="Comments"><?php echo $id != null?$agreement[0]->pa_comments:set_value('comments'); ?></textarea>
                        <?php echo form_error('comments'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>