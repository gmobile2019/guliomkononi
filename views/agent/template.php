<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<html>
    <head>
        <link href="<?php echo base_url() ?>styles/css/fontawesome-all.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>styles/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>styles/css/jquery-ui.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>styles/css/custom.css" rel="stylesheet">
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.min.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.min.js" ></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function() {
              
              
                $('div').find('ul').find('li.nav-item').removeClass('activeLMenu');
               $('div.dropdown-menu').find('a').removeClass('activeLink');
               
                var activeMenu=$('input#activeMenu').val();
             
               if(activeMenu !== ""){
                   
                  $('div').find('ul').find('a#'+activeMenu).addClass('activeLMenu');
                }
               
               var activeLink=$('input#activeLink').val();
             
               if(activeLink !== ""){
                   $('div.dropdown-menu').find('a#'+activeLink).addClass('activeLink');
               }
               
               $("#from").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
                });
            
                $("#to").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    dateFormat: 'yy-mm-dd'
                });
            });
        </script>
        <style type="text/css">
            
            
        </style>
        <title>Agriculture Center</title>
    </head>
    <body id="pageBody">
      <!-----top banner comes here----->
      <div class="container-fluid" id="banner">
          <img src="<?php echo base_url()?>imgs/kilimoone2.gif" class="rounded mx-auto d-block" alt="banner image">
      </div>
      <div class="container-fluid row" id="topMenu">
           <input type="hidden" value="<?php echo $activeMenu; ?>" id="activeMenu"/>
            <input type="hidden" value="<?php echo $activeLink; ?>" id="activeLink"/>
          <div class="col-12">
              <!----login, signup and top menu links------>
                <nav class="navbar navbar-expand-lg navbar-light bg-success text-dark" id="userNavigationBar">
                    <a class="navbar-brand" href="#">Quality</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                      <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                          <a class="nav-link" id="homemenu" href="<?php echo base_url() ?>Agent">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="productsmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Products
                            </a>
                            <div class="dropdown-menu" aria-labelledby="productsmenu">
                              <a id="sellingcropslnk" class="dropdown-item" href="<?php echo base_url()?>Agent/crops">Crops</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="servicesmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Services
                            </a>
                            <div class="dropdown-menu" aria-labelledby="servicesmenu">
                              <a id="dlrdstlnk" class="dropdown-item" href="<?php echo base_url()?>Agent/delivery_destinations">Delivery Destinations</a>
                            </div>
                        </li>
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="businessmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              Business
                            </a>
                            <div class="dropdown-menu" aria-labelledby="businessmenu">
                              <a id="ordlnk" class="dropdown-item" href="<?php echo base_url()?>Agent/orders">Orders</a>
                              <a id="txnlnk" class="dropdown-item" href="<?php echo base_url()?>Agent/transactions">Transactions</a>
                              <a id="inventorystocklnk" class="dropdown-item" href="<?php echo base_url()?>Agent/inventory_stock">Products Inventory</a>
                            </div>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="#getInTouch">Contact Us</a>
                        </li>
                      </ul>
                      <form class="form-inline my-2 my-lg-0">
                        <a class="btn btn-link my-2 my-sm-0" href="<?php echo base_url() ?>Agent/profile" style="color: black"><i class="fas fa-user" ></i>&nbsp;User Profile</a>
                        <a class="btn btn-link my-2 my-sm-0" href="<?php echo base_url() ?>Welcome/logout" style="color: black"><i class="fas fa-sign-out-alt"></i>&nbsp;Sign Out</a>
                      </form>
                    </div>
                </nav>
          </div>
      </div>
      <div class="container-fluid row" id="contentArea">
          <div class="col-6" id="pageTitle">
              <?php echo $title; ?>
          </div>
          <div class="col-6" id="usrFeedbackMessage"><?php echo $message; ?></div>
          <div class="col-3">
              <?php if($searchform <> NULL){ ?>
              
              <h5 style="text-align: center;font-style: italic;font-family: cursive">Filter Data</h5>
              <!------side bar or search area------>
              <?php $this->load->view($searchform);  } ?>
          </div>
          <div class="col-9">
              <!--------------------page contents----->
              <?php $this->load->view($content); ?>
          </div>
      </div>
    </body>
</html>
