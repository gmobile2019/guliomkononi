<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
        <?php if($data != null){ 
            
    foreach($data AS $ky=>$vl){ ?>
    <div class="col-12 crpdsply">
                
        <img src="<?php echo base_url().'imgs/products/sellingcrops/'.$vl->productimage ?>" alt="cropImage" class="imgviewdsplay"/>
        <div><b>Name</b>&nbsp;:&nbsp;<?php echo $vl->productname; ?></div>
        <div><b>Category</b>&nbsp;:&nbsp;<?php echo $vl->productcategory; ?></div>
        <div><b>Price </b>&nbsp;:&nbsp;<?php echo number_format($vl->productprice,2).'/= Tsh per '.$vl->productunit; ?></div>
        <div><b>Description</b>&nbsp;:&nbsp;<?php echo $vl->description; ?></div>
        <div><b>Status</b>&nbsp;:&nbsp;<?php echo $vl->status; ?></div>
    </div>
    <?php }
        }?>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>