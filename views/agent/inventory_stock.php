<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="offset-2 col-4">
        <?php echo anchor("Agent/inventory_stock/product_".$product."_batch_".$batch."_av_".$availability."_sp_".$supplier."_docType_1/",'<span id="pdf" class="fa fa-file-pdf fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to download pdf document"></span>'); ?>
    </div>
    <div class="col-12">
	<table class="table table-condensed table-hover table-striped table-bordered">

            <thead>
                <tr>
                    <th style="text-align:center;">S/NO</th>
                    <th style="text-align:center;">Product Name</th>
                    <th style="text-align:center;">Product Batch</th>
                    <th style="text-align:center;">Quantity</th>
                    <th style="text-align:center;">Available Date</th>
                 </tr>
            </thead>
            <tbody>
                <?php if($data != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($data as $key=>$value){ 
                        $prod=$this->Administration_model->products($value->productid,NULL,NULL,NULL,NULL);
                        $inv_dtl=$this->Inventory_model->inventory_details($value->productid,NULL,$value->productbatch);
                        ?>
                        <tr>
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<?php echo anchor('Agent/inventory_details/'.$value->productid.'/'.$value->productbatch,$value->productname); ?></td>
                            <td>&nbsp;&nbsp;<?php echo anchor('Agent/inventory_details/'.$value->productid.'/'.$value->productbatch,$value->productbatch); ?></td>
                            <td>&nbsp;&nbsp;<?php echo number_format($value->quantity).' '.$prod[0]->productunit; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $inv_dtl[0]->availableDate; ?></td>
                        </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="5" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>