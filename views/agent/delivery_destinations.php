<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="col-12">
	<table class="table table-condensed table-hover table-striped table-bordered">

            <thead>
                <tr>
                    <th style="text-align:center;">S/NO</th>
                    <th style="text-align:center;">Destination</th>
                    <th style="text-align:center;">Cost</th>
                    <th style="text-align:center;">Status</th>
                 </tr>
            </thead>
            <tbody>
                <?php if($data != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($data as $key=>$value){ ?>
                        <tr>
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->destination; ?></td>
                            <td>&nbsp;&nbsp;<?php echo number_format($value->cost,2)."/="; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->status; ?></td>
                        </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="5" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>