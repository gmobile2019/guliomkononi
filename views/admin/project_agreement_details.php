<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
      var map;
      function initMap() {
        
        var myCenter = new google.maps.LatLng(<?php echo $agreement[0]->farm_latitude_gps; ?>,<?php echo $agreement[0]->farm_longitude_gps; ?>);
        var mapCanvas = document.getElementById("map");
        var mapOptions = {
                              center: myCenter,
                              zoom: 15, 
                              mapTypeId: google.maps.MapTypeId.TERRAIN,

                          };
        var map = new google.maps.Map(mapCanvas, mapOptions);
        var marker = new google.maps.Marker({
                                      position:myCenter,
                                      //animation:google.maps.Animation.BOUNCE,
                                  });
        marker.setMap(map);

        var infowindow = new google.maps.InfoWindow({
        content:"<b>Farmer</b>&nbsp;:&nbsp;<?php echo $agreement[0]->fullname; ?><br/>\n\
                <b>Size</b>&nbsp;:&nbsp;<?php echo $agreement[0]->farm_size; ?>&nbsp;acres<br/>\n\
                <b>Region</b>&nbsp;:&nbsp;<?php echo $agreement[0]->region; ?><br/>\n\
                <b>District</b>&nbsp;:&nbsp;<?php echo $agreement[0]->district; ?><br/>\n\
                <b>Ward</b>&nbsp;:&nbsp;<?php echo $agreement[0]->farm_ward; ?><br/>\n\
                <b>Street</b>&nbsp;&nbsp;<?php echo $agreement[0]->farm_street; ?><br/>"
        });

        google.maps.event.addListener(marker, 'mouseover', function() {
        infowindow.open(map,marker);
        }); 
        
        google.maps.event.addListener(marker, 'mouseout', function() {
            infowindow.close(map,marker);
          });
      }
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCeBwb1JewkrfHXVeI51osgYAXTRMgg1_Y&callback=initMap"  async defer></script>
<div class="offset-5 col-4">
    <?php echo anchor('Administrator/crop_details','<span class="fas fa-file-pdf fa-2x" style="color:darkred"aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to download pdf file"></span>'); ?>
</div>
<div class="col-12">
    <table class="table table-condensed table-hover table-striped table-bordered">
        <tbody>
            <tr class="table-warning">
                <th style="text-align: left"><?php echo anchor('Administrator/project_agreements','<span class="fas fa-reply fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?></th>
                <th style="text-align: right"><?php echo anchor('Administrator/add_project_agreement/'.$agreement[0]->agreementID,'<span class="fas fa-edit fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to edit agreement details"></span>'); ?></th>
            </tr>
            <tr class="table-info">
                <th style="text-align: center" colspan="2">Basic Details</th>
            </tr>
            <tr>
                <th>&nbsp;&nbsp;Agreement ID</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->agreementID; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Farmer</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->fullname; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Crop</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->productname; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Farming Scheme</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->farming_scheme; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Sponsorship Scheme</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->sp_scheme; ?></td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Estimated Organization Investment Amount</th>
                <td>&nbsp;&nbsp;<?php echo number_format($agreement[0]->org_investment_amount, 2); ?> Tsh</td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Estimated Farmer Investment Amount</th>
                <td>&nbsp;&nbsp;<?php echo number_format($agreement[0]->pvt_investment_amount, 2); ?> Tsh</td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Market Price per unit</th>
                <td>&nbsp;&nbsp;<?php echo number_format($agreement[0]->market_price, 2); ?> Tsh</td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Farmer's Profit Share</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->farmer_profit_share; ?>%</td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Organization's Profit Share</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->org_profit_share; ?>%</td>
            </tr>  
            <tr class="table-info">
                <th style="text-align: center" colspan="2">Farm Details</th>
            </tr>
             <tr>
                <th>&nbsp;&nbsp;Farm Size</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->farm_size; ?> acres</td>
            </tr>  
            <tr>
                <th>&nbsp;&nbsp;Farm location Region</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->region; ?></td>
            </tr> 
            <tr>
                <th>&nbsp;&nbsp;Farm location DIstrict</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->district; ?></td>
            </tr> 
            <tr>
                <th>&nbsp;&nbsp;Farm location Ward</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->farm_ward; ?></td>
            </tr> 
            <tr>
                <th>&nbsp;&nbsp;Farm location Street</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->farm_street; ?></td>
            </tr>
            <tr class="table-info">
                <th style="text-align: center" colspan="2">Other Details</th>
            </tr>
             <tr>
                <th>&nbsp;&nbsp;Comments</th>
                <td>&nbsp;&nbsp;<?php echo $agreement[0]->pa_comments; ?></td>
            </tr>
             <tr>
                <td colspan="2"><div id="map" style="height: 400px"></div></td>
            </tr>
        </tbody>
    </table>
    
</div>