<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript"> 
    
    $(document).ready(function(){
        
        $('a#confirmorder').click(function(){
           
            $('div#confirmorderModal').modal();
        });
        
        $('a#cancelorder').click(function(){
           
            $('div#cancelorderModal').modal();
        });
        
        $("#deliverydate").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd'
        });
        
    });
</script>
<div class="row">
    <div class="col-6">
        <?php echo anchor($this->session->userdata('prev_url_path'),'<span class="fas fa-reply fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?>
    </div>
    <div class="col-3">
        <?php
        
        echo $ord[0]->orderstatus == 'pending'?'<a href="#" class="btn btn-outline-success" id="confirmorder" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to confirm order"> Confirm Order</a>':'';
        
        ?>
    </div>
    <div class="col-3">
        <?php
        if($ord[0]->orderstatus == 'pending' && $ord[0]->orderstatus <> 'paid'){ ?>
        
            <a href="#" class="btn btn-outline-danger" id="cancelorder" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to cancel order"> Cancel Order</a>
        <?php } ?>
    </div>
</div>
<hr/>
<div class="row">
    <div class="col-4"><b>Buyer Name</b>&nbsp;:&nbsp;<?php echo $ord[0]->buyer; ?> </div>
    <div class="col-4"><b>Buyer Contact</b>&nbsp;:&nbsp;<?php echo $ord[0]->buyercontact; ?> </div>
    <div class="col-4"><b>Order ID</b>&nbsp;:&nbsp;<?php echo $ord[0]->orderid; ?> </div>
    <div class="col-4"><b>Order Date</b>&nbsp;:&nbsp;<?php echo $ord[0]->orderdate; ?> </div>
    <div class="col-4"><b>Order Status</b>&nbsp;:&nbsp;<?php echo $ord[0]->orderstatus; ?> </div>
    <div class="col-4"><b>Delivery Type</b>&nbsp;:&nbsp;<?php echo $ord[0]->deliverytype == 1?'Self Pickup':'Door Delivery'; ?> </div>
     <?php if($ord[0]->deliverytype <> 1){ ?>
    <div class="col-4"><b>Destination</b>&nbsp;:&nbsp;<?php echo $ord[0]->deliverydestination; ?> </div>
    <div class="col-4"><b>Delivery Fee</b>&nbsp;:&nbsp;<?php echo number_format($ord[0]->deliverycost,2)." /="; ?> </div>
    <div class="col-4"><b>Driver Name</b>&nbsp;:&nbsp;<?php echo $ord[0]->drivername; ?> </div>
    <div class="col-4"><b>Vehicle No</b>&nbsp;:&nbsp;<?php echo $ord[0]->vehicleno; ?> </div>
    <?php }?>
    <div class="col-4"><b>Product Cost</b>&nbsp;:&nbsp;<?php echo number_format($ord[0]->productcost,2)." /="; ?> </div>
    <div class="col-4"><b>Total Cost </b>&nbsp;:&nbsp;<?php echo number_format($ord[0]->deliverycost+$ord[0]->productcost,2)." /="; ?> </div>
    <div class="col-4"><b> <?php echo $ord[0]->deliverytype <> 1?'Delivery Date':'Pickup Date' ?></b>&nbsp;:&nbsp;<?php echo $ord[0]->deliverydate; ?> </div>
    
    <hr/>
    <div class="col-12">
	<table class="table table-condensed table-hover table-bordered">

            <tbody>
                <?php if($ord != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($ord as $v){ 
                        $reserved=$this->Inventory_model->total_stock($v->productid,NULL,NULL,$v->aoiID)->ttlStk;
                        ?>
                        
                        <tr>
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<img src="<?php echo base_url().'imgs/products/sellingcrops/'.$v->productimage ?>" alt="cropImage" class="imgviewdsplay"/></td>
                            <td>
                                &nbsp;&nbsp;<span><b>Product Name : </b><?php echo $v->productname; ?></span><br/>
                                &nbsp;&nbsp;<span><b>Quantity : </b><?php echo number_format($v->productqty)." ".$v->productunit; ?></span><br/>
                                &nbsp;&nbsp;<span><b>Reserved : </b><?php echo number_format($reserved)." ".$v->productunit; ?></span><br/>
                                &nbsp;&nbsp;<span><b>Cost : </b><?php echo number_format($v->productprice,2)." /="; ?></span><br/>
                                &nbsp;&nbsp;<span><b>Status : </b><?php echo $v->status; ?></span><br/>
                                &nbsp;&nbsp;<span>
                                    <?php echo $ord[0]->orderstatus == 'pending' && $v->status <> 'cancelled'?anchor('Administrator/available_stock/'.$v->productid.'_'.$v->orderid.'_'.$v->id,'<span class="btn btn-link" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="view product stock">view stock</span>'):''; ?>
                                    <?php echo $ord[0]->orderstatus == 'confirmed' && $v->status <> 'cancelled'?anchor('Administrator/view_supply_details/'.$v->aoiID,'<span class="btn btn-link" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="view details">view details</span>'):''; ?>
                                &nbsp;&nbsp;
                                <?php echo $ord[0]->orderstatus == 'pending' && $v->status <> 'cancelled'?anchor('Administrator/remove_agro_order_item/'.$v->aoiID,'<span class="fas fa-times-circle fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="remove product"></span>'):''; ?>
                                </span>
                            </td>
                        </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="3" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
    
    <!--------------confirm order modal--->
<div class="modal fade confirmorderModal" id="confirmorderModal" tabindex="-1" role="dialog" aria-labelledby="confirmorderModal" aria-hidden="true" style="font-family: cursive">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="confirmorderModalTitle" style="text-align: center;">Fill Order <?php echo $ord[0]->orderid;?> Delivery Details!!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form class="form-horizontal" id="confirmorderForm" role="form" method="POST" action="<?php echo base_url().'Administrator/orderdetails/'.$ord[0]->orderid; ?>">
        <div class="modal-body">
            <input type="hidden" name="orderid" value="<?php echo $ord[0]->orderid; ?>"/>
            <div class="form-group row">
                <label for="driver" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Driver</label>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    <select name="driver" id="driver" class="form-control" required>
                        <option value=""></option>
                        <?php foreach($drivers AS $k=>$v){ ?>
                        <option value="<?php echo $v->id; ?>" ><?php echo $v->first_name.' '.$v->last_name; ?></option>
                        <?php }?>
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <label for="deliverydate" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Delivery Date</label>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    <input type="text" name="deliverydate" class="form-control" id="deliverydate" placeholder="Delivery Date" required/>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal" >Cancel</button>
          <input type="submit" class="btn btn-outline-success" name="confirm_order" id="confirm_order" value="Submit"/>
        </div>
    </form>

    </div>
  </div>
</div>
    
    <!--------------cancel order modal--->
<div class="modal fade cancelorderModal" id="cancelorderModal" tabindex="-1" role="dialog" aria-labelledby="cancelorderModal" aria-hidden="true" style="font-family: cursive">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="cancelorderModalTitle" style="text-align: center;">Cancellation Order <?php echo $ord[0]->orderid;?> Details!!</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
        <form class="form-horizontal" id="cancelorderForm" role="form" method="POST" action="<?php echo base_url().'Administrator/orderdetails/'.$ord[0]->orderid; ?>">
        <div class="modal-body">
            <input type="hidden" name="orderid" value="<?php echo $ord[0]->orderid; ?>"/>
            <div class="form-group row">
                <label for="reason" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 control-label">Cancellation Reason</label>
                <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                    <textarea name="reason" class="form-control" id="reason" placeholder="Reason" required></textarea>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-outline-danger" data-dismiss="modal" >Cancel</button>
          <input type="submit" class="btn btn-outline-success" name="cancel_order" id="cancel_order" value="Submit"/>
        </div>
    </form>

    </div>
  </div>
</div>
</div>