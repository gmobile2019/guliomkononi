<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="offset-2 col-4">
        <?php echo anchor("Administrator/orders/ord_".$orderid."_dst_".$dst."_status_".$status."_by_".$buyer."_st_".$from."_end_".$to."_docType_1/",'<span id="pdf" class="fa fa-file-pdf fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to download pdf document"></span>'); ?>
    </div>
    <div class="col-12">
	<table class="table table-condensed table-hover table-striped table-bordered">

            <thead>
                <tr>
                    <th style="text-align:center;">S/NO</th>
                    <th style="text-align:center;">Order ID</th>
                    <th style="text-align:center;">Order Amount</th>
                    <th style="text-align:center;">Destination</th>
                    <th style="text-align:center;">Delivery Fee</th>
                    <th style="text-align:center;">Order Date</th>
                    <th style="text-align:center;">Status</th>
                 </tr>
            </thead>
            <tbody>
                <?php if($data != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($data as $key=>$value){ ?>
                        <tr>
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<?php echo anchor('Administrator/orderdetails/'.$value->orderid,$value->orderid); ?></td>
                            <td>&nbsp;&nbsp;<?php echo number_format($value->productcost,2); ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->deliverydestination; ?></td>
                            <td>&nbsp;&nbsp;<?php echo number_format($value->deliverycost, 2); ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->orderdate; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->orderstatus; ?></td>
                        </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="8" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>