<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="offset-2 col-4">
        <?php echo anchor("Administrator/crops/name_".$name."_categ_".$categ."_status_".$status."_docType_1/",'<span id="pdf" class="fa fa-file-pdf fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to download pdf document"></span>'); ?>
    </div>
    <div class="offset-3 col-3">
        <?php echo anchor('Administrator/add_crop','<span class="btn btn-outline-primary">Add Selling Crop</span>'); ?>
    </div>
        <?php if($data != null){ 
            
    foreach($data AS $ky=>$vl){ ?>
    <div class="col-12 crpdsply">
                
        <img src="<?php echo base_url().'imgs/products/sellingcrops/'.$vl->productimage ?>" alt="cropImage" class="imgviewdsplay"/>
        <div><b>Name</b>&nbsp;:&nbsp;<?php echo $vl->productname; ?></div>
        <div><b>Category</b>&nbsp;:&nbsp;<?php echo $vl->productcategory; ?></div>
        <div><b>Price </b>&nbsp;:&nbsp;<?php echo number_format($vl->productprice,2).'/= Tsh per '.$vl->productunit; ?></div>
        <div><b>Description</b>&nbsp;:&nbsp;<?php echo $vl->description; ?></div>
        <div><b>Status</b>&nbsp;:&nbsp;<?php echo $vl->status; ?></div>
        <div style="text-align: center">
            <?php 
            
            if($vl->status == 'Pending'){
                
                echo anchor('Administrator/process_crop_request/'.$vl->id,'please click here to process request');
            }else{
                
                $active_status=$vl->status == 'Active'?'<span class="fas fa-minus-circle fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Suspend"></span>':'<span class="fas fa-plus-circle fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>';
                echo anchor('Administrator/add_crop/'.$vl->id,'<span class="fas fa-edit fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>')."&nbsp;&nbsp;";
                echo anchor('Administrator/activate_deactivate_crop/'.$vl->id.'/'.$vl->status.'/administrators',$active_status);
            }
            ?>
        </div>
    </div>
    <?php }
        }?>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>