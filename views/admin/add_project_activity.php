<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
            
            
    });
</script>
<div class="">
   
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Administrator/add_project_activity/'.$id); 
                ?>
                <div class="form-group row">
                    <div class="col-xs-12 col-4">
                        <?php echo anchor($this->session->userdata('prev_url_path'),'<span class="fas fa-reply fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="projectID" class="col-xs-12 col-5 control-label">Project ID&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <select name="projectID" id="projectID" class="form-control" >
                        <option></option>
                        <?php foreach($projects as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->agreementID; ?>" <?php echo ($id != null && trim($value->agreementID) == trim($project_activity[0]->projectID))?'selected="selected"':set_select('projectID',$value->agreementID); ?>><?php echo $value->agreementID; ?></option>
                        
                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('projectID'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="stage" class="col-xs-12 col-5 control-label">Stage&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <select name="stage" id="stage" class="form-control" >
                        <option></option>
                        <?php foreach($stages as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->id; ?>" <?php echo ($id != null && trim($value->id) == trim($project_activity[0]->project_stage))?'selected="selected"':set_select('stage',$value->id); ?>><?php echo $value->stage; ?></option>
                        
                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('stage'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="activity_subject" class="col-xs-12 col-5 control-label">Activity Subject&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <input type="text" class="form-control" name="activity_subject" id="activity_subject" placeholder="Activity Subject" value="<?php echo $id != null?$project_activity[0]->activity_subject:set_value('activity_subject'); ?>" required/>
                        <?php echo form_error('activity_subject'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="activity_summary" class="col-xs-12 col-5 control-label">Activity Summary&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <textarea class="form-control" name="activity_summary" rows="4" id="activity_summary" placeholder="Summary" required><?php echo $id != null?$project_activity[0]->activity_summary:set_value('activity_summary'); ?></textarea>
                        <?php echo form_error('activity_summary'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="status" class="col-xs-12 col-5 control-label">Project Status&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-4">
                        <select name="status" id="status" class="form-control" >
                        <option value=""></option>
                        <option value="Ongoing" <?php echo ($id != null && "Ongoing" == trim($project_activity[0]->status))?'selected="selected"':set_select('status','Ongoing'); ?>>Ongoing</option>
                        <option value="Completed" <?php echo ($id != null && "Completed" == trim($project_activity[0]->status))?'selected="selected"':set_select('status','Completed'); ?>>Completed</option>
                        </select>
                        <?php echo form_error('status'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>