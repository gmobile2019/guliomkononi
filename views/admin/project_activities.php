<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="offset-8 col-3">
        <?php echo anchor('Administrator/add_project_activity','<span class="btn btn-outline-primary">click here to add new project activity</span>'); ?>
    </div>
    <div class="col-12">
	<table class="table table-condensed table-hover table-striped table-bordered">

            <thead>
                <tr>
                    <th style="text-align:center;">S/NO</th>
                    <th style="text-align:center;">Project ID</th>
                    <th style="text-align:center;">Project Stage</th>
                    <th style="text-align:center;">Activity Subject</th>
                    <th style="text-align:center;">Date</th>
                    <th style="text-align:center;">Status</th>
                    <th style="text-align:center;">Action</th>
                 </tr>
            </thead>
            <tbody>
                <?php if($data != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($data as $key=>$value){ ?>
                        <tr class="<?php echo $value->status == 'Completed'?'table-success':'table-info'; ?>" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $value->activity_summary; ?>">
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<?php echo anchor('Administrator/project_agreement_details/'.$value->pjid,'<span aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here for project details">'.$value->projectID.'</span>'); ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->stage; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->activity_subject; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->lastupdate2; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->status; ?></td>
                            <td> &nbsp;&nbsp;
                                <?php 
                                $date = strtotime($value->lastupdate);
                                $date = strtotime("+7 day", $date);
                               
                                if($value->status <> 'Completed' && date('Y-m-d H:i:s', $date) >= date('Y-m-d H:i:s')){ 
                                
                                echo anchor('Administrator/add_project_activity/'.$value->id,'<span class="fas fa-edit fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                                &nbsp;&nbsp;
                                <?php echo anchor('Administrator/remove_project_activity/'.$value->id,'<span class="fas fa-trash-alt fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="delete activity"></span>'); 
                                }
                                ?>
                            </td>
                        </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="7" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>