<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row">
    <div class="offset-8 col-3">
        <?php echo anchor('Administrator/add_project_agreement','<span class="btn btn-outline-primary">click here to add new project agreement</span>'); ?>
    </div>
    <div class="col-12">
	<table class="table table-condensed table-hover table-striped table-bordered">

            <thead>
                <tr>
                    <th style="text-align:center;">S/NO</th>
                    <th style="text-align:center;">Farmer</th>
                    <th style="text-align:center;">Product</th>
                    <th style="text-align:center;">Farming Scheme</th>
                    <th style="text-align:center;">Sponsorship Scheme</th>
                    <th style="text-align:center;">Status</th>
                    <th style="text-align:center;">Action</th>
                 </tr>
            </thead>
            <tbody>
                <?php if($data != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($data as $key=>$value){ ?>
                        <tr class="<?php echo $value->status == 'Closed'?'table-success':'table-info'; ?>" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="<?php echo $value->description; ?>">
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<?php echo anchor('Administrator/project_agreement_details/'.$value->agreementID,'<span aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here for more details">'.$value->fullname.'</span>'); ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->productname; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->farming_scheme; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->sp_scheme; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->status; ?></td>
                            <td> &nbsp;&nbsp;
                                <?php if($value->status <> 'Closed'){ 
                                $active_status=$value->status == 'Active'?'<span class="fas fa-minus-circle fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Suspend"></span>':'<span class="fas fa-plus-circle fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 

                                echo anchor('Administrator/add_project_agreement/'.$value->agreementID,'<span class="fas fa-edit fa-1x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                                &nbsp;&nbsp;
                                <?php echo anchor('Administrator/activate_deactivate_add_project_agreement/'.$value->id.'/'.$value->status,$active_status); 
                                }
                                ?>
                            </td>
                        </tr>  
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="7" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>