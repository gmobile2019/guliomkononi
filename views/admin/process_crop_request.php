<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
      
            $('input#requestRej').click(function(){
                
                var conf=confirm("Rejection means request deletion,\nAre you sure you want to continue?");
                
                if(!conf){
                    return false;
                }
            });
            
            $('input#requestApp').click(function(){
                
                var conf=confirm("Approval means allowing crop for selling,\nAre you sure you want to continue?");
                
                if(!conf){
                    return false;
                }
            });
    });
</script>
<div class="">
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Administrator/process_crop_request/'.$id); 
                ?>
                <img src="<?php echo base_url().'imgs/products/sellingcrops/'.$crop[0]->productimage ?>" alt="cropImage" class="imgviewdsplay"/>
                <div class="form-group row">
                    <label for="cropname" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Crop Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-5 col-lg-5">
                        <input type="text" class="form-control" name="cropname" id="cropname" placeholder="Crop Name" value="<?php echo $crop[0]->productname; ?>" required/>
                        <input type="hidden" name="cropid" value="<?php echo $crop[0]->id; ?>" required/>
                        <?php echo form_error('cropname'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cropcategory" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Crop Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-5 col-lg-5">
                    <select name="cropcategory" id="cropcategory" class="form-control" required>
                        <option value=""></option>
                        <option value="Cereal" <?php echo ('Cereal' == trim($crop[0]->productcategory))?'selected="selected"':set_select('cropcategory','Cereal'); ?>>Cereal</option>
                        <option value="Fruit" <?php echo ('Fruit' == trim($crop[0]->productcategory))?'selected="selected"':set_select('cropcategory','Fruit'); ?>>Fruit</option>
                        <option value="Vegetable" <?php echo ('Vegetable' == trim($crop[0]->productcategory))?'selected="selected"':set_select('cropcategory','Vegetable'); ?>>Vegetable</option>
                    </select>
                        <?php echo form_error('cropcategory'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cropunit" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Crop Unit &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-5 col-lg-5">
                        <input type="text" class="form-control" name="cropunit" id="cropunit" placeholder="Crop Unit" value="<?php echo $crop[0]->productunit; ?>" required/>
                        <?php echo form_error('cropunit'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Description</label>
                    <div class="col-xs-12 col-sm-8 col-md-5 col-lg-5">
                        <textarea  rows="3" class="form-control" name="description" id="description" placeholder="Description"><?php echo $crop[0]->description; ?></textarea>
                        <?php echo form_error('description'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="cropphoto" id="cropphoto" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="offset-3 col-6">
                        <input type="submit" class="btn btn-success" id="requestApp" value="Approve" name="approve"/>&nbsp;&nbsp;
                        <input type="submit" class="btn btn-danger" id="requestRej" value="Reject" name="reject"/>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
