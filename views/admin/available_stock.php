<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
        
        $('a.reserveLink').each(function(){
            var id=$(this).attr('id');
            
            $(this).click(function(){
                
                $('div#reserveModal_'+id).modal();
            });
       });
       
       var ord_quantity=parseInt($('input[name=orderqty]').val());
       
       $('input[name=quantity]').each(function(){
           
            var id=$(this).attr('id');
            var idd=id.split('_');

            var qty=parseInt($(this).val());
            var available_qty=$('input#available_'+idd[1]+'_'+idd[2]+'_'+idd[3]).val();
           
            if(isNaN(qty) || isNaN(ord_quantity)){
               
               $('input#reserveBatch_'+idd[1]+'_'+idd[2]+'_'+idd[3]).hide();
            }
           
            $(this).keyup(function(){
               
               qty=parseInt($(this).val());
               
                if(isNaN(qty) || isNaN(ord_quantity)){
               
                    $('input#reserveBatch_'+idd[1]+'_'+idd[2]+'_'+idd[3]).hide('slow');
                }else{
                 
                    if(qty <= ord_quantity && qty > 0 && qty <= available_qty){
                        
                        $('input#reserveBatch_'+idd[1]+'_'+idd[2]+'_'+idd[3]).show('slow');
                    }else{
                        
                         $('input#reserveBatch_'+idd[1]+'_'+idd[2]+'_'+idd[3]).hide('slow');
                    }
                    
                }
            });
        });
        
       $("form").submit(function(e){
            var id=$(this).attr('id');
           
            $.ajax({
            type:'POST',
            url: '<?php echo site_url('Administrator/reserve_stock'); ?>',
            data:$(this).serialize(),
            success: function(data){
                
                $('div#reserveModal_'+id).modal('hide');
                alert(data);
                location.reload();
            }
            });
             e.preventDefault();
        });
    });
</script>
<div class="row">
    <div class="col-6">
        <?php echo anchor($this->session->userdata('prev_url_path'),'<span class="fas fa-reply fa-2x" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to go back"></span>'); ?>
    </div>
    <div class="col-12">
	<table class="table table-condensed table-hover table-striped table-bordered">
            <input type="hidden" name="orderqty" value="<?php echo $ordered_qty; ?>"/>
            <thead>
                <tr>
                    <th style="text-align:center;">S/NO</th>
                    <th style="text-align:center;">Product Name</th>
                    <th style="text-align:center;">Product Batch</th>
                    <th style="text-align:center;">Supplier</th>
                    <th style="text-align:center;">Quantity</th>
                    <th style="text-align:center;">Available Date</th>
                    <th style="text-align:center;">Action</th>
                 </tr>
            </thead>
            <tbody>
                <?php if($data != null){

                    if($per_page == null){
                            $i=1;
                        }else{
                            $i=$per_page+1;
                        }
                    foreach($data as $key=>$value){ 
                        $prod=$this->Administration_model->products($value->productid,NULL,NULL,NULL,NULL);
                        $supplier=$this->Administration_model->get_member_info($value->supplier);
                        ?>
                        <tr>
                            <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $prod[0]->productname; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->productbatch; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $supplier[0]->first_name.' '.$supplier[0]->last_name; ?></td>
                            <td>&nbsp;&nbsp;<?php echo number_format($value->qty).' '.$prod[0]->productunit; ?></td>
                            <td>&nbsp;&nbsp;<?php echo $value->availableDate; ?></td>
                            <td>&nbsp;&nbsp;<a href="#" class="reserveLink" id="<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch?>" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="click here to reserve stock" >reserve stock</a></td>
                        </tr>
                        <!--------------product to reservation modal--->
                        <div class="modal fade reserveModal" id="reserveModal_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" tabindex="-1" role="dialog" aria-labelledby="reserve_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" aria-hidden="true" style="font-family: cursive">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header">
                                <h5 class="modal-title" id="reserve_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" style="text-align: center;">Fill <?php echo $prod[0]->productname;?> Reservation details!!</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                  <span aria-hidden="true">&times;</span>
                                </button>
                              </div>
                                <form class="form-horizontal" id="<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" role="form" method="POST">
                                    <div class="modal-body">
                                        
                                        <input type="hidden" name="productid" id="product_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" value="<?php echo $value->productid; ?>" required/>
                                        <input type="hidden" name="item_order_id" id="orderid_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" value="<?php echo $order_item[0]->aoiID; ?>" required/>
                                        <input type="hidden" name="supplierid" id="supplier_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" value="<?php echo $value->supplier; ?>" required/>
                                        <input type="hidden" name="productbatch" id="batch_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" value="<?php echo $value->productbatch; ?>" required/>
                                        <input type="hidden" name="available_qty" id="available_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" value="<?php echo $value->qty; ?>" required/>
                                        <div class="form-group row">
                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                <img src="<?php echo base_url().'imgs/products/sellingcrops/'.$prod[0]->productimage ?>" alt="cropImage" class="imgviewdsplay"/>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-6">
                                                <b>Buyer : </b><?php echo $order_item[0]->buyer; ?>
                                            </div>
                                            <div class="col-6">
                                                <b>Contact : </b><?php echo $order_item[0]->buyercontact; ?>
                                            </div>
                                            <div class="col-6">
                                                <b>Supplier : </b><?php echo $supplier[0]->first_name.' '.$supplier[0]->last_name; ?>
                                            </div>
                                            <div class="col-6">
                                                <b>Product Batch : </b><?php echo $value->productbatch; ?>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label for="qty_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Quantity</label>
                                            <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                                <input type="number" class="form-control" name="quantity" id="qty_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" placeholder="Quantity" value="" required/>
                                            </div>
                                            <label for="qty_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 control-label"><?php echo $prod[0]->productunit; ?></label>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-outline-danger" data-dismiss="modal" id="cancel_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>">Cancel</button>
                                      <input type="submit" class="btn btn-outline-success" name="reserve_batch" id="reserveBatch_<?php echo $value->productid.'_'.$value->supplier.'_'.$value->productbatch; ?>" value="Reserve Stock"/>
                                    </div>
                                </form>

                            </div>
                          </div>
                        </div>
                    <?php }
                    }else{ ?>
                <tr>
                    <td colspan="7" style="text-align:center"> NO DATA FOUND</td>
                </tr>  
                    <?php } ?>
            </tbody>
        </table>
    </div>
    <div class="offset-4 col-8">
            <?php echo $links; ?>
    </div>
</div>