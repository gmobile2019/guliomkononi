<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
      
    });
</script>
<div class="">
    <?php 
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Administrator/add_crop/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="cropname" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Crop Name &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="cropname" id="cropname" placeholder="Crop Name" value="<?php echo $id <> NULL?$crop[0]->productname:set_value('cropname'); ?>" required/>
                        <?php echo form_error('cropname'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cropcategory" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Crop Category&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                    <select name="cropcategory" id="cropcategory" class="form-control" required>
                        <option value=""></option>
                        <option value="Cereal" <?php echo ($id != null && 'Cereal' == trim($crop[0]->productcategory))?'selected="selected"':set_select('cropcategory','Cereal'); ?>>Cereal</option>
                        <option value="Fruit" <?php echo ($id != null && 'Fruit' == trim($crop[0]->productcategory))?'selected="selected"':set_select('cropcategory','Fruit'); ?>>Fruit</option>
                        <option value="Vegetable" <?php echo ($id != null && 'Vegetable' == trim($crop[0]->productcategory))?'selected="selected"':set_select('cropcategory','Vegetable'); ?>>Vegetable</option>
                    </select>
                        <?php echo form_error('cropcategory'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cropunit" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Crop Unit &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="cropunit" id="cropunit" placeholder="Crop Unit" value="<?php echo $id <> NULL?$crop[0]->productunit:set_value('cropunit'); ?>" required/>
                        <?php echo form_error('cropunit'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cropprice" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Market Price &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="number" class="form-control" name="cropprice" id="cropprice" placeholder="Crop Market Price" value="<?php echo $id <> NULL?$crop[0]->productprice:set_value('cropprice'); ?>" required/>
                        <?php echo form_error('cropprice'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cropdiscount" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Market Price Discount</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="numeric" class="form-control" name="cropdiscount" id="cropdiscount" placeholder="Market Price Discount" value="<?php echo $id <> NULL?$crop[0]->productdiscount:set_value('cropdiscount'); ?>" required/>
                        <?php echo form_error('cropdiscount'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="stocklimit" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Lower Stock Limit</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="numeric" class="form-control" name="stocklimit" id="stocklimit" placeholder="Lower Stock Limit" value="<?php echo $id <> NULL?$crop[0]->stocklimit:set_value('stocklimit'); ?>" required/>
                        <?php echo form_error('stocklimit'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Description</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea  rows="3" class="form-control" name="description" id="description" placeholder="Description"><?php echo $id <> NULL?$crop[0]->description:set_value('description'); ?></textarea>
                        <?php echo form_error('description'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="cropphoto" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Crop Image</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="file" class="form-control" name="cropphoto" id="cropphoto" value=""/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-6">
                        <button type="submit" class="btn btn-success btn-block">Submit</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
