<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
    $(document).ready(function(){
            var org=1;
        //pending files
        $.ajax({
            type:'POST',
            url:'<?php echo site_url('Administrator/getOutOfStockProducts'); ?>',
            data:{org :org},
            success:function(data){
                
                    $('div#stocklimit').empty();
                    $('div#stocklimit').html(data);
             }

            });
    });
    
</script>
<div class="row">
    <div class="col-6" id="chart" style="height:400px;width:400px; "></div>
    <div class="col-6" id="stocklimit"></div>
    <div class="col-3"></div>
</div>